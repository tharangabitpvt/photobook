$(document).ready(function(){
	const remote = require('electron').remote
	const {app, dialog, Menu, MenuItem} = require('electron').remote
	//var Jimp = require('jimp')
	const path = require('path')
	const url = require('url')
	const ipc = require('electron').ipcRenderer
	const fs = require('fs')
	const fse = require('fs-extra')
	const crypto = require('crypto')
	const piexif = require("piexifjs")
	const FontFaceObserver = require('fontfaceobserver')
	const isOnline = require('is-online')
	const fetch = require('node-fetch')
	//var dragDrop = require('drag-drop')
	require('electron-disable-file-drop')
//	const protocols = require('electron-protocols')
	const templatesPath = path.join(__dirname, 'templates')
	const ftConfigFilePath = path.join(__dirname, 'ft', 'ft.json')
	const ftErrorLogFile = path.join(__dirname, 'ft', 'error_log.txt')
	const fontsJSON = path.join(__dirname, 'fonts', 'fonts.json')
	const LOCK_OPEN = 1;
	const LOCK_CLOSE = 0;

	const VISIBLE_TRUE = 1;
	const VISIBLE_FALSE = 0;

	const PAGE_NOT_EDITTED = 0;
	const PAGE_EDITTED = 1;
	const PAGE_EXPORTED = 2;

	const photobookWeb = "http://127.0.0.1:8000";
	
	zoomLevel = 1;
	canvasFitWidth = 0;
	canvasFitHeight = 0;
	dpi = 300;
	projectName = '';
	undoReady = false;

	var imageArray = [];
    var filesObject = {};
	var selectedImages = [];
    var unusedFiles = [];
    var usedFiles = [];
    var pickAColor = false;
    var pickFillColor = false;
    var availableAlbumSizes = [];

    $ActiveObject = null;

    var customJSONOptions = [
		'ftObjectName',
		'ftEditable',
		'clipBoxName', 
		'ftObjectType',
		'ftCropPath',
		'ftImagePath',
		'fixed', 
		'lockRotation', 
		'lockMovementX', 
		'lockMovementY', 
		'lockScalingX',
		'lockScalingY',
		'filters',
		'selectable',
		'hoverCursor',
		'fontStyle',
		'fontFamily',
		'fontSize',
		'fontWeight',
		'ftColorPallete',
		'ftExportable',
		'ftPolyPoints',
		'ftRandId',
		'right',
		'bottom',
		'evented',
		'dynamicColor',
		'ftFilters',
		'ftFixedFilters',
		'ftClipCode',
		'ftApplyOn',
		'ftApplyOnClipPath'
		];

    var filters = [];
    var adjustableFilters = ['brightness', 'contrast', 'saturation', 'grayscale', 'blur', 'hue'];
	var grayscaleMode = ['average', 'luminosity', 'lightness'];

    var $masonryGrid = $('#image_panel').masonry({
        itemSelector: '.grid-item',
        percentPosition: true,
        horizontalOrder: true,
        containerStyle: null,
        gutter: 10
        //columnWidth: 'grid-sizer'
        //columnHeight: grid_height
    });

    var $masonryGridUnused = $('#unused').masonry({
    	itemSelector: '.img-container-outer',
        percentPosition: true,
        horizontalOrder: true,
        containerStyle: null
    });

    var $masonryGridUsed = $('#used').masonry({
    	itemSelector: '.img-container-outer',
        percentPosition: true,
        horizontalOrder: true,
        containerStyle: null
    });

	swal.setDefaults({
		confirmButtonClass: 'btn btn-default',
	  	cancelButtonClass: 'btn btn-default',
	  	buttonsStyling: false,
	  	allowOutsideClick: false,
	  	allowEscapeKey: false,
	  	animation: false,
	});

	const menuTemplate = [
		{
			label: 'File',
			submenu: [
				{
					label: 'New project', 
					click: () => {$('.new-project-btn').trigger('click')}
				},
				{
					label: 'Recent projects',
					submenu: [
						{label: 'No recent projects'}
					]
				},
				 {type: 'separator'},
				{
					label: 'Export',
					submenu: [
						{
							label: 'Export to JPEG',
							accelerator: 'CommandOrControl+Shift+E',
							click: () => {$('.export-file-btn').trigger('click')}
						}
					]
				},
			 	{type: 'separator'},
				{role: 'quit'}
			]
		},
		{
			label: 'Edit',
			submenu: [
				{
					label: 'Undo',
					accelerator: 'CommandOrControl+Z',
					click: () => {$('.undo').trigger('click')}
				},
				{
					label: 'Redo',
					accelerator: 'CommandOrControl+Y',
					click: () => {$('.redo').trigger('click')}
				}
			]
		},
		{
			label: 'Go to',
			submenu: [
				{
					label: 'My projects',
					click: () => {$('a[href="#your_projects"]').trigger('click')}
				},
				{
					label: 'Add files',
					click: () => {$('a[href="#add_files"]').trigger('click')}
				},
				{
					label: 'Develop',
					click: () => {$('a[href="#develop"]').trigger('click')}
				},
				{
					label: 'Templates',
					click: () => {$('a[href="#templates"]').trigger('click')}
				},
				{
					label: 'Updates',
					click: () => {$('a[href="#ft_shop"]').trigger('click')}
				},
				{type: 'separator'},
				{
					label: 'Shortcuts',
					click: () => {$('#shorcutsModal').modal('show')}
				},
			]
		},
		{
			label: 'Project',
			submenu: [
				{
					label: 'Change workspace',
					click: () => {ft.changeWorkspace()}
				}
			]
		},
		{
	    	role: 'window',
		    submenu: [
		      {role: 'minimize'},
		      {role: 'close'},
		      {role: 'reload'},
		      {role: 'forcereload'},
		      {role: 'toggledevtools'}
		    ]
	  	},
	  	{
		    label: 'Help',
		    submenu: [
		    	{
		    		label: 'About Photobook',
		    		click: () => {ipc.send('show-about-page')}
		    	},
		      	{
			        label: 'Learn More',
			        click () { require('electron').shell.openExternal('http://photobook.lk') }
		      	}
		    ]
	  	}
	]

	var menu = Menu.buildFromTemplate(menuTemplate)
	Menu.setApplicationMenu(menu)

	$('#brightness').slider({
		min: -1,
		max: 1,
		value: 0
	});

	$('#contrast').slider({
		min: -1,
		max: 1,
		value: 0
	});

	$('#saturation').slider({
		min: -1,
		max: 1,
		value: 0
	});

	$('#ft_line_height').slider({
		min: 0.25,
		max: 3,
		value: 1
	});

	$('#ft_font_size_slider').slider({
		min: 10,
		max: 1000,
		value: 200
	});

	$('#ft_shadow_spread_slider').slider({
		min: 0,
		max: 500,
		value: 50
	});

	$('#ft_shadow_offset_x_slider').slider({
		min: -300,
		max: 300,
		value: 0
	});

	$('#ft_shadow_offset_y_slider').slider({
		min: -300,
		max: 300,
		value: 0
	});

	$('#ft_stroke_slider').slider({
		min: 0,
		max: 50,
		value: 0
	});

	$('#exp_quality').slider({
		min: 0.25,
		max: 1,
		value: 1
	});

	$('#ft_opacity_slider').slider({
		min: 0,
		max: 1,
		value: 1
	});	

	var ft = {
		'editImgPath': path.join(__dirname, 'images', 'edit'),
		'projectJSON': {
			name: '',
			files: {}, // {filename1: {path: '', is_added: 0, use_count: 0}, filename2: {path: '', is_added: 1, use_count: 0}, ...}
			album_size: '',
			dpi: 300,
			template: 0,
			pages: {}, // pages of project with export status(1=exported, 0=not exported). {page1: 0, page2: 1, page3: 1, ...}
			exportPath: '',
			createDate: '',
		},

		'albumSizes': ["8x16",
						"10x24", "10x30",
						"12x18", "12x24",
						"13x16",
						"16x20", "16x22", "16x24", "16x30",
						//"4x10", "4x12",
						//"5x7", "5x12",
						//"6x10", "6x12"
						],

		'fonts': JSON.parse(fs.readFileSync(fontsJSON, 'utf8')),

		'findFontLang': function(fontName) {
			var fontLang = '';
			$.each(ft.fonts, function(lang, fontFamily){
				$.each(fontFamily, function(fFam, fObj){
					if(fFam == fontName) {
						//console.log(lang);
						fontLang = lang;
					}
				});
			});

			return fontLang;
		},

		'loadFontAndUse': function(font) {
		  var myfont = new FontFaceObserver(font)
		  myfont.load()
		    .then(function() {
		      // when font is loaded, use it.
		      canvas.getActiveObject().set("fontFamily", font);
		      canvas.requestRenderAll();
		    }).catch(function(e) {
		      console.log(e)
		      //alert('font loading failed ' + font);
		    });
		},

		'addImage': function(fileName) {
			var imagePane = $('#image_pane');
			imagePane.attr('src', ft.editImgPath + fileName);
		},

		'setDPI': function (canvas, dpi) {
		    // Set up CSS size.
		    canvas.style.width = canvas.style.width || canvas.width + 'px';
		    canvas.style.height = canvas.style.height || canvas.height + 'px';

		    // Get size information.
		    var scaleFactor = dpi / 96;
		    var width = parseFloat(canvas.style.width);
		    var height = parseFloat(canvas.style.height);

		    // Backup the canvas contents.
		    var oldScale = canvas.width / width;
		    var backupScale = scaleFactor / oldScale;
		    var backup = canvas.cloneNode(false);
		    backup.getContext('2d').drawImage(canvas, 0, 0);

		    // Resize the canvas.
		    var ctx = canvas.getContext('2d');
		    canvas.width = Math.ceil(width * scaleFactor);
		    canvas.height = Math.ceil(height * scaleFactor);

		    // Redraw the canvas image and scale future draws.
		    ctx.setTransform(backupScale, 0, 0, backupScale, 0, 0);
		    ctx.drawImage(backup, 0, 0);
		    ctx.setTransform(scaleFactor, 0, 0, scaleFactor, 0, 0);
		},

		// set canvas size according to the sie of the album page
		'setCanvasSize': function(fabCanvas, canHeight=10, canWidth=24, dpi=300) {
			var widthInPx = canWidth * dpi;
			var heightInPx = canHeight * dpi;

			var editContainerWidth = $('#ft_image_editor').width();
			var editContainerHeight = $('#ft_image_editor').height();

			var workAreaHeight = canvasFitHeight = window.innerHeight - 100;
			var workAreaWidth = canvasFitWidth = (workAreaHeight * widthInPx)/heightInPx;

			fabCanvas.setDimensions({width: widthInPx, height: heightInPx}, {backstoreOnly: true});

			//console.log('edit container W:' + workAreaWidth + ' ' + editContainerWidth + ' height:'+editContainerHeight);
			$('#canvas_size').val(canHeight + '" x ' + canWidth + '"');
			$('#canvas_size').data('size', canHeight + ',' + canWidth);
			ft.fitToScreen(fabCanvas);	
			$('#resolution_val').html(heightInPx + ' x ' + widthInPx + ' px');		
		},

		// set zoom level
		'zoomCanvas': function (canvasObj, zoomPercentage, infinit=false) {
			if((!infinit && zoomPercentage > 100) || zoomPercentage < 0) {
				return;
			} else if(zoomPercentage == 0) {
				ft.fitToScreen(canvasObj);
			} else {
				//var canvasCssWidth = parseFloat($('#ft_canvas').css('width'));
				var canvasActualWidth = canvasObj.width;

				var newWidth= (canvasActualWidth * (zoomPercentage/100)).toFixed(2) + "px";
				var newHeight  = "auto";
			
				canvasObj.setDimensions({width: newWidth, height: newHeight}, {cssOnly: true});				

				canvasObj.calcOffset();

				ft.vCenterCanvas(canvasObj);
			}
		},

		'vCenterCanvas': function(canvasObj) {
			var canvasWidth = canvasObj.width;
			var canvasHeight = canvasObj.height;
			var heightRatio = canvasHeight/canvasWidth;

			var canvasCSSWidth = $('#ft_canvas').width();
			var canvasCSSHeight = canvasCSSWidth * heightRatio;
			var editContainerHeight = $('#ft_image_editor').height();
			//var editConteinerWidth = parseFloat($('#ft_image_editor').width());
			//console.log(editContainerHeight, canvasCSSHeight);
			if(canvasCSSHeight < editContainerHeight) {
				$('.canvas-container').css({'top': parseInt(editContainerHeight/2) + 'px', 'margin-top': '-' + parseInt(canvasCSSHeight/2) + 'px'});
			}
		},

		'fitToScreen': function(fabCanvas) {			
			var canvasCurrentCSSWidth = $('#ft_canvas').width();
			var workAreaWidth = $('#ft_image_editor').width();
			//console.log(canvasCurrentCSSHeight, workAreaHeight);
			//fabCanvas.setDimensions({width: 'auto', height: workAreaHeight + 'px'}, {cssOnly: true});
			$('.canvas-container').css('width', workAreaWidth + 'px');
			//$('#ft_canvas').css({width: workAreaWidth, height: 'auto'});
			fabCanvas.setDimensions({width: workAreaWidth + 'px', height: 'auto'}, {cssOnly: true});

			//var editContainerWidth = parseFloat($('#ft_image_editor').width());
			var editContainerHeight = $('#ft_image_editor').height();

			//var canvasCssWidth = parseFloat($('#ft_canvas').width());
			var canvasCssHeight = $('#ft_canvas').height();
			//console.log(canvasCssWidth, canvasCssHeight);
			if(canvasCssHeight > editContainerHeight) {
				$('.canvas-container').css('height', editContainerHeight);
				fabCanvas.setDimensions({width: 'auto', height: editContainerHeight}, {cssOnly: true});
			}

			ft.vCenterCanvas(fabCanvas);
			ft.calcZoomValue(fabCanvas);

			fabCanvas.renderAll();
			fabCanvas.calcOffset();
			
		},

		// get canvas dimentions in inches
		'currentCanvasDim': function() {
			var canvasSizeArray = $('#canvas_size').data('size').split(",");
			return {canvasHeight: canvasSizeArray[0], canvasWidth: canvasSizeArray[1]}
		},

		'calcZoomValue': function(canvas) {
			var canvasCssWidth = parseFloat($('#ft_canvas').css('width'));
			var actualZoomVal = (canvasCssWidth/canvas.width) * 100;
			$('#zoom_value').val(Math.round(actualZoomVal));
		},

		'calcCropCanvasZoomValue': function(cropCanvasObj) {
			var canvasCssWidth = parseFloat($('#crop_canvas').css('width'));
			
			var actualZoomVal = (canvasCssWidth/cropCanvasObj.width) * 100;
			$('#crop_zoom_val').val(Math.round(actualZoomVal));
		},

		'resizeControls': function(canvas) {
			var objects = canvas.getObjects();
			var newDip = $('#dpi').val();
			for(i=0; i < objects.length; i++) {
				objects[i].cornerSize = Math.round((13 * newDip)/72);
				objects[i].rotatingPointOffset = Math.round((40 * newDip)/72);
				objects[i].transparentCorners = false;
				objects[i].borderScaleFactor = Math.round(3 * newDip/72);
			}
		},

		'resizeControlsOnZoom': function(canvasObj) {
			var objects = canvasObj.getObjects();
			var zoomRatio = parseInt($('#crop_zoom_val').val())/100;
			for(i=0; i < objects.length; i++) {
				objects[i].cornerSize = Math.round((20 * zoomRatio));
				objects[i].rotatingPointOffset = Math.round((50 * zoomRatio));
				objects[i].transparentCorners = false;
				objects[i].borderScaleFactor = 2;

				if(objects[i].stroke && objects[i].strokeWidth >= 20) {
					objects[i].strokeWidth = (objects[i].strokeWidth + 5)*zoomRatio;
					objects[i].strokeDashArray = [(zoomRatio * 10) + 60,  (zoomRatio * 10) + 40];
				}
			}
			canvasObj.requestRenderAll();
			//console.log(zoomRatio);
		},

		'setAppStatus': function(status='', level='ok', popover="", show=false){ // level = ['ok', 'warning', 'error', critical]
			var levels = {
							ok		: 'fa-check-circle',
							info	: 'fa-info-circle',
							busy	: 'fa-circle-o-notch fa-spin fa-fw',
							warning	: 'fa-exclamation-circle',
							error	: 'fa-exclamation-circle text-danger blink',
							critical: 'fa-exclamation-triangle text-danger blink'
						}

			if(popover != "") {
				var statusStr = '<a id="app_status_popover" tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-html="true" data-placement="top" data-animation="true" data-content="<i class=\'fa ' + levels[level] + '\' aria-hidden=\'true\'></i> ' + popover + '"><i class="fa ' + levels[level] + '" aria-hidden="true"></i> ' + status + '</a>';
			} else {
				var statusStr = '<div><i class="fa ' + levels[level] + '" aria-hidden="true"></i> ' + status + '</div>';
			}

			$('#app_status').html(statusStr);
			$('#app_status_popover').popover();

			if(popover != "" && show) {
				$('#app_status_popover').popover('show');
				setTimeout(function(){
					$('#app_status_popover').popover('hide');
				}, 3000);
			}
		},

		'loadingDialog': function(msg="Processing...") {
			swal({
	  				html:'<i class="fa fa-circle-o-notch fa-spin fa-fw"></i> ' + msg + ' <span class="ft-progress"></span>',
	  				showCancelButton: false,
	  				showConfirmButton: false,
	  				allowEscapeKey: false,
	  				allowOutsideClick: false,
	  				padding: 30,
	  			});
		},

		'hideSwal': function() {
			 if(swal.isVisible()){
	            swal.closeModal();
	        }
		},

		'logError': function (log_entry) {
			var date = new Date()
			var log_string = "========================= Renderer process ==========================\n";
			log_string += date.toISOString() + ": \n"; 
			log_string += log_entry + "\n\n";

			fs.appendFile(ftErrorLogFile, log_string, (err) => {

			})
		},

		'availableControls': {
			image: ['src', 'scale', 'angle', 'centeredRotation', 'cropX', 'cropY', 'flipX', 'flipY', 'opacity', 'selectable'],
			text: ['fontFamily'],
		},

		'enableFilters': function() {
			$('#brightness').slider('enable');
			$('#contrast').slider('enable');
			$('#saturation').slider('enable');
			$('#blur').slider('enable');

			$('#enable_grayscale').prop('disabled', false);
			$('#enable_blur').prop('disabled', false);
			$('#enable_filters').prop('checked', true);
		},

		'disableFilters': function() {
			$('#brightness').slider('refresh');
			$('#brightness_value').val(0);
			$('#brightness').slider('disable');

			$('#contrast').slider('refresh');
			$('#contrast_value').val(0);
			$('#contrast').slider('disable');

			$('#saturation').slider('refresh');
			$('#saturation_value').val(0);
			$('#saturation').slider('disable');

			$('#blur').slider('refresh');
			$('#blur_value').val(0);
			$('#blur').slider('disable');

			$('#grayscale').slider('refresh');
			$('#grayscale').slider('disable');
			$('#enable_grayscale').prop('checked', false);
			$('#enable_grayscale').prop('disabled', true);

			$('#blur').slider('refresh');
			$('#blur').slider('disable');
			$('#enable_blur').prop('checked', false);
			$('#enable_blur').prop('disabled', true);

			$('#hue').slider('refresh');
			$('#hue_value').val(0);
			$('#hue').slider('disable');

			$('#enable_filters').prop('checked', false);

			$.each(adjustableFilters, function(k, v){
				filters[k] = null;
			});
		},

		'lockControls': function(obj) {
			obj.lockMovementX 	= true;
		    obj.lockMovementY 	= true;
		    obj.lockScalingX 	= true;
		    obj.lockScalingY 	= true;
		    obj.lockRotation 	= true;
		},

		'makeid': function() {
		  var text = "";
		  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

		  for (var i = 0; i < 10; i++)
		    text += possible.charAt(Math.floor(Math.random() * possible.length));

		  return text;
		},

		'selectLayer': function(oCanvas, ftName){
			var cavasObjects = oCanvas.getObjects();

			$.each(cavasObjects, function(k, obj){
				//console.log(obj.ftObjectName != undefined && obj.ftObjectName == ftName);
				if(obj.ftObjectName != undefined && obj.ftObjectName == ftName) {
					oCanvas.setActiveObject(obj);
				}
			});
		},

		'toggleLockByFTName': function(oCanvas, ftName) {
			var cavasObjects = oCanvas.getObjects();

			$.each(cavasObjects, function(k, obj){
				if(obj.ftObjectName != undefined && obj.ftObjectName == ftName) {
					console.log(obj.selectable);
					var isSelectable = !obj.selectable;
					obj.selectable = isSelectable;
				} 
			});
		},

		'toggleObjectByFTName': function(oCanvas, ftName) {
			var canvasObjects = oCanvas.getObjects();

			$.each(canvasObjects, function(k, obj){
				//console.log(obj.ftObjectName != undefined && obj.ftObjectName == ftName);
				if(obj.ftObjectName != undefined && obj.ftObjectName == ftName) {
					var visible = !obj.visible;
					obj.visible = visible;
				}
			});

			oCanvas.renderAll();
		},

		'setUsedUnusedCount': function(unusedCount, usedCount) {
			$('.unused-count').html(unusedCount);
	  		$('.used-count').html(usedCount);
		},

		'applyImageFilters': function(canvas) {
		  canvas.forEachObject(function(obj) {
		    if (obj.type === 'image' && obj.filters.length) {
		      obj.applyFilters(function() {
		        obj.canvas.renderAll();
		      });
		    }
		  });
		},

		'hexToRGBA': function(hex, a){
		    var c;
		    if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
		        c= hex.substring(1).split('');
		        if(c.length== 3){
		            c= [c[0], c[0], c[1], c[1], c[2], c[2]];
		        }
		        c= '0x'+c.join('');
		        return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+',' + a + ')';
		    }
		    return false;
		},

		'enableShadow': function(){
			//$('#enable_shadow').prop('checked', true);
			$('#ft_shadow_spread_slider').slider('enable');
			$('#ft_shadow_offset_x_slider').slider('enable');
			$('#ft_shadow_offset_y_slider').slider('enable');
			$('#shadow_color').find('.diable-pannel').hide();

			var activeObject = canvas.getActiveObject();

			activeObject.shadow = {
				color: $('#shadow_colorpicker_val').colorpicker('getValue'),
				blur: $('#ft_shadow_spread_slider').slider('getValue'),
				offsetX: $('#ft_shadow_offset_x_slider').slider('getValue'),
				offsetY: $('#ft_shadow_offset_y_slider').slider('getValue')
			};

			canvas.renderAll();
		},

		'disableShadow': function() {
			var activeObject = canvas.getActiveObject();

			//$('#enable_shadow').prop('checked', false);

			$('#ft_shadow_spread_slider').slider('refresh');
			$('#ft_shadow_offset_x_slider').slider('refresh');
			$('#ft_shadow_offset_y_slider').slider('refresh');

			$('#ft_shadow_spread_slider').slider('disable');
			$('#ft_shadow_offset_x_slider').slider('disable');
			$('#ft_shadow_offset_y_slider').slider('disable');
			$('#shadow_color').find('.diable-pannel').show();

			activeObject.shadow = false;

			canvas.renderAll();
		},

		'getColorPallete': function(imgPath) {
			var img = document.createElement('img');
			img.setAttribute('src', imgPath)

			var colorPallete = {};
			img.addEventListener('load', function() {
			    var vibrant = new Vibrant(img, 64, 3);
			    var swatches = vibrant.swatches()
			    
			    for (var swatch in swatches) {

			        if (swatches.hasOwnProperty(swatch) && swatches[swatch]) {
			        	//console.log(swatch, swatches[swatch].getHex())
			        	colorPallete[swatch] = swatches[swatch].getHex()
			        }
			            
			    }
			    console.log(colorPallete);
			    
			    /*
			     * Results into:
			     * Vibrant #7a4426
			     * Muted #7b9eae
			     * DarkVibrant #348945
			     * DarkMuted #141414
			     * LightVibrant #f3ccb4
			     */
			});

			return colorPallete;
		},

		'resizeImage': function(srcWidth, srcHeight, maxWidth, maxHeight) {
		    var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);
		    return { width: srcWidth*ratio, height: srcHeight*ratio };
		 },

		'smoothThumbnailsLoader': function (files, callBack) {
			files = files.sort();
			var newFiles = [];
			var template = '';
  			$.each(files, function(index, filepath) {
  				if($.inArray(filepath, imageArray) == -1) {
  					newFiles.push(filepath);

  					var filename = path.basename(filepath);

  					template += '<div class="img-container-outer col-md-3 col-lg-3 col-sm-4 grid-item is-loading">';
	  				template += '<div class="img-container">';
		    		template += '</div>'; 
		    		template += '<div class="ft-truncate"><small>' + filename + '</small></div>';  
		    		template += '</div>';
  				}
  			});

  			if(newFiles.length > 0) {
  				$('#image_panel').removeClass('no-data');
  				$('#image_panel').append(template);

  				ft.loadingDialog(newFiles.length + " images are loading...");
				ft.setAppStatus(status="", level='busy', popover="");

				var thumbCanvas =  new fabric.StaticCanvas("thumb_canvas");

	  			var templateWithImg = "";
	  			var counter = 0;
	  			var imgCounter = imageArray.length + 1;

	  			var thumbsPath = path.join(getConfig('workspace'), projectJSON.name, 'thumbnail');
	  			if(!fs.existsSync(thumbsPath)) {
	  				fs.mkdirSync(thumbsPath);
	  			}

	  			newFiles = newFiles.sort();
	  			$.each(newFiles, function(index, filepath) {
	  				var filename = path.basename(filepath);

		    		var origImg = new Image();
				  	origImg.src = filepath;

		    		origImg.onload = function() {
				  		var newSize = ft.resizeImage(origImg.width, origImg.height, 500, 300);
				  		if(origImg.height > origImg.width) {
				  			newSize = ft.resizeImage(origImg.width, origImg.height, 200, 400);
				  		}
					  	origImg.width = newSize.width;
					  	origImg.height = newSize.height;
					  	//console.log(newSize.width + ' x ' + newSize.height);

					  	fabric.Image.fromURL(filepath, function(oImg){
					  		oImg.scaleToWidth(newSize.width);
					  		oImg.selectable = false;
					  		thumbCanvas.add(oImg);

					  		var dataUrl = thumbCanvas.toDataURL({
					  			//format: 'jpeg',
					  			//quality: 0.6,
					  			top: 0,
					  			left: 0,
					  			width: newSize.width,
					  			height: newSize.height
					  		});

					  		//var image = new Image();
							//image.src = dataUrl;
						  	//console.log(image);
						  	//$('#' + imgParentId).html(image);
						  	var rand_1 = ft.makeid();
							var rand_2 = Math.floor(Math.random() * 1000000);
							var rand_id = rand_1 + '_' + rand_2;

							templateWithImg += '<div class="img-container-outer col-md-3 col-lg-3 col-sm-4 grid-item">';
			  				templateWithImg += '<div id="' + rand_id + '" class="img-container" data-filepath="' + filepath + '">';
				    		templateWithImg += '<div class="img-count-number">' + imgCounter + '</div>';
				    		templateWithImg += '<img src="' + dataUrl + '" />';	
				    		templateWithImg += '</div>'; 
				    		templateWithImg += '<div class="ft-truncate"><small>' + filename + '</small></div>';  
				    		templateWithImg += '</div>';

				    		imageArray.push(filepath);

		    				filesObject[rand_id] = {path: filepath, use_count: 0};

		    				var thumbJsonFile = path.join(thumbsPath, rand_id + '.json');
		    				fs.writeFileSync(thumbJsonFile, JSON.stringify({data: dataUrl}) , 'utf8');

						  	thumbCanvas.clear();
						  	counter++;		
						  	imgCounter++;				  	

						  	var percentage = parseInt((counter / newFiles.length)*100);
							$('.progress').show();
							$('.ft-progress').html(percentage + "%");
							$('.progress-bar').css('width', percentage + '%');

						  	if(newFiles.length == counter) {
						  		$('#image_panel').find('.is-loading').remove();
			  					$('#image_panel').append(templateWithImg);

			  					projectJSON.files = filesObject;
			  					var albumSize = ft.currentCanvasDim();
								var templSize = albumSize.canvasHeight + "x" + albumSize.canvasWidth;
						    	projectJSON.album_size = templSize;
						    	projectJSON.dpi = $('#dpi').val();
							  	
							  	$('.added-count').html(imageArray.length);

			  					// run callback function if available
			  					if(typeof callBack == "function") callBack(templateWithImg);

			  					thumbCanvas.dispose();

			  					var imgPanelWidth = Math.round((parseFloat($('#image_panel').find('.img-container-outer').css('width'))/$(window).width())*100);
			  					//console.log('Image panel width:', imgPanelWidth);
			  					$("#thumb_sizer").slider('setValue', parseInt(imgPanelWidth), true);
			  					$("#thumb_sizer").slider('enable');
						  	}
					  	},
					  	{crossOrigin: 'anonymous'});
		
				  	} 
	  			});

  			}
					  	
		}, 

		'smoothThumbnailsLoader2': function(files, callback) {
			ft.loadingDialog("Images are loading...");
			ft.setAppStatus(status="", level='busy', popover="");

			var template = "";
			$.each(files, function(index, file) {
				var filepath = file.path;
				var filename = path.basename(filepath);

				if(!fs.existsSync(filepath)) {
  					delete(files[index]);
  				} else {
  					template += '<div class="img-container-outer col-md-3 col-lg-3 col-sm-4 grid-item is-loading">';
	  				template += '<div class="img-container">';
		    		template += '</div>'; 
		    		template += '<div class="ft-truncate"><small>' + filename + '</small></div>';  
		    		template += '</div>';
  				}
  			});

			//console.log('images length ', Object.keys(files).length);
			if(Object.keys(files).length == 0) {
				ft.setAppStatus(status="", level='ok', popover="");
				ft.hideSwal();
				ipc.send('close-splash');
			} else {
				$('#image_panel').removeClass('no-data');
	  			$('#image_panel').append(template);

	  			var template_added = template_unused = template_used = '';
	  			var thumbCanvas =  new fabric.StaticCanvas("thumb_canvas");
	  			var counter = 0;
	  			var imgCounter = imageArray.length + 1;

	  			var thumbsPath = path.join(getConfig('workspace'), projectJSON.name, 'thumbnail');

	  			$.each(files, function(k, v) {
	  				var origImg = new Image();
				  	var filepath = v.path;
				  	var rand_id = k;
					var filename = path.basename(filepath);

					var thumbJSONFile = JSON.parse(fs.readFileSync(path.join(thumbsPath, rand_id + '.json'),  'utf8'));
				  	var dataUrl = thumbJSONFile.data;

					var selectedImgClass = '';
					if(projectJSON.files[rand_id].is_added == 1) {
						selectedImgClass = ' selected-img';
					}

					template_added += '<div class="img-container-outer col-md-3 col-lg-3 col-sm-4 grid-item">';
	  				template_added += '<div id="' + rand_id + '" class="img-container' + selectedImgClass + '" data-filepath="' + filepath + '">';
		    		template_added += '<div class="img-count-number">' + imgCounter + '</div>';
		    		template_added += '<img src="' + dataUrl + '" />';	
		    		template_added += '</div>'; 
		    		template_added += '<div class="ft-truncate"><small>' + filename + '</small></div>';  
		    		template_added += '</div>';

		    		imageArray.push(filepath);

		    		// add files to storyboard (unused fiels)
		    		if(projectJSON.files[rand_id].is_added == 1) {
		    			var hiddenClass = '';
		    			var hiddenClassUsed = ' hidden';
		    			if(parseInt(projectJSON.files[rand_id].use_count) > 0) {
		    				hiddenClass = ' hidden';
		    				hiddenClassUsed = '';
		    				
		    				usedFiles.push(rand_id);
		    			} else {
		    				unusedFiles.push(rand_id);
		    			}

		    			template_unused += '<div class="img-container-outer col-md-3 col-lg-3 col-sm-6' + hiddenClass + '">';
						template_unused += '<div class="remove-unused" data-file-id="' + rand_id + '" title="remove"><i class="fa fa-times"></i></div>';
						template_unused += '<div class="dev-img-container unused-' + rand_id + '" data-randid="' + rand_id + '" data-filepath="' + filepath + '">';
			    		template_unused += '<img src="' + dataUrl + '" />';	
			    		template_unused += '</div>'; 
			    		template_unused += '<div class="ft-truncate"><small>' + filename + '</small></div>';  
			    		template_unused += '</div>';

			    		// add files to storyboard (used files)
			    		template_used += '<div class="img-container-outer col-md-4 col-lg-4 col-sm-6' + hiddenClassUsed + '">';
						template_used += '<div class="dev-img-container used-' + rand_id + '" data-randid="' + rand_id + '" data-filepath="' + filepath + '">';
						template_used += '<span class="badge use-count-badge" title="' + projectJSON.files[rand_id].use_count + ' time(s) used">' + projectJSON.files[rand_id].use_count + '</span>';
						template_used += '<img src="' + dataUrl + '" />';	
						template_used += '</div>'; 
						template_used += '<div class="ft-truncate"><small>' + filename + '</small></div>';  
						template_used += '</div>';

			    		selectedImages.push(rand_id);
		    		}

				  	//thumbCanvas.clear();
				  	counter++;
				  	imgCounter++;

				  	var percentage = parseInt((counter / Object.keys(files).length)*100);
					$('.progress').show();
					$('.ft-progress').html(percentage + "%");
					$('.progress-bar').css('width', percentage + '%');

				  	if(Object.keys(files).length == counter) {
				  		$('#image_panel').find('.is-loading').remove();
	  					$('#image_panel').append(template_added);
			  			$('#unused').find('.file-container').append(template_unused);
			  			$('#used').find('.file-container').append(template_used);

	  					//projectJSON.files = filesObject;
				    	//projectJSON.album_size = $('#canvas_size').data('size');
				    	//projectJSON.dpi = $('#dpi').val();
					  	
					  	$('.added-count').html(imageArray.length);
					  	$('.selected-count').html(selectedImages.length);

					  	ft.setUsedUnusedCount(unusedFiles.length, usedFiles.length);

					  	var imgPanelWidth = Math.round((parseFloat($('#image_panel').find('.img-container-outer').css('width'))/$(window).width())*100);
	  					//console.log('Image panel width:', imgPanelWidth);
	  					$("#thumb_sizer").slider('setValue', parseInt(imgPanelWidth), true);
	  					$("#thumb_sizer").slider('enable');

	  					// run callback function if available
	  					if(typeof callback == "function") callback(template_added);
				  	}
	  			});
			}
		},

		'changeWorkspace': function() {
			dialog.showMessageBox(
				{
					buttons: ['Yes, go ahead', 'No, go back'], 
					type: 'question', 
					title: 'Change workspace',
					message: 'Change workspace location.',
					detail: 'You are going to change the workspace. Are you sure?',
					}, function(btnIndexConfirm){
						if(btnIndexConfirm == 0) {
							dialog.showOpenDialog({
							    properties: ['openDirectory', 'createDirectory', 'promptToCreate']
						  	}, function (dir_path) {
							    if(dir_path == undefined) {
							    	return false;
							    } else {
							    	if(setConfig({'workspace': dir_path})) {
							    		dialog.showMessageBox({buttons: ['OK'], type: 'info', title: 'Change workspace', message: 'Workspace changed to following path.\n' + dir_path + '\nIf you wish to use the projects from previous workspace, you may move them to new workspace.'},
							    			function(btnIndex){
							    				app.relaunch();
							    				app.exit(0);
							    			});
							    	}
							    }
						  	});
						} else {
							return;
						}
					});
			
		},

		'setLayersList': function(oCanvas) {
			var layers_temp = '';
			var canvasObjects = oCanvas.getObjects();

			$('#layers').find('.layers-panel').html('<div class="layer-item active"><div class="layer-name" data-layer-name="canvas_bg">background</div></div>');

			$.each(canvasObjects, function(k, obj){
				//console.log(obj.ftObjectName + " " + obj.ftEditable);
				if(obj.ftEditable != undefined && obj.ftEditable) {
					layers_temp = '<div class="layer-item">';
					layers_temp += '<div class="layer-name" data-layer-name="' + obj.ftObjectName + '">' + obj.ftObjectName + '</div>';
					layers_temp += '<div class="actions">';
					layers_temp += '<button class="btn btn-xs layer-action-lock layer-' + obj.clipBoxName + '" data-status="' + LOCK_OPEN + '" data-layer-name="' + obj.ftObjectName + '"><i class="fa fa-unlock" aria-hidden="true"></i></button>';
					layers_temp += '<button class="btn btn-xs layer-action-showhide" data-status="' + VISIBLE_TRUE + '" data-layer-name="' + obj.ftObjectName + '"><i class="fa fa-eye" aria-hidden="true"></i></button></div></div>';

					$('#layers').find('.layers-panel').prepend(layers_temp);
				}
			});
		},

		'resetCanvas': function(oCanvas) {
			oCanvas.clear();
		},

		'saveProject': function(oCanvas, isNew=false, showMsg=false) {
			var projectSaveName = $('#project_name').val();
			if(!isNew && projectSaveName == ""){
				$('#project_name').popover('show');
				$('#project_name').focus();
			} else {
				$('#project_name').popover('hide');

				if(isNew) {
					//projectJSON = ft.projectJSON;
					projectJSON.files = {};
					projectSaveName = $('#ft_project_name').val();
					projectJSON.template = "";
					$('#image_panel').html('');
					$('#image_panel').addClass('no-data');
					$('#unused').find('.file-container').html('');
					$('#used').find('.file-container').html('');
					imageArray = [];
				    filesObject = {};
					selectedImages = [];
				    unusedFiles = [];
				    usedFiles = [];
				}

				// send data to Main process for saving
				projectJSON.name = projectSaveName;
				var cavasJSON = canvas.toJSON(customJSONOptions);
				var currentPage = $('#selected_page').val();

				ipc.send('save-project', JSON.stringify({projectJSON: projectJSON, cavasJSON: cavasJSON, currentPage: currentPage, isNew: isNew}));
				ipc.on('save-project-resp', function(event, args){
					var resp = JSON.parse(`${args}`);
			  		var respStatus = resp.status;
			  		var respMsg = resp.msg;
			  		if(respStatus == 'ok'){
			  			if(isNew) {
			  				window.location.reload();
			  			}
			  			$('#project_name').val(projectName);
						$('#canvasSizeModal').modal('hide');

			  			if(showMsg === true) {
			  				ft.setAppStatus(status="", level='ok', popover=respMsg, show=true);
			  			}			  			
			  			$('#project_name').val(projectSaveName);
			  			$('#project_name').prop('readonly', true);
			  		} else {
			  			if(resp.errcode == 'EEXIST') {
			  				dialog.showMessageBox(
			  					options={buttons: ['OK'], type: 'info', title: 'New project',	message: 'Project name is already taken.', detail: 'This project name is already exists. Please enter another name.'},
			  					function(buttonIndex) {
			  						$('#ft_project_name').focus();
			  						$('.project-setup-btn').prop('disabled', false);
			  					}
			  				);
			  			} else {
			  				ft.setAppStatus(status="", level='error', popover=respMsg);
			  			}
			  		}

			  		ipc.removeAllListeners('save-project-resp');
				});
			}
		},

		'addGuileLines': function(canvasObj) {
		 	var canvasObjects = canvasObj.getObjects();

		 	var gline = false;
		 	$.each(canvasObjects, function(k, obj){
		 		if(obj != undefined && obj.ftObjectName == 'center guidelines') {
		 			gline = true;
		 			obj.visible = !obj.get('visible');
		 		}

		 		if(obj != undefined && (obj.ftObjectName == 'center guideline h' || obj.ftObjectName == 'center guideline v')) {
		 			canvasObj.remove(obj);
		 		}
		 	});

		 	if(!gline) {
		 		var vGline = new fabric.Line([canvas.width/2, 0, canvas.width/2, canvas.height], {
					ftEditable: false,
					ftExportable: false,
					originX: 'center',
					originY: 'center',
			        stroke: '#00edfa',
			        strokeWidth: canvas.width/1080,
			        strokeDashArray: [100, 10],
			        selectable: false,
			        opacity: 1,
			        evented: false
			    });

		 		var hGline = new fabric.Line([0, canvas.height/2, canvas.width, canvas.height/2], {
					ftEditable: false,
					ftExportable: false,
					originX: 'center',
					originY: 'center',
			        stroke: '#00edfa',
			        strokeWidth: canvas.width/1080,
			        strokeDashArray: [100, 10],
			        selectable: false,
			        opacity: 1,
			        evented: false
			    })

		 		var gLineGroup = new fabric.Group([vGline, hGline], {
		 			left: 0,
		 			top: 0,
		 			ftObjectName: 'center guidelines',
		 			ftEditable: false,
					ftExportable: false,
		 			evented: false
		 		})
		 		canvasObj.add(gLineGroup);
		 	}

		 	canvasObj.requestRenderAll();
		 },

		'switchProjects': function(switchProjectName) {
			dialog.showMessageBox(options={buttons: ['Yes', 'No'], type:'question', title:'Switch project', message: 'Do you want to open this project?', detail: 'You are going to open the project "' + switchProjectName + '". Are you sure?'},
				function(buttonIndex){
					if(buttonIndex == 0) {					
						ft.loadingDialog("Project is loading...");

						var switchProjectResp = switchProject(switchProjectName);
						//console.log(switchProjectResp);
						if(switchProjectResp) {
							location.reload();
						} else {
							ft.hideSwal();
							dialog.showMessageBox(options={buttons: ['OK', 'Quit'], type: 'error', title: 'Open project', message:'Open project failed.'},
								function(btnIndex){
									if(btnIndex == 1) {
										app.quit();
									}
								}
							);
						}
					}
				}
			);
		},

		'progress': function(p) {
			$('.progress').show();
			$('.ft-progress').html(p + '%');
			$('.progress-bar').css('width', p + '%');
		},

		'resetProgress': function(){
			$('.progress').hide();
			$('.ft-progress').html('');
			$('.progress-bar').css('width', '0%');
		},

		bytesConverter: function(a,b){if(0==a)return"0 Bytes";var c=1e3+24,d=b||2,e=["Bytes","KB","MB","GB","TB","PB","EB","ZB","YB"],f=Math.floor(Math.log(a)/Math.log(c));return parseFloat((a/Math.pow(c,f)).toFixed(d))+" "+e[f]},
	}

	// check for illegally added templates
	var templString = getTemplatesString()
	var currentTmplKey = getConfig('tmplKey')

	if(tempSecret(templString) !== currentTmplKey) {
		dialog.showMessageBox(remote.getCurrentWindow(), {
			buttons: ['Quit'],
			type: 'warning',
			title: 'Illegal action',
			message: 'Illegal action detected',
			detail: 'Illegal action or Terms and Conditions violation detected.\nIt is prohibited to install PhotoBook Templates manually or using any third party software.\nIt must be done through our website http://photobook.lk only.\nIn these conditions, you cannot use PhotoBook app anymore.\nBut don\'t worry. Just remove those items you added illegally. You will be just fine.\nIf you did not commit any illegal action, please contact PhotoBook as soon as possible.'
		}, function(btnIndex){
			app.exit(0)
		})

		return;
	}

	projectJSON = ft.projectJSON;

	ipc.send('get-ft-config');
	ipc.on('config-data', function(event, arg) {
		var resp = JSON.parse(`${arg}`);
		//console.log(resp)
		var defaultWorkspace = resp.defaultWorkspace;
		var ftConfigData = JSON.parse(resp.data);
		var projects = resp.userProjects;
		var activationCode = resp.activationCode;
		var appName = ftConfigData.app_label != null && ftConfigData.app_label != "" ? ftConfigData.app_label : "<a href='#' class='btn btn-xs set-app-name-btn'>set app name</a>";

		$('#app_name_label').html(appName);

		if(activationCode && activationCode != "") {
			$('.activation-sign').html('<i class="fa fa-check-circle fa-2x tf-text-sccess"></i>');
			$('.activation-sign').attr('title', 'App is active');
		} else {
			$('#app_name_label').html('');
			$('.activation-sign').html('<i class="fa fa-exclamation-circle fa-2x ft-text-warning"></i>');
			$('.activation-sign').attr('title', 'App is on trial/inactive');
		}

		var projectNames = ftConfigData.projects;
		var recentProjects = [];
		for(var k=(projectNames.length-1); k >= (projectNames.length - 5); k--) {
			if(projectNames[k] != undefined) {
				recentProjects.push({label: projectNames[k], click: (event, item) => ft.switchProjects(event.label)})
			}
		};

		if(!$.isEmptyObject(recentProjects)) {
			menuTemplate[0].submenu[1].submenu = recentProjects;
			menu = Menu.buildFromTemplate(menuTemplate)
			Menu.setApplicationMenu(menu)
		}
		

		//console.log(templates);		
		projectName = ftConfigData.current_project;
		
		if($.isEmptyObject(projects)) {
			$('#project_list').addClass('no-data');
		} else {
			// projects list
			var projInfoTemp = "<div class='container-fluid'>";
			$.each(projects, function(proName, projectObj){
				var pagesObj = projectObj.pages;
				var overallStatus = " status_1";
				var overallStatusTitle = "not completed. " + (pagesObj.total - pagesObj.exported) + " pages left.";
				if(pagesObj.total == pagesObj.exported) {
					overallStatus = ' status_2';
					overallStatusTitle = "completed";
				}

				var currentProjectClass = '';
				var disableDeleteBtn = disabledOpenBtn = '';
				if(proName == projectName) {
					currentProjectClass = ' ft-selected-card';
					disableDeleteBtn = disabledOpenBtn = "disabled='disabled'";
				}

				projInfoTemp += "<div class='col-lg-3 col-md-3'>";
				projInfoTemp += "<div class='row'>";
				projInfoTemp += "<div class='ft-card ft-project-card" + currentProjectClass + "' data-project-name='" + proName + "'>";
				projInfoTemp += "<div class='ft-card-header ft-truncate' title='" + proName + "'>" + proName + "</div><div class='proj-status" + overallStatus + "' title='" + overallStatusTitle + "'></div>";
				projInfoTemp += "<hr/>";
				projInfoTemp += "<div class='ft-card-body'>";
				projInfoTemp += "<div class='ft-card-item'><label>Size:</label> <span>" + projectObj.album_size + "</span></div>";
				projInfoTemp += "<div class='ft-card-item'><label>Template:</label> <span class='ft-tmpl-name'>" + getTemplateName(projectObj.template) + "</span></div>";
				projInfoTemp += "<div class='ft-card-item'><label>Date:</label> <span>" + projectObj.createDate + "</span></div>";
				projInfoTemp += "<div class='ft-card-item'>" + 
									"<div class='pull-left'><span class='badge' title='Total pages'><i class='fa fa-book' aria-hidden='true'></i> " + pagesObj.total + "</span><span class='badge status_1' title='Not exported'>" + pagesObj.edit + "</span><span class='badge status_2' title='Exported'>" + pagesObj.exported + "</span></div>" +
									"<div class='pull-right text-right'><a class='btn btn-defalt btn-sm switch-project-btn' title='Open project' " + disabledOpenBtn + "><i class='fa fa-folder-open-o' aria-hidden='true'></i></a><a class='btn btn-default btn-sm delete-project-btn' title='delete project' " + disableDeleteBtn + "><i class='fa fa-trash-o' aria-hidden='true'></i></a></div>" + 
								"</div>";
				projInfoTemp += "</div></div></div></div>";
				
			});
			projInfoTemp += "</div>";

			$('#project_list').html(projInfoTemp);
		}

		var projectJSONData = {};
		if(!$.isEmptyObject(resp.projectJSON)) {
			projectJSONData = JSON.parse(resp.projectJSON);
		}

		if (ftConfigData == null) {
			console.log('no config file')
		} else {
			if(!$.isEmptyObject(projectJSONData)) { // existing project
				projectJSON = projectJSONData;
				filesObject = projectJSON.files;
				// load data from unfinished project
				if(projectJSON.name != "" && !$.isEmptyObject(projectJSON.files)) {
					ft.smoothThumbnailsLoader2(projectJSON.files, function(template){
						$('#image_panel').imagesLoaded(function(){
			  				arrangeImages(delay=300, append=true, elements=template);
			  				arrangeUnusedImages(delay=300, append=false, elements="");
			  				arrangeUsedImages(delay=300, append=false, elements="");
					        $('.ft-progress').html("");
					        $('.progress').hide();
					        ipc.send('close-splash');
					    })
					    .progress(function(instance, image) {
					    	//console.log(instance.progressedCount + ', ' + imageArray.length);
					    	/*var percentage = parseInt((instance.progressedCount / imageArray.length)*100);
					    	$('.progress').show();
							$('.ft-progress').html(percentage + "%");
							$('.progress-bar').css({'width': percentage + "%"});*/
						});
					});
				}

				// setup canvas size
				var projAlbumSize = (projectJSON.album_size).split("x");

				var dpi = projectJSON.dpi;
				ft.setCanvasSize(canvas, projAlbumSize[0], projAlbumSize[1], dpi);
				$('.canvas-zoom[data-zoom="reset"]').trigger('click');

				// create pages list
				var currentWorkingPage = ftConfigData.current_page;
				var projectPages = projectJSON.pages;
				//console.log(projectPages);
				var pagesTemp = "";
				var i = 1;
				$.each(projectPages, function(page, status){
					if(i == currentWorkingPage) {
						pagesTemp += "<div class='page-pannel-item status_" + status + " current' data-page='" + i + "'>" + i + "</div>";
						$('#selected_page').val(i);
					} else {
						pagesTemp += "<div class='page-pannel-item status_" + status + "' data-page='" + i + "'>" + i + "</div>";
					}
					
					i++;
				});

				$('#project_pages').find('.page-pannel').html(pagesTemp);

				/*// select template
				var selectedTemplate = projectJSON.template;
				if(parseInt(selectedTemplate) > 0) {
					var tmplName = templates[selectedTemplate];
					$('#template_list').find('div[data-tmpl-name="' + tmplName + '"]').addClass('selected');
				}*/

				// load canvas JSON
				if(!$.isEmptyObject(JSON.parse(resp.projectDataJSON))) {
					//ft.loadingDialog("Project is loading...");
	  				ft.setAppStatus(status="", level='busy', popover="");

					projectDataJSONObj = JSON.parse(resp.projectDataJSON);
					canvas.clear();
					canvas.loadFromJSON(projectDataJSONObj, function(){
						canvas.defaultCursor = 'default';
						canvas.hoverCursor = 'pointer';

						canvas.renderAll.bind(canvas);

						undoReady = true; // ready to start undo/redo

						$.each(canvas.getObjects(), function(k, obj){
							if(!$.isEmptyObject(obj.ftColorPallete)) {
			  					var colorPallete = obj.ftColorPallete;
			  					var colorPalleteTemplate = "";
								$.each(colorPallete, function(colname, hex){
									colorPalleteTemplate += "<div class='color-pallete-color' data-color='" + hex + "' style='background:" + hex + "'></div>";
								});

								colorPalleteTemplate += "<div class='color-pallete-color pick-a-color' title='pick a color'><div class='glyph-icon flaticon-pipette'></div></div>";

								$('#color_pallete').html(colorPalleteTemplate);
			  				}
						});

						//ft.hideSwal();
						ft.setLayersList(canvas);
						ft.setAppStatus(status="", level='ok', popover="");
						setTimeout(function(){	
							ipc.send('close-splash');
						}, 2000);
					});
				}				
			} else {
				//console.log('no project');
				//ipc.send('open_project_save_window');
				setTimeout(function(){	
					ipc.send('close-splash');
					$('#canvasSizeModal').modal('show');
				}, 2000);
				
			}		

			var templates = resp.templatesList;
			// templates list
			var templateTemp = "<div class='container-fluid'>";
			$.each(templates, function(tempId, tempObj){
				//console.log(tempObj);
				var templateId = tempObj.id;
				var templateName = tempObj.name;
				var templateSizes = tempObj.sizes;
				var tempPages = tempObj.pages;
				var tempPlaceholders = tempObj.noof_placeholders;
				var tempThumb = path.join(templatesPath, tempObj.id.toString(), '1_thumb.jpg');
				var tempThumbImg = "";
				if(fs.existsSync(tempThumb)) {
					tempThumbImg = "<img src='" + tempThumb + "' width='100%' />";
				}

				// sizes
				var tempSizeTemp = "";
				$.each(ft.albumSizes, function(k, albmSize){
					if($.inArray(albmSize, templateSizes) >= 0) {
						if($.inArray(albmSize, availableAlbumSizes) === -1) {
							availableAlbumSizes.push(albmSize);
						}
						tempSizeTemp += "<span class='badge available'>" + albmSize + "</span>";
					} else {
						tempSizeTemp += "<span class='badge'>" + albmSize + "</span>";
					}					
				});

				var currentTemplateClass = '';
				var disableUseBtn = disabledPreviewBtn = '';
				if(templateId == projectJSON.template) {
					currentTemplateClass = ' ft-selected-card';
					disableUseBtn = "disabled='disabled'";
				}

				var sizeNotAvailableClass = sizeNotAvailableText = "";
				if($.inArray(projectJSON.album_size, templateSizes) < 0) {
					sizeNotAvailableClass = " size-not-available";
					sizeNotAvailableText = "<span class='ft-text-warning'><i class='fa fa-exclamation-circle' aria-hidden='true'></i> size not available</span>";
					disableUseBtn = "disabled='disabled'";
				}

				templateTemp += "<div class='col-lg-3 col-md-3'>";
				templateTemp += "<div class='row'>";
				templateTemp += "<div class='ft-card template-item tmpl-id-class-" + templateId + currentTemplateClass + sizeNotAvailableClass + "' data-tmpl-name='" + templateName + "' data-tmpl-id='" + templateId + "'>";
				templateTemp += "<div class='ft-card-header ft-truncate' title='" + templateName + "'>" + templateName + "</div>";
				templateTemp += "<hr/>";
				templateTemp += "<div class='ft-card-body'>";
				templateTemp += "<div class='ft-card-item tmpl-thumb text-center no-image'>" + tempThumbImg + "</div>";
				templateTemp += "<div class='ft-card-item'><label>Sizes:</label> <div class='size-list'>" + tempSizeTemp + "</div></div>";
				templateTemp += "<div class='ft-card-item'><label>Placeholders:</label> <span>" + tempPlaceholders + "</span></div>";
				templateTemp += "<div class='ft-card-item text-right'>" + sizeNotAvailableText + " <a class='btn btn-default btn-sm temp-use-btn' " + disableUseBtn + " data-tmpl-id='" + templateId + "' data-tmpl-name='" + templateName + "' data-tmpl-pages='" + tempPages + "'>Use</a><a class='btn btn-default btn-sm temp-prev-btn' " + disabledPreviewBtn + " data-tmpl-id='" + templateId + "' data-tmpl-name='" + templateName + "' data-tmpl-pages='" + tempPages + "'>Preview</a></div>";
				templateTemp += "</div></div></div></div>";
			});
			templateTemp += "</div>";

			$('#template_list').html(templateTemp);
		}

		$('#project_name').val(projectName);
		$('.selected-count').html(selectedImages.length);

		ipc.removeAllListeners('config-data');
	});

	// preview templates
	$('#template_list').on('click', '.temp-prev-btn', function(e){
		e.preventDefault();
		e.stopPropagation();

		var templId = $(this).data('tmpl-id');
		var tempName = $(this).data('tmpl-name');
		var tmplPages = $(this).data('tmpl-pages');

		var parentTemlItem = $(this).closest('.template-item');

		var thisTemplatePath = path.join(templatesPath, templId.toString());

		var pagin = "";
		var slideItem = "";
		var activeItemClass = "";
		for(var n=0; n < tmplPages; n++){				
			var tumbImg = path.join(thisTemplatePath, (n+1) + '_thumb.jpg');

			if(!fs.existsSync(tumbImg)) {
				slideItem += '<div class="bb-item"><div class="no-image"><h3>Preview image not found!</h3></div></div>';
			} else {
				slideItem += '<div class="bb-item"><img src="' + tumbImg + '" alt="Page ' + (n+1) + '"></div>';
			}
		}

		if(parentTemlItem.hasClass('size-not-available') || templId == projectJSON.template) {
			$('#use_tmpl_btn_on_modal').prop('disabled', true);
		} else {
			$('#use_tmpl_btn_on_modal').prop('disabled', false);
			$('#use_tmpl_btn_on_modal').data('tmplitem', parentTemlItem);
		}

		$('#album_name_on_preview').html(tempName);
		$('#bb-bookblock').html(slideItem);

		$('#template_preview_modal').modal('show');
	});

	$('#template_preview_modal').on('shown.bs.modal', function(e){
		var Page = (function() {
				
			var config = {
					$bookBlock : $( '#bb-bookblock' ),
					$navNext : $( '#bb-nav-next' ),
					$navPrev : $( '#bb-nav-prev' ),
					$navFirst : $( '#bb-nav-first' ),
					$navLast : $( '#bb-nav-last' )
				},
				init = function() {
					config.$bookBlock.bookblock( {
						speed : 800,
						shadowSides : 0.8,
						shadowFlip : 0.7
					} );
					initEvents();
				},
				initEvents = function() {
					
					var $slides = config.$bookBlock.children();

					// add navigation events
					config.$navNext.on( 'click touchstart', function() {
						config.$bookBlock.bookblock( 'next' );
						return false;
					} );

					config.$navPrev.on( 'click touchstart', function() {
						config.$bookBlock.bookblock( 'prev' );
						return false;
					} );

					config.$navFirst.on( 'click touchstart', function() {
						config.$bookBlock.bookblock( 'first' );
						return false;
					} );

					config.$navLast.on( 'click touchstart', function() {
						config.$bookBlock.bookblock( 'last' );
						return false;
					} );
					
					// add swipe events
					$slides.on( {
						'swipeleft' : function( event ) {
							config.$bookBlock.bookblock( 'next' );
							return false;
						},
						'swiperight' : function( event ) {
							config.$bookBlock.bookblock( 'prev' );
							return false;
						}
					} );

					// add keyboard events
					$( document ).keydown( function(e) {
						var keyCode = e.keyCode || e.which,
							arrow = {
								left : 37,
								up : 38,
								right : 39,
								down : 40
							};

						switch (keyCode) {
							case arrow.left:
								config.$bookBlock.bookblock( 'prev' );
								break;
							case arrow.right:
								config.$bookBlock.bookblock( 'next' );
								break;
						}
					} );
				};

				return { init : init };

		})();

		Page.init();
	});

	$('#project_name').popover();

	$('.project-save-btn').on('click', function(e){
		e.preventDefault();
		ft.saveProject(canvas, isNew=false, true);
	});

	$('#project_name').on('blur', function(e){
		$('#project_name').popover('hide');
	});

	$('#dpi').val(dpi);

	$('#ft_maintabs a').click(function (e) {
	  	e.preventDefault()

	  	var tabId = e.currentTarget.hash.slice(1);
	  	var toolbarItemsToShow = 'ft-' + tabId;

	  	$('.toolbar-items').hide();
	  	$('.' + toolbarItemsToShow).show();
	  	$(this).tab('show')
	});

	$('#ft_maintabs a[href="#add_files"]').trigger('click');

	$('#ft_maintabs a').on('shown.bs.tab', function(e){
		if(e.relatedTarget) {
			ft.hideSwal();
		}
	});

	//fabric.filterBackend = new fabric.Canvas2dFilterBackend();
	fabric.Object.prototype.transparentCorners = false;
	fabric.Object.prototype.cornerColor = '#37474F';
	fabric.Object.prototype.borderColor = '#E91E63';
	fabric.Object.prototype.borderDashArray = [30,20];
	fabric.Object.prototype.padding = 10;
	fabric.textureSize = 50000;

    // ************** Image render method is extended for dynamic clipping support ****************
	fabric.Image.prototype._render = function(ctx) {
      // custom clip code
      	if (this.clipPath) {
	        ctx.save();
	        if (this.clipPath.fixed) {
	          var retina = this.canvas.getRetinaScaling();
	          ctx.setTransform(retina, 0, 0, retina, 0, 0);
	          // to handle zoom
	          ctx.transform.apply(ctx, this.canvas.viewportTransform);
	          //
	          this.clipPath.transform(ctx);
	        }
	        this.clipPath._render(ctx);
	        ctx.restore();
	        ctx.clip();
      	}
	      
      	// end custom clip code
    
    
      	var x = -this.width / 2, y = -this.height / 2, elementToDraw;

      	if (this.isMoving === false && this.resizeFilter && this._needsResize()) {
        	this._lastScaleX = this.scaleX;
        	this._lastScaleY = this.scaleY;
        	this.applyResizeFilters();
      	}
      	elementToDraw = this._element;
      	elementToDraw && ctx.drawImage(elementToDraw,
                                     0, 0, this.width, this.height,
                                     x, y, this.width, this.height);
      	this._stroke(ctx);
      	this._renderStroke(ctx);
    };


	var ftCanvas = (function() {
	  	var _canvasObject = new fabric.Canvas('ft_canvas', 
	  			{
	  				selection : false,
	  				backgroundColor: 'transparent',
	  				svgViewportTransformation: true,
	  				enableRetinaScaling: true,
	  				stateful: false,
	  				preserveObjectStacking: true,
	  				renderOnAddRemove: false
	  			});

		var _config = {
			canvasState             : [],
			currentStateIndex       : -1,
			undoStatus              : false,
			redoStatus              : false,
			undoFinishedStatus      : 1,
			redoFinishedStatus      : 1,
	    	undoButton              : document.getElementById('undo'),
			redoButton              : document.getElementById('redo'),
			historyStackLimit		: 50
		};
		_canvasObject.on(
			'object:modified', function(){
		  		updateCanvasState();
			}
		);
	  
	  _canvasObject.on(
			'object:added', function(){
		  		updateCanvasState();
			}
		);

	  _canvasObject.on(
			'object:moving', function(e){
				clearTimeout($.data(this, 'timer'));
			  	$.data(this, 'timer', setTimeout(function() {
			  		console.log('updateCanvasState fired');
			     	updateCanvasState();
			  	}, 250));		  		
			}
		);

	  _canvasObject.on(
	  		'after:render', function(){
	  			var cavasObjects = _canvasObject.getObjects();

				$.each(cavasObjects, function(k, obj){
					//console.log(obj.ftObjectName != undefined && obj.ftObjectName == ftName);
					if(obj.ftObjectName != undefined && (obj.ftObjectName == 'center guideline v' || obj.ftObjectName == 'center guideline h')) {
						obj.bringToFront();
					}

					if(obj.ftObjectType == "StaticObject") {
						obj.set({evented: false});
					}
				});

	  			ft.resizeControls(_canvasObject);
	  		}
	  	);

	  	_canvasObject.on('mouse:over', function(e) {
	  		if(e.target != undefined) {
  				//e.target.set({'stroke': '#69a4bd', 'strokeWidth': 1});
		    	//_canvasObject.renderAll();
			}
	  	});

	  	_canvasObject.on('mouse:out', function(e) {
	  		if(e.target != undefined) {
	  			//e.target.set('stroke', '');
	    		//_canvasObject.renderAll();
	    	}	    	
	  	});

	  	var ctx = _canvasObject.getContext('2d');
	    _canvasObject.on('mouse:move', function(e) {
	    	var pointer = _canvasObject.getPointer(e.e);
	    	var posX = Math.round(pointer.x);
	    	var posY = Math.round(pointer.y);

	        $('#x_coord').html(posX);
		  	$('#y_coord').html(posY);

		  	if(pickAColor) {
		  		// get the color array for the pixel under the mouse
			  	var px = ctx.getImageData(posX, posY, 1, 1).data;
			  	// report that pixel data
			  	var rgba = 'rgba(' + px[0] + ',' + px[1] + ',' + px[2] + ',1)';
			  	$('.pick-a-color').css('background', rgba);
			  	$('#picked_color').val(rgba);
		  	}		  	
	    });

	    _canvasObject.on('mouse:down', function(e){
	    	if(pickAColor) {
	    		//console.log($('#picked_color').val());
	    		_canvasObject.backgroundColor = $('#picked_color').val();
	    		$('#fill_colorpicker_val').colorpicker('setValue', $('#picked_color').val());
	    		_canvasObject.renderAll();
	    	}
	    });

	  	_canvasObject.on('object:selected', function(e) {
	  		//console.log(e.target.type);	  		 		
	  		if(e.target != undefined) {
	  			$ActiveObject = e.target;

	  			if($ActiveObject.fixed) {
	  				$ActiveObject.hasControls = false;
	  			}

	  			$('.fixed-filter').removeClass('active-filter');
	  			//filters[6] = null;
	  			//$('#ft_image_editor').removeClass('dragscroll');
	  			//dragscroll.reset();

	  			// select layers panel
	  			var ftObjName = $ActiveObject.ftObjectName;
	  			//console.log(ftObjName);
	  			$('#properties_and_layers').find('.layer-item').removeClass('active');
	  			$('#properties_and_layers').find('div[data-layer-name="' + ftObjName + '"]').parent('.layer-item').addClass('active');

	  			// set active object for undo

	  			$('#fill_color').find('.diable-pannel').hide();

	  			var objType = e.target.type;
	  			$('.tb-controls').fadeOut(100, function(){
	  				$('.type-' + objType).fadeIn(100);
	  				$('.type-all').show();
	  			});	

	  			$('.flip-btn').prop('disabled', true);
	  			if(objType == 'image') {
	  				$('.flip-btn').prop('disabled', false);
	  				$('#fill_color').find('.diable-pannel').show();

	  				var imageFilters = e.target.filters;
	  				var fltDisabledCount = 0;
	  				$.each(adjustableFilters, function(k, v){
	  					if(imageFilters[k] == null || imageFilters[k] == undefined){
	  						fltDisabledCount++;
	  					}
	  				});

	  				if(e.target.filters.length > 0 && fltDisabledCount < adjustableFilters.length) {
	  					ft.enableFilters();

	  					var brightnessFilter = getFilter(0);
		  				if(brightnessFilter && brightnessFilter != undefined) {
		  					$('#brightness').slider('setValue', brightnessFilter.brightness, true);
		  					$('#brightness_value').val(parseInt(brightnessFilter.brightness*100));
		  				}	

		  				var contrastFilter = getFilter(1);
		  				if(contrastFilter && contrastFilter != undefined) {
		  					$('#contrast').slider('setValue', contrastFilter.contrast, true);
		  					$('#contrast_value').val(parseInt(contrastFilter.contrast*100));
		  				}

		  				var saturationFilter = getFilter(2);
		  				if(saturationFilter && saturationFilter != undefined) {
		  					$('#saturation').slider('setValue', saturationFilter.saturation, true);
		  					$('#saturation_value').val(parseInt(saturationFilter.saturation*100));
		  				}

		  				var grayscaleFilter = getFilter(3);
		  				if(grayscaleFilter && grayscaleFilter != undefined) {
		  					//console.log(grayscaleMode.indexOf(grayscaleFilter.mode));
		  					$('#enable_grayscale').prop('checked', true);
		  					$('#grayscale').slider('enable');
		  					$('#grayscale').slider('setValue', grayscaleMode.indexOf(grayscaleFilter.mode), true);
		  				}	

		  				var blurFilter = getFilter(4);
						console.log("Blur value:", blurFilter);
		  				if(blurFilter && blurFilter != undefined && blurFilter.blur != undefined) {
		  					$('#enable_blur').prop('checked', true);
		  					$('#blur').slider('enable');
		  					
		  					$('#blur').slider('setValue', blurFilter.blur, true);
		  					$('#blur_value').val(parseInt((blurFilter.blur)*100));
		  				}

		  				var hueFilter = getFilter(5);		  				
		  				if(hueFilter && hueFilter != undefined) {
		  					$('#hue').slider('enable');
		  					$('#hue').slider('setValue', hueFilter.rotation, true);
		  					$('#hue_value').val(parseInt(hueFilter.rotation*100));
		  				}
	  				} else {
	  					ft.disableFilters();
	  				}

	  				// pre-builr filters
	  				var fixedFilter = getFilter(6);
	  				if(fixedFilter && fixedFilter != undefined) {
	  					$('.flt-' + fixedFilter.type).addClass('active-filter');
	  				}

	  				if(!$.isEmptyObject($ActiveObject.ftColorPallete)) {
	  					var colorPallete = $ActiveObject.ftColorPallete;
	  					var colorPalleteTemplate = "";
						$.each(colorPallete, function(colname, hex){
							colorPalleteTemplate += "<div class='color-pallete-color' data-color='" + hex + "' style='background:" + hex + "'></div>";
						});

						colorPalleteTemplate += "<div class='color-pallete-color pick-a-color' title='pick a color'><div class='glyph-icon flaticon-pipette'></div></div>";

						$('#color_pallete').html(colorPalleteTemplate);
	  				}

	  			}

	  			if(e.target.type == 'textbox') {
	  				var textbox = e.target;
	  				if(textbox.fontWeight != undefined && textbox.fontWeight == 'bold') {
	  					$('.ft-text-bold').removeClass('ft-active').addClass('ft-active');
	  				} else {
	  					$('.ft-text-bold').removeClass('ft-active');
	  				}
	  				if(textbox.fontStyle != undefined && textbox.fontStyle == 'italic') {
	  					$('.ft-text-italic').removeClass('ft-active').addClass('ft-active');
	  				} else {
	  					$('.ft-text-italic').removeClass('ft-active');
	  				}
	  				if(textbox.textAlign != undefined) {
	  					$('.ft-txtal').removeClass('ft-active')
	  					$('.ft-text-align-' + textbox.textAlign).addClass('ft-active');
	  				}
	  				if(textbox.fontSize != undefined) {
	  					$('#ft_font_size').val(textbox.fontSize);
	  					$('#ft_font_size_slider').slider('setValue', textbox.fontSize, false, true);
	  				}		
	  				if(textbox.lineHeight != undefined) {
	  					$('#ft_line_height').val(textbox.lineHeight);
	  					$('#ft_line_height').slider('setValue', textbox.lineHeight, false, true);
	  					$('#line_height_value').html(parseFloat(textbox.lineHeight).toFixed(2));
	  				}		
	  				if(textbox.fontFamily != undefined) {
	  					var fontLang = ft.findFontLang(textbox.fontFamily);
	  					//console.log(fontLang);
	  					var fontObj = ft.fonts[fontLang][textbox.fontFamily];
	  					var fontName = fontObj.fontFamily;
	  					var fontQuote = ft.fonts[fontLang].fontQuote;

	  					$('#ft_font_face').val(fontName);
	  					
				    	var styles = fontObj.styles;

				    	var styleBBadge = '<span class="badge inactive"><i class="fa fa-bold" aria-hidden="true"></i></span>';
				    	var styleIBadge = '<span class="badge inactive"><i class="fa fa-italic" aria-hidden="true"></i></span>';

				    	if(styles.indexOf('bold') >= 0) {
				    		styleBBadge = '<span class="badge active"><i class="fa fa-bold" aria-hidden="true"></i></span>';
				    	}

				    	if(styles.indexOf('italic') >= 0) {
				    		styleIBadge = '<span class="badge active"><i class="fa fa-italic" aria-hidden="true"></i></span>';
				    	}

	  					var fontTemplate = '<div class="font-item" data-font-family="' + fontName + '" data-style-bold="" data-style-italic="" title="keep this font">';
				    	fontTemplate += '<div class="font-name">' + fontName + '</div>';
				    	fontTemplate += '<div class="font-display" style="font-family:\'' + fontName + '\'">' + fontQuote + '</div>';
				    	//fontTemplate += '<div class="available-styles" title="available styles">' + styleBBadge + ' ' + styleIBadge + '</div>';
				    	fontTemplate += '</div>';

	  					$('#fonts_list .selected-font .content').html(fontTemplate);
	  					$('#fonts_tabs a[href="#' + fontLang + '_fonts"]').tab('show');

	  					$('.ft-textbox-content').css('font-family', "Monda");
	  					if(fontLang != 'en') {
	  						$('.ft-textbox-content').css('font-family', textbox.fontFamily);
	  					}
	  					
	  				}

	  				$('.ft-textbox-content').val(textbox.text);
	  				
	  			}

	  			if(e.target.ftObjectType != undefined) {
	  				$('#obj_prop_type').html(e.target.get('type') + ' (' + e.target.ftObjectType + ')');
	  				$('#obj_prop_size').html('W: ' + Math.floor(e.target.width*e.target.scaleX) + 'px&nbsp;&nbsp;H: ' + Math.floor(e.target.height*e.target.scaleY) + 'px');
	  				$('#obj_prop_position').html('Left: ' + Math.floor(e.target.left) + 'px&nbsp;&nbsp;Top: ' + Math.floor(e.target.top) + 'px');
	  				$('#obj_prop_angle').html(Math.round(e.target.angle) + '<sup>o</sup> (' + degToRad(e.target.angle).toFixed(2) + ' rad)');
	  			}

	  			// fill color
	  			if(e.target.fill != undefined) {
	  				if(typeof e.target.fill === 'object' || $ActiveObject.ftObjectType == "Placeholder") {
	  					$('#fill_color').find('.diable-pannel').show();
	  				} else {
	  					$('#fill_color').find('.diable-pannel').hide();
	  					$('#fill_colorpicker_val').colorpicker('setValue', e.target.fill);
	  				}	
	  			}
	  			
	  			// stroke color
				if(e.target.stroke != undefined && e.target.strokeWidth != undefined) {
					$('#stroke_colorpicker_val').colorpicker('setValue', e.target.stroke);
					$('#stroke_color').find('.diable-pannel').hide();					
					$('#ft_stroke_slider').slider('setValue', e.target.strokeWidth, true, true);
					$('#stroke_width_value').val(e.target.strokeWidth.toFixed(2));
				} else {
					$('#stroke_color').find('.diable-pannel').show();
					$('#ft_stroke_slider').slider('setValue', 0, true, true);
				}
				// shadow dolor
				if(e.target.shadow != undefined && e.target.shadow != false) {
					$('#enable_shadow').prop('checked', true);					
					$('#shadow_color').find('.diable-pannel').hide();

					$('#ft_shadow_spread_slider').slider('enable');
					$('#ft_shadow_offset_x_slider').slider('enable');
					$('#ft_shadow_offset_y_slider').slider('enable');

					$('#shadow_colorpicker_val').colorpicker('setValue', e.target.shadow.color);
					$('#ft_shadow_spread_slider').slider('setValue', e.target.shadow.blur, true);
					$('#ft_shadow_offset_x_slider').slider('setValue', e.target.shadow.offsetX, true);
					$('#ft_shadow_offset_y_slider').slider('setValue', e.target.shadow.offsetY, true);
				} else {
					$('#shadow_color').find('.diable-pannel').show();
					$('#enable_shadow').prop('checked', false);
					ft.disableShadow();
				}
	  		}
	  	});

	  	_canvasObject.on('before:selection:cleared', function(e){
	  		//$('#ft_image_editor').addClass('dragscroll');
	  		//dragscroll.reset();
	  		$ActiveObject = null;
	  		var objType = e.target.type;
	  		$('.type-' + objType).hide();
	  		$('.type-all').hide();
	  		$('.type-bg').show();
	  		$('#fonts_list').hide();
	  		$('.ft-prop-item').html('');	
	  		//ft.distroyColorPicker();  	
	  		$('#properties_and_layers').find('.layer-item').removeClass('active');	
  			$('#properties_and_layers').find('div[data-layer-name="canvas_bg"]').parent('.layer-item').addClass('active');
	  		$('.flip-btn').prop('disabled', true);

	  		$('#fill_color').find('.diable-pannel').hide();
			$('#fill_colorpicker_val').colorpicker('setValue', _canvasObject.backgroundColor);
			$('.fixed-filter').removeClass('active-filter');
	  	});

	  	var resetHistoryStack = function() {
			_config.canvasState 		= [];
			_config.currentStateIndex 	= -1;
			_config.undoStatus         	= false;
			_config.redoStatus          = false;
			_config.undoFinishedStatus  = 1;
			_config.redoFinishedStatus  = 1;
		}

		var updateCanvasState = function() {	
			if((_config.undoStatus == false && _config.redoStatus == false) && undoReady){
				var jsonData        = _canvasObject.toJSON(customJSONOptions);
				//console.log(jsonData);
				var canvasAsJson        = JSON.stringify(jsonData);
				//console.log(jsonData);
				if(_config.currentStateIndex < _config.canvasState.length-1){
					var indexToBeInserted                  = _config.currentStateIndex+1;
					_config.canvasState[indexToBeInserted] = canvasAsJson;
					var numberOfElementsToRetain           = indexToBeInserted+1;
					_config.canvasState                    = _config.canvasState.splice(0,numberOfElementsToRetain);
				} else {
		    		_config.canvasState.push(canvasAsJson);
				}

				if(_config.canvasState.length > _config.historyStackLimit) {
					_config.canvasState.length = _config.canvasState.slice((_config.canvasState.length - _config.historyStackLimit - 1),numberOfElementsToRetain);
				}

		    	_config.currentStateIndex = _config.canvasState.length-1;

		      	if((_config.currentStateIndex == _config.canvasState.length-1) && _config.currentStateIndex != -1){
		        	_config.redoButton.disabled= "disabled";
		      	}
			}

			console.log('History stake length', _config.canvasState.length);
		}
	 
		var undo = function() {
			//$currentActiveObject = _canvasObject.getActiveObject();
			if(_config.undoFinishedStatus){
				if(_config.currentStateIndex == -1){
		    	_config.undoStatus = false;
				} else{
				    if (_config.canvasState.length >= 1) {
		        	_config.undoFinishedStatus = 0;
				      if(_config.currentStateIndex != 0){
				    	_config.undoStatus = true;

				      	_canvasObject.loadFromJSON(_config.canvasState[_config.currentStateIndex-1], function(){
							var jsonData = JSON.parse(_config.canvasState[_config.currentStateIndex-1]);
							//ft.applyImageFilters(_canvasObject);
							$.each(jsonData, function(k, obj){
								if(!obj.selectable) {
									console.log('Not Selectables undo, ', obj.clipBoxName);
									$('#layers .layer-' + obj.clipBoxName).find('.fa').removeClass('fa-unlock').addClass('fa-lock');
									$('#layers .layer-' + obj.clipBoxName).data('status', LOCK_CLOSE);
									canvas.discardActiveObject();
								} else {
									console.log('Selectables undo, ', obj.clipBoxName);
									$('#layers .layer-' + obj.clipBoxName).find('.fa').removeClass('fa-lock').addClass('fa-unlock');
									$('#layers .layer-' + obj.clipBoxName).data('status', LOCK_OPEN);
								}

								if(obj.shadow != undefined && obj.shadow != false) {
									obj.set('shadow', obj.shadow);
								}
							});
							
				    		_canvasObject.renderAll();
		      				_config.undoStatus = false;
		      				_config.currentStateIndex -= 1;
							_config.undoButton.removeAttribute("disabled");
							if(_config.currentStateIndex !== _config.canvasState.length-1){
								_config.redoButton.removeAttribute('disabled');
							}
							_config.undoFinishedStatus = 1;
			      		});
			      	}
			      	else if(_config.currentStateIndex == 0){
		 		      	//_canvasObject.clear();
						_config.undoFinishedStatus = 1;
						_config.undoButton.disabled= "disabled";
						_config.redoButton.removeAttribute('disabled');
				      	_config.currentStateIndex -= 1;
				      }
				    }
				}
			}
			
			//ft.resizeControls(_canvasObject);
		}
	  
		var redo = function() {
			if(_config.redoFinishedStatus){
				if((_config.currentStateIndex == _config.canvasState.length-1) && _config.currentStateIndex != -1){
					_config.redoButton.disabled= "disabled";
				}else{
			  	if (_config.canvasState.length > _config.currentStateIndex && _config.canvasState.length != 0){
						_config.redoFinishedStatus = 0;
			    	_config.redoStatus = true;
			      _canvasObject.loadFromJSON(_config.canvasState[_config.currentStateIndex+1],function(){
								var jsonData = JSON.parse(_config.canvasState[_config.currentStateIndex+1]);

								$.each(jsonData, function(k, obj){
									if(!obj.selectable) {
										console.log('Not Selectables redo, ', obj.clipBoxName);
										$('#layers .layer-' + obj.clipBoxName).find('.fa').removeClass('fa-unlock').addClass('fa-lock');
										$('#layers .layer-' + obj.clipBoxName).data('status', LOCK_CLOSE);
										canvas.discardActiveObject();
									} else {
										console.log('Selectables redo, ', obj.clipBoxName);
										$('#layers .layer-' + obj.clipBoxName).find('.fa').removeClass('fa-lock').addClass('fa-unlock');
										$('#layers .layer-' + obj.clipBoxName).data('status', LOCK_OPEN);
									}

									if(obj.shadow != undefined && obj.shadow != false) {
										obj.set('shadow', obj.shadow);
									}
								});
					    	_canvasObject.renderAll();
				    		_config.redoStatus = false;
			      		_config.currentStateIndex += 1;
								if(_config.currentStateIndex != -1){
									_config.undoButton.removeAttribute('disabled');
								}
							_config.redoFinishedStatus = 1;
	            if((_config.currentStateIndex == _config.canvasState.length-1) && _config.currentStateIndex != -1){
	              _config.redoButton.disabled= "disabled";
	            }
			      });
			    }
				}
			}
		}
	    
	 	return {
	 		canvas		: _canvasObject,
			undoButton 	: _config.undoButton,
			redoButton 	: _config.redoButton,
			undo       	: undo,
			redo       	: redo,
			updateCanvasState : updateCanvasState,
			resetHistoryStack : resetHistoryStack
	  	}


  	})();

	  
	$('#undo').on('click',function(){
		ftCanvas.undo();
	});

	$('#redo').on('click',function(){
		ftCanvas.redo();
	});

	var canvas = ftCanvas.canvas;
	canvas.defaultCursor = 'default';
	canvas.hoverCursor = 'pointer';

	dpi = $('#dpi').val() ? $('#dpi').val() : 300;
	var cSz = ft.currentCanvasDim();
	var canvasHeight = cSz.canvasHeight;
	var canvasWidth = cSz.canvasWidth;

	var canvasObj = document.getElementById('ft_canvas');

	ft.setCanvasSize(fabCanvas=canvas, canHeight=canvasHeight, canWidth=canvasWidth, dpi=dpi);
	ft.calcZoomValue(canvas);
	
	var addImage = function(oCanvas, src, left, top, angle=0, opacity=1, clipPath=''){
     	fabric.Image.fromURL(src, function(img) {
		    //img.scale(scale);
		    img.left = left;
		    img.top = top;
		    img.angle = angle;
		    img.clipPath = new fabric.Rect({left:1000, top:300, width:1000, height:1000, angle:30, fixed:true, fill: '', strokeWidth:50, stroke:'rgba(0,0,0,0.5)'});

		    img.setControlVisible('ml', false);
		    img.setControlVisible('mt', false);
		    img.setControlVisible('mb', false);
		    img.setControlVisible('mr', false);

		    oCanvas.add(img).setActiveObject(img);
	  	},
	  	{crossOrigin: 'anonymous'});
	    oCanvas.renderAll();
  	}

	////////////////// Color Picker - Fill color //////////////////
	$('#fill_colorpicker_val').colorpicker({
		color: false,
		container: $('.fill-colorpicker'),
		inline: true,
		format: 'rgba',
		fallbackFormat: 'rgba',
		hexNumberSignPrefix: false,
		customClass: 'ft-colorpicker-ov',
	}).on('changeColor', function(e){
		var activeObject = $ActiveObject;
		var colorObj = e.color;
		//console.log(colorObj);

		var colorRGBAVal = $(this).val().match(/\(([^)]+)\)/)[1]; // get values inside paranthesis
		var colorHexVal = colorObj.toHex();
		
		var rgba = colorRGBAVal.split(',');
		$('#fill_cp_red').val(rgba[0]);
		$('#fill_cp_green').val(rgba[1]);
		$('#fill_cp_blue').val(rgba[2]);
		if(rgba.length > 3) {
			$('#fill_cp_alpha').val(parseFloat(rgba[3]));
		} else {
			$('#fill_cp_alpha').val('1.0');
		}
		
		$('#fill_cp_hex').val(colorHexVal);

		// change canvas object color
		//console.log(colorHexVal);
		if(!activeObject) {			
			canvas.set('backgroundColor', $(this).val());
			clearTimeout($.data(this, 'timer'));
		  	$.data(this, 'timer', setTimeout(function() {		  		
		     	canvas.requestRenderAll();
				ftCanvas.updateCanvasState();
		  	}, 250));
		} else {
			if(activeObject.ftObjectType != "Placeholder" && activeObject.fill != undefined) {
				//activeObject.fill = $(this).val();
				activeObject.set('fill', $(this).val());
				clearTimeout($.data(this, 'timer'));
			  	$.data(this, 'timer', setTimeout(function() {			     	
					canvas.renderAll();
					ftCanvas.updateCanvasState();
			  	}, 250));
			}
		}		
	});

	///////////// Color picker - stroke color ////////////////
	$('#stroke_colorpicker_val').colorpicker({
		color: false,
		container: $('.stroke-colorpicker'),
		inline: true,
		format: 'rgba',
		fallbackFormat: 'rgba',
		hexNumberSignPrefix: false,
		customClass: 'ft-colorpicker-ov'
	}).on('changeColor', function(e){
		var activeObject = $ActiveObject;
		var colorObj = e.color;
		//console.log(colorObj);

		var colorRGBAVal = $(this).val().match(/\(([^)]+)\)/)[1]; // get values inside paranthesis
		var colorHexVal = colorObj.toHex();
		
		var rgba = colorRGBAVal.split(',');
		$('#stroke_cp_red').val(rgba[0]);
		$('#stroke_cp_green').val(rgba[1]);
		$('#stroke_cp_blue').val(rgba[2]);
		if(rgba.length > 3) {
			$('#stroke_cp_alpha').val(parseFloat(rgba[3]));
		} else {
			$('#stroke_cp_alpha').val('1.0');
		}
		
		$('#stroke_cp_hex').val(colorHexVal);

		// change canvas object color
		if(activeObject && activeObject.stroke != undefined) {
			activeObject.set('stroke', $(this).val());
			canvas.renderAll();
		}	
	});

	////////////// Color Picker - shadow /////////////////
	$('#shadow_colorpicker_val').colorpicker({
		color: '#000000',
		container: $('.shadow-colorpicker'),
		inline: true,
		format: 'rgba',
		fallbackFormat: 'rgba',
		hexNumberSignPrefix: false,
		customClass: 'ft-colorpicker-ov'
	}).on('changeColor', function(e){
		var activeObject = $ActiveObject;
		var colorObj = e.color;
		//console.log(colorObj);

		var colorRGBAVal = $(this).val().match(/\(([^)]+)\)/)[1]; // get values inside paranthesis
		var colorHexVal = colorObj.toHex();
		
		var rgba = colorRGBAVal.split(',');
		$('#shadow_cp_red').val(rgba[0]);
		$('#shadow_cp_green').val(rgba[1]);
		$('#shadow_cp_blue').val(rgba[2]);
		if(rgba.length > 3) {
			$('#shadow_cp_alpha').val(parseFloat(rgba[3]));
		} else {
			$('#shadow_cp_alpha').val('1.0');
		}
		
		$('#shadow_cp_hex').val(colorHexVal);

		// change canvas object color
		if(activeObject && activeObject.shadow != undefined) {
			activeObject.shadow.color = $(this).val();
			canvas.renderAll();
		}	
	});

	////////////////// Layers /////////////////////
	ft.setLayersList(canvas);

	$('#properties_and_layers').on('click', '.layer-name', function(e){
		var layerName = $(this).data('layer-name');
		if(layerName == 'canvas_bg') {
			canvas.discardActiveObject();
		} else {
			ft.selectLayer(canvas, layerName);
		}		
		$(this).removeClass('active').addClass('active');
	});	

	$('#properties_and_layers').on('click', '.layer-action-showhide', function(e){
		e.preventDefault();
		var layerName = $(this).data('layer-name');
		var actionStatus = $(this).data('status');
		ft.toggleObjectByFTName(canvas, layerName);
		if(actionStatus == VISIBLE_FALSE) {
			$(this).find('.fa').removeClass('fa-eye-slash').addClass('fa-eye');
			$(this).data('status', VISIBLE_TRUE);
		} else {
			$(this).find('.fa').removeClass('fa-eye').addClass('fa-eye-slash');
			$(this).data('status', VISIBLE_FALSE);
		}
	});

	$('#properties_and_layers').on('click', '.layer-action-lock', function(e){
		e.preventDefault();
		var layerName = $(this).data('layer-name');
		var actionStatus = $(this).data('status');
		ft.toggleLockByFTName(canvas, layerName);
		if(actionStatus == LOCK_OPEN) {
			$(this).find('.fa').removeClass('fa-unlock').addClass('fa-lock');
			$(this).data('status', LOCK_CLOSE);
			canvas.discardActiveObject();
		} else {
			$(this).find('.fa').removeClass('fa-lock').addClass('fa-unlock');
			$(this).data('status', LOCK_OPEN);
		}
	});

	function degToRad(degrees) {
	    return degrees * (Math.PI / 180);
	}

	//var leftTopRect = new fabric.Rect({left:1000, top:300, width:500, height:1000, angle:30, fixed:true, strokeWidth:10, stroke:'rgba(0,0,0,0.5)'});
	//canvas.add(leftTopRect);
	//addImage(oCanvas=canvas, src='/home/tharanga/Documents/SASEY/happyshare.lk/resourses/Story Book/28.jpg', left=500, top=200);

	// Toolbar button actions
	$('.canvas-zoom').on('click', function(e) {
		var currentZoomVal = parseFloat($('#zoom_value').val());
		var newZoomVal = 0;
		var zoomDir = $(this).data('zoom');
		if(zoomDir == 'in') {
			newZoomVal = currentZoomVal + 5;
			if(newZoomVal > 100) {
				newZoomVal = 100;
			}
		}
		if(zoomDir == 'out') {
			newZoomVal = currentZoomVal - 5;
			if(newZoomVal <= 0) {
				newZoomVal = 5;
			}
		}
		if(zoomDir == 'reset') {
			newZoomVal = 0;
		}

		//console.log(zoomLevel);
		ft.zoomCanvas(canvas, newZoomVal);
		
		ft.calcZoomValue(canvas);
	});

	// zooming on mousewheel	
	$('#ft_image_editor .canvas-container').on('mousewheel', function(event) {
		if(event.ctrlKey) {
			if(event.deltaY === 1) {				
				$('.zoomin').trigger('click');
			} else if(event.deltaY === (-1)) {
				$('.zoomout').trigger('click');
			}

			// scroll to center when zooming
			var x = (event.pageX - $('#ft_image_editor').offset().left);
			var y = (event.pageY - $('#ft_image_editor').offset().top);

			var canvasCSSWidth = parseInt($('#ft_canvas').css('width'))
			var imgEditorCssWidth = parseInt($('#ft_image_editor').css('width'));

			if(canvasCSSWidth > imgEditorCssWidth) {
				//$('#ft_image_editor').scrollLeft(x);
			}

			var canvasCssHeight = parseInt($('#ft_canvas').css('height'));
			var imgEditorCssHeight = parseInt($('#ft_image_editor').css('height'));

			if(canvasCssHeight > imgEditorCssHeight) {
				//$('#ft_image_editor').scrollTop(y);
			}
		}
	});

	// key pressed events
	$(document).on('keydown', function(e){	
	 	e = e || window.event;
	 	// ctrl + N for new project
	 	if(e.ctrlKey && e.keyCode === 78) {
	 		$('.new-project-btn').trigger('click');
	 	}	
	 	// undo redo on ctrl+z and ctrl+y
		if(e.keyCode == 90 && e.ctrlKey) {
			$('#undo').trigger('click');
		} else if(e.keyCode == 89 && e.ctrlKey) {
			$('#redo').trigger('click');
		}

		// save on Ctrl + s
		if(e.ctrlKey && e.keyCode == 83) {
			ft.saveProject(canvas, isNew=false, true);
		}

		// export on ctrl + shift + e
		if(e.ctrlKey && e.shiftKey && e.keyCode == 69) {
			$('.export-file-btn').trigger('click');			
		}

		// add files key combinations	
		if($('#add_files').hasClass('active')) {
			// select all
			if(e.ctrlKey && e.keyCode === 65) {
				$('#top_toolbar').find('.select-all').trigger('click');
			}
			// select none
			if(e.keyCode === 27) {
				$('#top_toolbar').find('.select-none').trigger('click');
			}
			// delete
			if(e.keyCode === 8 || e.keyCode === 46) {
				$('#top_toolbar').find('.remove-selected').trigger('click');
			}
		}			

		// panning on spacebar
		if(e.keyCode == 32) {
			//console.log(($("#croperModal").data('bs.modal') || {isShown: false}).isShown);
			if(!$('input[type="text"]').is(':focus') && !$('textarea').is(':focus') && !($("#croperModal").data('bs.modal') || {isShown: false}).isShown) {
				e.preventDefault();
				canvas.discardActiveObject();

				if(!$('#ft_image_editor').hasClass('dragscroll')) {
					$('#ft_image_editor').addClass('dragscroll');
	  				dragscroll.reset();
				}
				
				var layouts = canvas.getObjects();
				$.each(layouts, function(k, obj){
					if(obj.type == 'text' || obj.type == 'textbox' || obj.ftObjectType == 'Placeholder') {
						obj.selectable = false;
					}
				});
			}
		}

		// toggle image pane on Q
		if(e.keyCode === 81) {
			if(!$('input[type="text"]').is(':focus') && !$('textarea').is(':focus') && !($("#croperModal").data('bs.modal') || {isShown: false}).isShown) {
				$('#input_image_panel .handle').trigger('click');
			}
		}	
	});

	$(document).on('keyup', function(e){
		// panning on spacebar
		if(e.keyCode == 32) {
			if($('#ft_image_editor').hasClass('dragscroll')) {
				$('#ft_image_editor').removeClass('dragscroll');
  				dragscroll.reset();
			}

			var layouts = canvas.getObjects();
			$.each(layouts, function(k, obj){
				if(obj.type == 'text' || obj.type == 'textbox' || obj.ftObjectType == 'Placeholder') {
					obj.selectable = true;
				}
			});
		}

		if(($("#shorcutsModal").data('bs.modal') || {isShown: false}).isShown) {
			$('#shorcutsModal').modal('hide');
		}			

	});

	// show/hide guidelines
	$('.giudelines').on('click', function(e){
		e.preventDefault();

		ft.addGuileLines(canvas);
	});

	// Show/Hide toolbars
	$('.toolbar-show-hide').on('click', function(e) {
		$toolbarToggle = $(this);
		var tbid = $toolbarToggle.data('tbid');
		var animDir = $toolbarToggle.data('dir');

		if(animDir == 'right') {
			$('#' + tbid).css('right', '-250px');
			$('#ft_image_editor').css('width', '100vw');

			$toolbarToggle.data('dir', 'left');
			if($toolbarToggle.hasClass('fa-chevron-circle-right')) {
				$toolbarToggle.removeClass('fa-chevron-circle-right').addClass('fa-chevron-circle-left');
			}
		} else if(animDir == 'left') {
			$('#' + tbid).css('right', '0px');
			$('#ft_image_editor').css('width', (window.innerWidth - 250) + "px");

			$toolbarToggle.data('dir', 'right');
			if($toolbarToggle.hasClass('fa-chevron-circle-left')) {
				$toolbarToggle.removeClass('fa-chevron-circle-left').addClass('fa-chevron-circle-right');
			}
		}
	});


	// Set canvas size according to the  DPI entered in the textbox
	$('#dpi').on('blur', function(e){
		var newDip = $(this).val();
		if(newDip == dpi){
			return false;
		} else {
			var canDim = ft.currentCanvasDim();
			ft.setCanvasSize(canvas, canDim.canvasHeight, canDim.canvasWidth, newDip);
			ft.resizeControls(canvas);

			dpi = newDip;
			ft.calcZoomValue(canvas);

	    	projectJSON.dpi = newDip;
		}
	});

	$('#dpi').keydown(function(e){
		if(e.keyCode === 13) {
			$(this).trigger('blur');
		}
	});

	$('#zoom_value').on('change', function(e){
		//console.log(e);
		var newZoomVal = $(this).val();
		//var newZoomVal = parseFloat($(this).val());
		///var canvasCSSWidth = (newZoomVal*canvas.width)/100;
		
		//zoomLevel = canvasCSSWidth/(100);
		//console.log(canvasCSSWidth);
		ft.zoomCanvas(canvas, newZoomVal);
	});

	$('#input_image_panel .handle').on('click', function(){
		$this = $(this);
		$panel = $('#input_image_panel');

		var dir = $this.data('dir');

		if(dir == 'up'){
			$panel.animate({
				bottom: "+=488"
			}, 200, function(){
				$this.data('dir', 'down');
			});
		} else {
			$panel.animate({
				bottom: "-460"
			}, 200, function(){
				$this.data('dir', 'up');
			});
		}
	});

	$('#right_tool_bar .handle').on('click', function(){
		$this = $(this);
		$panel = $('#right_tool_bar');
		
		var dir = $this.data('dir');
		if(dir == 'left') {
			$panel.animate({
				right: "-=238"
			}, 300, function(){
				var imgEditorWidth = window.innerWidth - 10;
				$('#ft_image_editor').css('width', imgEditorWidth + 'px');
				$this.data('dir', 'right');
			});
		} else {
			$panel.animate({
				right: "+=238"
			}, 300, function(){
				var imgEditorWidth = window.innerWidth - 250;
				$('#ft_image_editor').css('width', imgEditorWidth + 'px');
				$this.data('dir', 'left');
			});
		}
	});

    $masonryGrid.imagesLoaded(function(){
        $masonryGrid.masonry("layout");
        ft.hideSwal();
    });

    // drag and grop files
    (function () {
        var addFilesPanel = $('#file_browser');

        addFilesPanel.on('dragover', (e) => {
        	if(!addFilesPanel.hasClass('drag-over')) {
        		addFilesPanel.addClass('drag-over');
        	}

            return false;
        });

        addFilesPanel.on('dragleave', (e) => {
        	addFilesPanel.removeClass('drag-over');
            return false;
        });

        addFilesPanel.on('dragend', (e) => {
            return false;
        });

        addFilesPanel.on('drop', (e) => {
            e.preventDefault();

            addFilesPanel.removeClass('drag-over');
            var filePaths = [];
            for (let oFile of e.originalEvent.dataTransfer.files) {
                //console.log('File(s) you dragged here: ', oFile.type)
                if(oFile.type == 'image/jpeg' || oFile.type == 'image/png' || oFile.type == 'image/gif') {
	  				filePaths.push(oFile.path);
	  			}
            }

            if(filePaths.length > 0) {
	  			ft.smoothThumbnailsLoader(filePaths, function(template){
					$('#image_panel').imagesLoaded(function(instance){
				    	arrangeImages(delay=300, append=true, elements=template);
			    	 	$('.ft-progress').html("");
						$('.progress-bar').css('width', "0%");
				        $('.progress').hide();
				        ft.saveProject(canvas);
				    }).progress(function(instance, image){
				    	//console.log(instance.progressedCount + ', ' + imageArray.length);
				    	/*var percentage = parseInt((instance.progressedCount / imageArray.length)*100);
						$('.progress').show();
						$('.ft-progress').html(percentage + "%");
						$('.progress-bar').css({'width': percentage + "%"});*/
					});
				});
	  		}
            
            return false;
        });
    })();

    /*dragDrop('#file_browser', function (files, pos) {
		  	//console.log('Here are the dropped files', files)
		  	//console.log('Dropped at coordinates', pos.x, pos.y)
		  	if(files != undefined) {
		  		var filePaths = [];
		  		$.each(files, function(k, oFile){
		  			if(oFile.type == 'image/jpeg' || oFile.type == 'image/png' || oFile.type == 'image/gif') {
		  				filePaths.push(oFile.path);
		  			}	
		  		});
		  		//console.log(filePaths.length);
		  		if(filePaths.length > 0) {
		  			ft.smoothThumbnailsLoader(filePaths, function(template){
						$('#image_panel').imagesLoaded(function(instance){
					    	arrangeImages(delay=300, append=true, elements=template);
				    	 	$('.ft-progress').html("");
							$('.progress-bar').css('width', "0%");
					        $('.progress').hide();
					        ft.saveProject(canvas);
					    }).progress(function(instance, image){
					    	//console.log(instance.progressedCount + ', ' + imageArray.length);
					    	/*var percentage = parseInt((instance.progressedCount / imageArray.length)*100);
							$('.progress').show();
							$('.ft-progress').html(percentage + "%");
							$('.progress-bar').css({'width': percentage + "%"});
						});
					});
		  		}
			}
		});*/

    // add files to PhotoBook
	$('.add-files').on('click', function(e){
		e.preventDefault()
		dialog.showOpenDialog({
		    properties: ['openFile', 'multiSelections'],
		  	filters: [
		    	{name: 'Images', extensions: ['jpg', 'png', 'gif']},
		  	]
	  	}, function (files) {
	  		if(files != undefined) {
	  			ft.smoothThumbnailsLoader(files, function(template){
	  				$('#image_panel').imagesLoaded(function(instance){
				    	arrangeImages(delay=300, append=true, elements=template);
			    	 	$('.ft-progress').html("");
						$('.progress-bar').css('width', "0%");
				        $('.progress').hide();
				        ft.saveProject(canvas);
				    }).progress(function(instance, image){
				    	//console.log(instance.progressedCount + ', ' + imageArray.length);
				    	/*var percentage = parseInt((instance.progressedCount / imageArray.length)*100);
						$('.progress').show();
						$('.ft-progress').html(percentage + "%");
						$('.progress-bar').css({'width': percentage + "%"});*/
					});
	  			});
	  		}
	  	});
	});

	$('#image_panel').dblclick(function(e) {
		e.preventDefault();
		e.stopPropagation();
     	if(e.target !== e.currentTarget) return;

     	$('.add-files').trigger('click');
	});

	// select images from Add files panel
	var shiftSelection = [];
	$('#image_panel').on('click', '.img-container', function(e) {
		e.preventDefault();

		var randId = $(this).attr('id');
		//console.log(e.shiftKey);
		if(e.ctrlKey) {
			shiftSelection = [];
			if($.inArray(randId, selectedImages) >= 0) {
				$(this).removeClass('selected-img');
				selectedImages.splice($.inArray(randId, selectedImages), 1);
			} else {
				$(this).addClass('selected-img');
				selectedImages.push(randId);
			}			
		} else if(e.shiftKey) {
			var startItem = randId;
			if(selectedImages.length > 0) {
				shiftSelection[0] = selectedImages[0];
				shiftSelection[1] = randId;
			} else {
				selectedImages.push(randId);
			}			

			if(shiftSelection.length == 2) {
				selectedImages = [];
				var startElIndex = $("div.img-container").index($('div#' + shiftSelection[0]));
				var endElIndex = $("div.img-container").index($('div#' + shiftSelection[1]));
				//console.log(startElIndex, endElIndex);
				if(startElIndex < endElIndex) {
					$('#image_panel').find('.img-container').removeClass('selected-img');
					for(var a = startElIndex; a <= endElIndex; a++){
						var dom = $('#image_panel').find('.img-container').eq(a);
						//console.log(dom[0]);
						selectedImages.push(dom[0].id);
						$('#' + dom[0].id).addClass('selected-img');
					}

					console.log(selectedImages);
				} else {
					$('#image_panel').find('.img-container').removeClass('selected-img');
					for(var a = endElIndex; a <= startElIndex; a++){
						var dom = $('#image_panel').find('.img-container').eq(a);
						//console.log(dom[0]);
						selectedImages.push(dom[0].id);
						$('#' + dom[0].id).addClass('selected-img');
					}

					console.log(selectedImages);
				}
			}
		} else {
			shiftSelection = [];
			selectedImages = [];
			selectedImages.push(randId);
			$('#image_panel').find('.img-container').removeClass('selected-img');
			$(this).addClass('selected-img');
		}		

		$('.selected-count').html(selectedImages.length);
		//console.log(selectedImages);
	});

	$('#top_toolbar').on('click', '.select-all', function(e) {
		selectedImages = [];
		$('#image_panel').find('.img-container').removeClass('selected-img');
		$('#image_panel').find('.img-container').each(function(e){
			selectedImages.push($(this).attr('id'));
			$(this).addClass('selected-img');
		});

		$('.selected-count').html(selectedImages.length);
	});

	$('#top_toolbar').on('click', '.select-none', function(e) {
		selectedImages = [];
		$('#image_panel').find('.img-container').removeClass('selected-img');
		$('.selected-count').html(selectedImages.length);
	});

	$('#top_toolbar').on('click', '.remove-selected', function(e) {
		if(selectedImages.length > 0) {
			dialog.showMessageBox({buttons: ['Yes, go ahead', 'No, go back'], title: 'Remove images', message: 'Remove images from your project.', detail: 'Do you really need to remove these images from your project?'},
				function(btnIndex){

					if(btnIndex == 0) {
						var thumbsPath = path.join(getConfig('workspace'), projectJSON.name, 'thumbnail');

						$.each(selectedImages, function(k, randId){
							$thisFile = $('#' + randId);
							var filepath = $thisFile.data('filepath');
							imageArray.splice($.inArray(filepath, imageArray), 1);
							unusedFiles.splice($.inArray(randId, unusedFiles), 1);
							usedFiles.splice($.inArray(randId, usedFiles), 1);
							
							$thisFile.parent('.img-container-outer').remove();
							$('#unused .unused-' + randId).parent('.img-container-outer').remove();
							$('#used .used-' + randId).parent('.img-container-outer').remove();

							// update project JSON
							delete(projectJSON.files[randId]);

							fse.removeSync(path.join(thumbsPath, randId + '.json'));
						});
						
						selectedImages = [];

						$('.selected-count').html(selectedImages.length);
						$('.added-count').html(imageArray.length);
						arrangeImages();

						if(imageArray.length == 0) {
							if(!$('#image_panel').hasClass('no-data')) {
								$('#image_panel').addClass('no-data');
							}					
						} else {
							$('#image_panel').find('.img-count-number').each(function(k, v){
								$(this).html(k+1);
							});
						}

						$('.unused-count').html(unusedFiles.length);

						ft.saveProject(canvas, isNew=false, false);
					}
				}					
			);
		}
	});

	$('#top_toolbar').on('click', '.remove-all', function(e){
		dialog.showMessageBox({buttons: ['Yes, go ahead', 'No, go back'], title: 'Remove images', message: 'Remove images from your project.', detail: 'Do you really need to remove these images from your project?'},
			function(btnIndex){
				if(btnIndex == 0) {
					selectedImages = [];
					imageArray = [];
					unusedFiles = [];

					// update project JSON file
					projectJSON.files = {};

					var thumbsPath = path.join(getConfig('workspace'), projectJSON.name, 'thumbnail');
					fse.removeSync(thumbsPath);

					$('#image_panel').html('');
					$('#unused .file-container').html('');
					if(!$('#image_panel').hasClass('no-data')) {
						$('#image_panel').addClass('no-data');
					}
					$('.selected-count').html(selectedImages.length);
					$('.added-count').html(imageArray.length);
					$('.unused-count').html(unusedFiles.length);

					ft.saveProject(canvas, false);
				}
			}
		);
	});

	// rearrage layout if needed
	$('#top_toolbar').on('click', '.rearrange-layout', function(e){
		e.preventDefault();
		arrangeImages(delay=300);
	});

	// Add files to storyboard
	$('.add-to-storyboard').on('click', function(e) {
		e.preventDefault();

		ft.setAppStatus(status="", level='busy', popover="");

		var template = template_used = '';

		//selectedImages = selectedImages.sort();
		$.each(selectedImages, function(k, randId) {
			var filepath = projectJSON.files[randId].path;
			var filename = path.basename(filepath);
			var thumbSrc = $('#' + randId).find('img').attr('src');
			//console.log($('#image_panel').find('div[data-filepath="' + filepath + '"]').find('img'));
			if($.inArray(randId, unusedFiles) === -1 && $.inArray(randId, usedFiles) === -1) {
				template += '<div class="img-container-outer col-md-3 col-lg-3 col-sm-6">';
				template += '<div class="remove-unused" title="remove" data-file-id="' + randId + '"><i class="fa fa-times"></i></div>';
				template += '<div class="dev-img-container unused-' + randId + '" data-randid="' + randId + '" data-filepath="' + filepath + '">';
	    		template += '<img src="' + thumbSrc + '" />';	
	    		template += '</div>'; 
	    		template += '<div class="ft-truncate"><small>' + filename + '</small></div>';  
	    		template += '</div>';

	    		unusedFiles.push(randId);

	    		// var fileId = $('#image_panel').find('div[data-filepath="' + filepath + '"]').prop('id');
	    		// console.log($('#image_panel').find('div[data-filepath="' + filepath + '"]'));
	    		projectJSON.files[randId].is_added = 1;

	    		// fill used images panel
				template_used += '<div class="img-container-outer col-md-4 col-lg-4 col-sm-6 hidden">';
				template_used += '<div class="dev-img-container used-' + randId + '" data-randid="' + randId + '" data-filepath="' + filepath + '">';
				template_used += '<span class="badge use-count-badge">0</span>';
				template_used += '<img src="' + thumbSrc + '" />';	
				template_used += '</div>'; 
				template_used += '<div class="ft-truncate"><small>' + filename + '</small></div>';  
				template_used += '</div>';
			}
			
		});

		ft.setUsedUnusedCount(unusedFiles.length, usedFiles.length);

		$('#unused').find('.file-container').append(template);
		$('#used').find('.file-container').append(template_used);

		$masonryGridUnused.imagesLoaded(function(){
			arrangeUnusedImages(delay=300, append=true, elements=template);
	    });

	    $masonryGridUsed.imagesLoaded(function(){
			arrangeUsedImages(delay=300, append=true, elements=template_used, function(){
				ft.setAppStatus(status="", level='ok', popover= selectedImages.length + " files added to the story board.", show=true);
				$('.unused-count').html(unusedFiles.length);
				ft.saveProject(canvas);
			});
			
	    });
		
		//$('.scrollbar-macosx').scrollbar();

		/*$('.unused-files').imagesLoaded(function(){
			ft.setAppStatus(status="", level='ok', popover= selectedImages.length + " files added to the story board.", show=true);
			$('.unused-count').html(selectedImages.length);
		});*/
	});

	$('#input_image_panel').on('click', '.dev-img-container', function(e) {
		e.stopPropagation();

		var randId = $(this).data('randid');
		var imgPath = $(this).data('filepath');		

		var activeObject = canvas.getActiveObject();
		//var sizeObj = activeObject.getOriginalSize();

		if(activeObject != undefined && activeObject.ftObjectType == "Placeholder") {
			var boxType = activeObject.ftCropPath;
			//console.log('box type: ' + boxType);
			
			var boxWidth 	= activeObject.width;
			var boxHeight 	= activeObject.height;
			var boxAngle 	= activeObject.angle;
			var boxLeft 	= activeObject.left;
			var boxTop 		= activeObject.top;
			var boxRight	= activeObject.right != undefined ? activeObject.right : '';
			var boxOrigX	= activeObject.originX;
			var boxOrigY	= activeObject.originY;
			if(activeObject.stroke) {
				var boxStroke 	= activeObject.stroke;
				var boxStrokeWidth = activeObject.strokeWidth;
			}			
			var boxPolypoints = activeObject.ftPolyPoints;

			var boxFilters = null;
			if(activeObject.ftFilters) {
				boxFilters = activeObject.ftFilters;
			}

			var ftApplyOn = null;
			if(activeObject.ftApplyOn) {
				ftApplyOn = activeObject.ftApplyOn;
			}

			if(boxType == 'circle') {
				var boxRadius = activeObject.radius;	
			}

			var origCanvasWidth = canvas.width;
			var origCanvasHeight = canvas.height;

			var canvasDimRatio = origCanvasHeight/origCanvasWidth;

			var canvasWidth = $(window).width();
			var canvasHeight = canvasDimRatio * canvasWidth;

			var scaleWidth = canvasWidth / origCanvasWidth;
			var scaleUp = origCanvasWidth / canvasWidth;

			/////////////////////////// Image Croping ///////////////////////////////

			// open modal for cropping
		
			var clipBoxName = activeObject.clipBoxName;
			var ftObjName = activeObject.ftObjectName;

			var imgColorPallete = "";
			if(clipBoxName == 'main_img') {
				imgColorPallete = ft.getColorPallete(imgPath);
			}

			//console.log(imgColorPallete);

			var cropCanvasHTML = '<canvas id="crop_canvas" width="800" height="600"></canvas>';
			$('#crop_canvas_container').html(cropCanvasHTML);

			var cropCanvas =  new fabric.Canvas('crop_canvas', 
						{
							selection : false,
							backgroundColor: 'transparent',
							preserveObjectStacking: true,
							stateful: false,
			  				preserveObjectStacking: true,
			  				renderOnAddRemove: false
						});

			cropCanvas.on('object:selected', onObjectScaled);
			cropCanvas.on('object:scaling', onObjectScaled); 

			ft.resizeControlsOnZoom(cropCanvas);

			function onObjectScaled(e){
				var currentObject = e.target;
				//console.log(currentObject.width);
				if(currentObject != undefined) {
					$('#crop_im_size_w').html(Math.floor(currentObject.width*currentObject.scaleX/scaleWidth) + 'px');
		  			$('#crop_im_size_h').html(Math.floor(currentObject.height*currentObject.scaleY/scaleWidth) + 'px');
				}
			};			

			// crop canvas zoom
			$('.crop-canvas-zoom').unbind('click');
			$('.crop-canvas-zoom').on('click', function(e) {
				ft.calcCropCanvasZoomValue(cropCanvas);

				var currentZoomVal = parseFloat($('#crop_zoom_val').val());
				var newZoomVal = 0;
				var zoomDir = $(this).data('zoom');
				if(zoomDir == 'in') {
					newZoomVal = currentZoomVal + 5;
					/*if(newZoomVal > 100) {
						newZoomVal = 100;
					}*/
				}
				if(zoomDir == 'out') {
					newZoomVal = currentZoomVal - 5;
					if(newZoomVal <= 0) {
						newZoomVal = 5;
					}
				}
				if(zoomDir == 'reset') {
					newZoomVal = 0;
				}

				//console.log(zoomLevel);
				ft.zoomCanvas(cropCanvas, newZoomVal, true);
				
				ft.calcCropCanvasZoomValue(cropCanvas);

				//ft.resizeControlsOnZoom(cropCanvas);
			});

			$('.rotate-90').unbind('click');
			$('.rotate-90').on('click', function(e){
				e.preventDefault();
				e.stopPropagation();

				var rotateDir = $(this).data('rotdir');
				var cropObject = cropCanvas.item(0);
				if(rotateDir == 'ccw') {
					if(cropObject.angle) {
						cropObject.angle -= 90;
					} else {
						cropObject.angle = -90;
					}
					
				} else {
					if(cropObject.angle) {
						cropObject.angle += 90;
					} else {
						cropObject.angle = 90;
					}
				}
				cropCanvas.requestRenderAll();
			});

			// flip images
			$('.flip-btn-on-crop').unbind('click');
			$('.flip-btn-on-crop').on('click', function(e){
				e.preventDefault();

				var flipDirection = $(this).data('flip');
				var activeObject = cropCanvas.getActiveObject();
				if(activeObject) {
					if(flipDirection == 'x') {
						var thisFlipX = !activeObject.flipX;
						activeObject.set('flipX', thisFlipX);
					} else if(flipDirection == 'y') {
						var thisFlipY = !activeObject.flipY;
						activeObject.set('flipY', thisFlipY);
					}

					cropCanvas.requestRenderAll();
				}
			});

			$('#croperModal .canvas-container').unbind('mousewheel');
			$('#croperModal .canvas-container').on('mousewheel', function(event) {
				if(event.ctrlKey) {
					if(event.deltaY === 1) {				
						$('.crop-zoomin').trigger('click');
					} else if(event.deltaY === (-1)) {
						$('.crop-zoomout').trigger('click');
					}
				}
			});

			// add cliping layout
			var cropLayout = '';
			
			if(boxType == 'rect') {
				cropLayout = new fabric.Rect({
					fill: '#eee',
					width: boxWidth,
					height: boxHeight,
					left: canvasWidth/2,
					top: canvasHeight/2,
					originX: 'center',
					originY: 'center',
					fixed: true,
					//angle: boxAngle,
				});
			}
			
			if(boxType == 'circle') {
				// different clip layout
				cropLayout = new fabric.Circle({
					fill: '#eee',
					radius: boxRadius,
					width: boxWidth,
					height: boxHeight,
					left: canvasWidth/2,
					top: canvasHeight/2,
					originX: 'center',
					originY: 'center',
					fixed: true,
					//angle: boxAngle,
				});
			}

			if(boxType == 'polygon') {
				var points = boxPolypoints;
				//console.log(activeObject.width, activeObject.height);
				cropLayout = new fabric.Polygon(points, {
					width: boxWidth,
					height: boxHeight,
					left: canvasWidth/2,
					top: (canvasHeight/2) + 150, 
					originX: 'center',
					originY: 'center',
					fixed: true
				});
			}

			var ftClipCode = null;
			if(boxType == 'svg_path') {
				ftClipCode = activeObject.ftClipCode;
				cropLayout = new fabric.Path(ftClipCode, {
					width: boxWidth,
					height: boxHeight,
					left: canvasWidth/2,
					top: (canvasHeight/2) + 150, 
					originX: 'center',
					originY: 'center',
					fixed: true,
					background: 'transparent'
				});
			}

			cropCanvas.setDimensions({width: canvasWidth + 300, height: canvasHeight + 300}, {backstoreOnly: true});
			
			var cropCanvasContect = $('#croperModal').find('.modal-dialog');
			var cropModelPosition = cropCanvasContect.position();
			var cropModelBottom = $(window).height() - cropModelPosition.top - cropCanvasContect.height();
			//console.log('Canvas model bottom', $(window).height(), cropModelBottom);
			var overflowHeight = cropModelBottom > $(window).height();
			if(overflowHeight > 0) {
				cropCanvas.setDimensions({width: 'auto', height: ($(window).height() - 200) + 'px'}, {cssOnly: true});
			} else {
				cropCanvas.setDimensions({width: '95%', height: 'auto'}, {cssOnly: true});
			}

			//$('#croperModal').modal('handleUpdate');

			cropCanvas.on('mouse:move', function(event) {
		    	var pointer = cropCanvas.getPointer(event.e);
		    	var posX = Math.round(pointer.x);
		    	var posY = Math.round(pointer.y);

		        $('#x_coord_crop').html(posX);
			  	$('#y_coord_crop').html(posY);			  	
		    });

			//console.log('Scale Width:', scaleWidth);

			cropLayout.set({
				stroke: "#00dc2f",
				strokeWidth: 20,
				strokeDashArray: [60, 40],
				fill: 'transparent',
				lockRotation: true,
				lockScalingX: true,
				lockScalingY: true,
				lockMovementX: true,
				lockMovementY: true,
				evented: false
			});

			$('#croperModal').unbind('keydown');
			$('#croperModal').keydown(function(ev){
				//panning on spacebar + mouse
				if(ev.keyCode === 32) {
					//console.log(($("#croperModal").data('bs.modal') || {isShown: false}).isShown);
					if(!$('input[type="text"]').is(':focus') && !$('textarea').is(':focus')) {
						ev.preventDefault();
						cropCanvas.discardActiveObject();

						if(!$('#crop_canvas_container').hasClass('dragscroll')) {
							$('#crop_canvas_container').addClass('dragscroll');
			  				dragscroll.reset();
						}
					}
				}

				// move object on crop modal ob arrow keys
				if(ev.keyCode === 37) {
					var getCropObject = cropCanvas.getActiveObject();
					if(ev.ctrlKey) {
						getCropObject.left -= 1;
					} else {
						getCropObject.left -= 10;
					}
					cropCanvas.requestRenderAll();
					cropCanvas.calcOffset();
				} else if(ev.keyCode === 38) {
					var getCropObject = cropCanvas.getActiveObject();
					if(ev.ctrlKey) {
						getCropObject.top -= 1;
					} else {
						getCropObject.top -= 10;
					}
					cropCanvas.requestRenderAll();
					cropCanvas.calcOffset();
				} else if(ev.keyCode === 39) {
					var getCropObject = cropCanvas.getActiveObject();
					if(ev.ctrlKey) {
						getCropObject.left += 1;
					} else {
						getCropObject.left += 10;
					}
					cropCanvas.requestRenderAll();
					canvas.calcOffset();
				} else if(ev.keyCode === 40) {
					var getCropObject = cropCanvas.getActiveObject();
					if(ev.ctrlKey) {
						getCropObject.top += 1;
					} else {
						getCropObject.top += 10;
					}
					cropCanvas.requestRenderAll();
					cropCanvas.calcOffset();
				}
			});

			$('#croperModal').keyup(function(ev){
				$('#crop_canvas_container').removeClass('dragscroll');
				dragscroll.reset();
			});

			//cropCanvas.add(clipLayout);
			//cropCanvas.renderAll();
			//cropCanvas.add(cropLayout);

			// image to crop
			var newDip = $('#dpi').val();
			var cropImage = new fabric.Image.fromURL(imgPath, function(img){
				/*if(img.width > canvasWidth)	{
					img.scaleToWidth(canvasWidth - 300);
				} else {
					img.scale(0.95);
				}*/	
				img.scaleToWidth(img.width * scaleWidth);		
				img.left = cropCanvas.width/2;
				img.top = cropCanvas.height/2;
				img.originX = 'center';
				img.originY = 'center';
				//img.clipPath = cropLayout;
				img.excludeFromExport = false;				

				img.setControlVisible('ml', false);
			    img.setControlVisible('mt', false);
			    img.setControlVisible('mb', false);
			    img.setControlVisible('mr', false);
			    img.transparentCorners= false;
			    img.cornerSize = Math.round((25 * newDip)/72) * scaleWidth;
			    img.rotatingPointOffset = Math.round((50 * newDip)/72) * scaleWidth;
			    img.borderScaleFactor = 1.5;
			    img.padding= 0;
			    img.borderDashArray = [20, 20];

			    cropCanvas.add(img).setActiveObject(img);
			    /*img.clone(function(shadedImg){
					shadedImg.opacity = 0.4;
					cropCanvas.add(shadedImg);

					var group = new fabric.Group([img, shadedImg], {
						cornerSize: Math.round((15 * newDip)/72),
						rotatingPointOffset: Math.round((40 * newDip)/72),
						transparentCorners: false,
					});

					cropCanvas.add(group);
					cropCanvas.renderAll();
					
				});*/
				cropLayout.scaleToWidth(boxWidth * scaleWidth);
				cropLayout.top = cropCanvas.height/2;
				cropLayout.left = cropCanvas.width/2;
				cropLayout.originX = 'center';
				cropLayout.originY = 'center';
				
				cropCanvas.calcOffset();
				
				cropCanvas.add(cropLayout)
				cropCanvas.renderAll();

				//cropImage.scale = (cropLayout.width/cropImage.width).toFixed(2);

				//console.log(cropCanvas);

				// add image for cropping

				$('#croperModal').modal('show');

				$('#crop_size_w').html(boxWidth + 'px');
				$('#crop_size_h').html(boxHeight + 'px');
		
				$('.crop-btn').unbind('click');
				$('.crop-btn').on('click', function(ev) {
					undoReady = false;
					ev.preventDefault();
					ev.stopPropagation();

					ft.loadingDialog('Processing your image...');

					if(cropCanvas.getActiveObject() == undefined) {
						cropCanvas.setActiveObject(cropCanvas.item(0));
					} 
					//console.log(cropCanvas)
					var actObj = cropCanvas.getActiveObject();
				
					cropCanvas.backgroundColor = 'transparent';
					/*actObj.clipTo = function(ctx){
						//if(boxType == 'rect' || boxType == 'image')
						ctx.rect(-cropLayout.left, -cropLayout.top, cropLayout.left + cropLayout.width, cropLayout.top + cropLayout.height);
					};*/
					
					cropCanvas.setDimensions({width: ((canvasWidth+300)/scaleWidth), height: ((canvasHeight+300)/scaleWidth)}, {backstoreOnly: true});
					var overflowHeight = cropModelBottom > $(window).height();
					if(overflowHeight > 0) {
						cropCanvas.setDimensions({width: 'auto', height: ($(window).height() - 200) + 'px'}, {cssOnly: true});
					} else {
						cropCanvas.setDimensions({width: '95%', height: 'auto'}, {cssOnly: true});
					}
					
					cropCanvas.calcOffset();

					//console.log('Crop image left, top:', actObj.left, actObj.top);
					
					//var prevWidth = actObj.width;
					//var prevHeight = actObj.height;
					var prevLeft = actObj.left;
					var pervTop = actObj.top;

					var boundingBox = actObj.getBoundingRect();

					actObj.scaleToWidth((boundingBox.width / scaleWidth));
					actObj.left = (prevLeft / scaleWidth);
					actObj.top = (pervTop / scaleWidth);
					
												
					actObj.setCoords();

					//console.log('After Crop image left, top:', actObj.left, actObj.top);

					cropLayout.scaleToWidth(boxWidth);
					cropLayout.top = cropCanvas.height/2;
					cropLayout.left = cropCanvas.width/2;
					cropLayout.originX = 'center';
					cropLayout.originY = 'center';
					
					//cropCanvas.calcOffset();
					//cropCanvas.renderAll();

					//var processCanvas = new fabric.StaticCanvas();

					ft.setAppStatus(status="", level='busy', popover="");

					var cropCanvasObjects = cropCanvas.getObjects();
					$.each(cropCanvasObjects, function(k, obj){
						obj.set({"stroke": null});
					});

					var imgDataURL = cropCanvas.toDataURL({
						format: 'jpeg',
						quality: 1,
						left: cropLayout.left - (cropLayout.width/2),
						top: cropLayout.top - (cropLayout.height/2),
						width: cropLayout.width,
						height: cropLayout.height
					});

					fabric.Image.fromURL(imgDataURL, function(cpImg){
						cpImg.ftObjectName = ftObjName;
						cpImg.clipBoxName = clipBoxName;
						cpImg.ftEditable = true;
						cpImg.fixed = activeObject.fixed;
						cpImg.ftObjectType = 'Placeholder';
						cpImg.ftImagePath = imgPath;
						cpImg.ftCropPath = boxType;
						cpImg.ftRandId = randId;
						cpImg.originX = activeObject.originX;
						cpImg.originY = activeObject.originY;
						if(boxPolypoints) {
							cpImg.ftPolyPoints = boxPolypoints;
						}					
						cpImg.ftColorPallete = $.isEmptyObject(imgColorPallete) ? {} : imgColorPallete;
						if(boxRight != undefined && boxRight != "") {
							cpImg.right = boxRight;
						} else {
							cpImg.left = boxLeft;
						}	
						if(boxFilters) {
							cpImg.ftFilters = boxFilters;
						}				
						cpImg.top = boxTop;
						cpImg.width = boxWidth;
						cpImg.radius = boxRadius;
						cpImg.height = boxHeight;
						//cpImg.scaleToWidth(boxWidth);
						cpImg.angle = boxAngle;
						cpImg.selectable = true;

						//console.log('STROKE: ' + boxStroke, 'WIDTH: ' + boxStrokeWidth);
						if(boxStroke != undefined && boxStrokeWidth != undefined) {

							cpImg.stroke = boxStroke;
							cpImg.strokeWidth = boxStrokeWidth;
						}		

						if(ftApplyOn) {
							cpImg.ftApplyOn = ftApplyOn;
						}		

						// clip image if SVG path available
						if(ftClipCode) {
							cpImg.set({
								ftClipCode: ftClipCode,
								clipTo: function (ctx) {
						        ctx.save();
							    ctx.beginPath();
							  
							  	var path = new fabric.Path(this.ftClipCode);
							  	path._renderPathCommands(ctx);
							    ctx.restore();
					      	}});
						}	

						ft.lockControls(cpImg);

						canvas.add(cpImg);
						
						canvas.moveTo(cpImg, canvas.getObjects().indexOf(canvas.getActiveObject()));

						canvas.remove(canvas.getActiveObject());
						canvas.setActiveObject(cpImg);
						
						if(!$.isEmptyObject(imgColorPallete)) {
							//console.log('main color: ' + imgColorPallete.Vibrant);
							canvas.backgroundColor = imgColorPallete.Vibrant;

							var canvasObjects = canvas.getObjects();
							$.each(canvasObjects, function(k, cObj){
								if(cObj.ftEditable && cObj.type == 'textbox') {
									cObj.fill = imgColorPallete.DarkVibrant;
								}

								if(cObj.dynamicColor) {
									cObj.fill = imgColorPallete.Vibrant;
								}
							});
						}

						// apply on other objects
						if(ftApplyOn) {
							///////////////// THIS PART NEED TO DEVELOP IN NEXT VERSION /////////////////

							/*var canvasObjects = canvas.getObjects();
							$.each(canvasObjects, function(k, cObj){
								Object.keys(ftApplyOn).forEach(function(objFtObjName){
									if(cObj.ftObjectName == objFtObjName) {
										var filterToApply = ftApplyOn[objFtObjName];

										fabric.Image.fromURL(imgDataURL, function(cpImgForOthers){
											if(cObj.width >= cObj.height) {
												cpImgForOthers.scaleToWidth(cObj.get('width'));
											} else {
												cpImgForOthers.scaleToHeight(cObj.get('height'));
											}

											cpImgForOthers.set({
												left: cObj.left,
												top: cObj.top,
												originX: cObj.originX,
												originY: cObj.originY,
												ftEditable: cObj.ftEditable,
												ftObjectName: cObj.ftObjectName,
												ftClipCode: cObj.ftClipCode,
												clipBoxName: cObj.clipBoxName,
												ftObjectType: cObj.ftObjectType,
												fixed: cObj.fixed,
												selectable: cObj.selectable,
												evented: cObj.evented,
												clipTo: function (ctx) {
											        ctx.save();
												    ctx.beginPath();

												  	var path = new fabric.Path(this.ftClipCode);
												  	path._renderPathCommands(ctx);
												    ctx.restore();
										      	}
											});

											canvas.add(cpImgForOthers);
											canvas.moveTo(cpImgForOthers, canvas.getObjects().indexOf(cObj));
											canvas.remove(cObj);
										});
									}
									
								});
							});*/							
						}

						if(boxFilters) {
							initFilters();

							//console.log("Grayscale filter ", $.inArray('grayscale', boxFilters));
							//console.log('Blur ', $.inArray('blur', boxFilters));
							if($.inArray('grayscale', boxFilters) >= 0){
								filters[3] = new fabric.Image.filters.Grayscale();
							}

							if($.inArray('blur', boxFilters) >= 0) {
								filters[4] = new fabric.Image.filters.Blur({blur: 0.2});
							}

							applyFilters(filters);
						} else {
							canvas.renderAll();
						}
						
						canvas.discardActiveObject().setActiveObject(cpImg);
						//console.log(cpImg.width, cpImg.height);
						ft.setAppStatus(status="", level='ok', popover="");

						cropCanvas.dispose();
						undoReady = true;
						//cropCanvas = '';

				        $('#croperModal').modal('hide');
					});

					// add colors to bottom bar				
					if(!$.isEmptyObject(imgColorPallete)) {
						var colorPalleteTemplate = "";
						$.each(imgColorPallete, function(colname, hex){
							colorPalleteTemplate += "<div class='color-pallete-color' data-color='" + hex + "' style='background:" + hex + "'></div>";
						});

						colorPalleteTemplate += "<div class='color-pallete-color pick-a-color' title='pick a color'><div class='glyph-icon flaticon-pipette'></div></div>";

						$('#color_pallete').html(colorPalleteTemplate);
					}

					// add image to used list
					// $thisImg = $('#image_panel').find('div[data-filepath="' + imgPath + '"]');
					var imgFileId = randId;
					//console.log('RAND: ' + randId);
					var cur_use_count = projectJSON.files[imgFileId].use_count;

					$unusedThisImg = $('#unused').find('.unused-' + randId);
					$usedThisImg = $('#used').find('.used-' + randId);

					if(activeObject.get('type') == 'image') { 
						/* 	
							* if user chosed to change existing cropped image.
							* It it's prev selected image, nothing will be changed except image data itself.
							* If it's a different image, increase use_count and show/hide images from the pannels if needed.
						*/
						var activeObjectSrc = activeObject.ftImagePath;
						//console.log(activeObjectSrc == imgPath);
						if(activeObjectSrc != imgPath) {
							//$thisPlaceholderImg = $('#image_panel').find('div[data-filepath="' + activeObjectSrc + '"]');
							var placeholderImgId = activeObject.ftRandId;

							// the image that already cropped.
							$thisPlaceholderImgUsed = $('#used').find('.used-' + placeholderImgId);
							$thisPlaceholderImgUnused = $('#unused').find('.unused-' + placeholderImgId);
							
							var placeholderImg_use_count = projectJSON.files[placeholderImgId].use_count;

							// count down use_count of prev selected image
							if(placeholderImg_use_count > 0) {
								placeholderImg_use_count -= 1; // projectJSON.files[placeholderImgId].use_count = 
							}
							
							projectJSON.files[placeholderImgId].use_count = placeholderImg_use_count;

							// hide prev selected image if use_count is 0
							if(placeholderImg_use_count == 0) {
								$thisPlaceholderImgUsed.find('.use-count-badge').html(placeholderImg_use_count);
								$thisPlaceholderImgUsed.parent('.img-container-outer').addClass('hidden');
								$unusedThisImg.parent('.img-container-outer').addClass('hidden');
								$thisPlaceholderImgUnused.parent('.img-container-outer').removeClass('hidden');
								$usedThisImg.parent('.img-container-outer').removeClass('hidden');

								projectJSON.files[imgFileId].use_count = cur_use_count + 1;

								usedFiles.splice($.inArray(randId, usedFiles), 1);
								unusedFiles.push(randId);
							} else {
								projectJSON.files[imgFileId].use_count = cur_use_count + 1;

								$thisPlaceholderImgUsed.find('.use-count-badge').html(projectJSON.files[placeholderImgId].use_count);
								$usedThisImg.parent('.img-container-outer').removeClass('hidden');
								$unusedThisImg.parent('.img-container-outer').addClass('hidden');
								$usedThisImg.find('.use-count-badge').html(projectJSON.files[imgFileId].use_count);
							}

							// add image to used list if it's a new used image.
							if(projectJSON.files[imgFileId].use_count == 1) {
								$unusedThisImg.parent('.img-container-outer').addClass('hidden');
								$usedThisImg.parent('.img-container-outer').removeClass('hidden');
								$usedThisImg.find('.use-count-badge').html('1');

								usedFiles.push(randId);
								unusedFiles.splice($.inArray(randId, unusedFiles), 1);
							} else {
								$usedThisImg.find('.use-count-badge').html(projectJSON.files[imgFileId].use_count);
							}
						}
						
					} else {
						/*
						* Selected item is a actual placeholder.
						* Image is beign added for the first time.
						*/
						projectJSON.files[imgFileId].use_count = cur_use_count + 1;
						if(projectJSON.files[imgFileId].use_count == 1) {
							$unusedThisImg.parent('.img-container-outer').addClass('hidden');
							$usedThisImg.parent('.img-container-outer').removeClass('hidden');
							$usedThisImg.find('.use-count-badge').html('1');

							usedFiles.push(randId);
							unusedFiles.splice($.inArray(randId, unusedFiles), 1);
						} else {
							$usedThisImg.find('.use-count-badge').html(projectJSON.files[imgFileId].use_count);
						}
					}

					ft.setUsedUnusedCount(unusedFiles.length, usedFiles.length);

					//arrangeUnusedImages(delay=300, append=false, elements="");
					//arrangeUsedImages(delay=300, append=false, elements="");
					
				});
   			
			},
			{crossOrigin: 'anonymous'});


		} else { // An image placeholder was not selected
			dialog.showMessageBox({buttons:["OK"], type: 'info', title: 'PhotoBook', message: 'Please select a placeholder', detail: 'You have to select a placeholder, where you want to insert the image.'});
		}
	});	

	$('#croperModal').on('hidden.bs.modal', function(e){	
		ft.hideSwal();				
		$('#crop_canvas_container').html('');
		ft.resizeControls(canvas);
	});

	var selectableObjects = [];
	$('#ft_footer').on('click', '.color-pallete-color', function(e){
		var color = $(this).data('color');
		
		if(!$(this).hasClass('pick-a-color') && color) {
			var activeObject = canvas.getActiveObject();
			if(activeObject == undefined) {
				canvas.backgroundColor = color;
			}

			$('#fill_colorpicker_val').colorpicker('setValue', color);
			
			canvas.renderAll();

			ftCanvas.updateCanvasState();

			pickAColor = false;
			canvas.hoverCursor = 'default';
			$('.pick-a-color').find('div').removeClass('blink');
			$('.pick-a-color').css('background', 'none');

			// set back objects selectable
			$.each(selectableObjects, function(k, obj){
				obj.selectable = true;
			});
			selectableObjects = [];
		} 

		if($(this).hasClass('pick-a-color')) {
			// set objects selectable false
			canvas.forEachObject(function(obj) {
				if(obj.selectable == true) {
					selectableObjects.push(obj);
					obj.selectable = false;
				}			  
			});
			// pick a color ckicked
			if(!pickAColor) {
				pickAColor = true;
				$(this).find('div').addClass('blink');
				canvas.hoverCursor = canvas.defaultCursor = 'crosshair';
			} else {
				pickAColor = false;
				canvas.hoverCursor = 'pointer';
				canvas.defaultCursor = 'default'
				$(this).find('div').removeClass('blink');
				$(this).css('background', 'none');

				// set back objects selectable
				$.each(selectableObjects, function(k, obj){
					obj.selectable = true;
				});
				selectableObjects = [];
			}
			
		}
		
	});


	// set album size
	$('#create_project_album_sizes').on('click', '.album-size', function(e){
		e.preventDefault();
		var sizeValue = $(this).data('value');
		var sizeText = $(this).html();
		//var dpi = $('#dpi').val();

		if(!sizeValue) {
			return;
		}

		var canHeight = sizeValue.split(',')[0];
		var canWidth = sizeValue.split(',')[1];

		$('#custom_canvas_size_h').val(canHeight);
		$('#custom_canvas_size_w').val(canWidth);
	});

	$('#custom_size').on('change', function(e){
		if($(this).prop('checked')) {
			$('#custom_canvas_size_h').prop('readonly', false);
			$('#custom_canvas_size_w').prop('readonly', false);
		} else {
			$('#custom_canvas_size_h').prop('readonly', true);
			$('#custom_canvas_size_w').prop('readonly', true);
		}
	});

	// save project settings
	$('.project-setup-btn').on('click', function(e){
		e.preventDefault();
		var projectName = $('#ft_project_name').val();
		var noOfPages = $('#no_of_pages').val();

		var re = /^[a-zA-Z].*/; // validate project name
		//console.log(projectName + ' ' + noOfPages);
		if(projectName == "" || noOfPages == "" || noOfPages == 0) {
			dialog.showMessageBox(
	    		options={buttons: ['OK'], type: 'error', title: 'Input error', message: 'Invalid project details.', detail: 'Please provide all requested details.'},
    			function(buttonIndex){
	    			//$('#projectName').focus();
    		});
		} else if(!re.test(projectName)) {
			dialog.showMessageBox(
	    		options={buttons: ['OK'], type: 'error', title: 'Input error', message: 'Invalid project name.', detail: 'Project name must not contain any special characters.'},
	    		function(buttonIndex){
	    			//$('#projectName').focus();
    		});
		} else {
			$(this).prop('disabled', true);			

			var canHeight = $('#custom_canvas_size_h').val();
			var canWidth = $('#custom_canvas_size_w').val();

			if(canHeight > 0 && canWidth > 0) {
				var sizeText = canHeight + ' x ' + canWidth;
				var sizeValue = canHeight + ',' + canWidth;
				var dpi = $('#dpi').val();

				$('#canvas_size').val(sizeText);
				$('#canvas_size').data('size', sizeValue);

				
				//ft.calcZoomValue();
				//console.log(projectName + ' ' + noOfPages); return;
				projectJSON.album_size = canHeight + 'x' + canWidth;
				var pages = {};
				var pagesTemp = "";
				for(i=1; i <= noOfPages; i++) {
					if(i == 1) {
						pages['page' + i] = '1';
						pagesTemp += "<div class='page-pannel-item current status_" + PAGE_EDITTED + "' data-page='" + i + "'>" + i + "</div>";
					} else {
						pages['page' + i] = '0';
						pagesTemp += "<div class='page-pannel-item status_" + PAGE_NOT_EDITTED + "' data-page='" + i + "'>" + i + "</div>";
					}		
				}
				projectJSON.pages = pages;

				// create pages list
				$('#project_pages').find('.page-pannel').html(pagesTemp);

				projectJSON.name = projectName;
				var canvasJSON = canvas.toJSON(customJSONOptions);
				//console.log(projectJSON);
				
				ft.resetCanvas(canvas);
				ft.saveProject(canvas, isNew=true, false);

				/*ipc.send('save-project', JSON.stringify({projectJSON: projectJSON, cavasJSON: canvasJSON}));
				ipc.on('save-project-resp', function(event, args){
					var resp = JSON.parse(`${args}`);
			  		var respStatus = resp.status;
			  		var respMsg = resp.msg;
			  		if(respStatus == 'ok'){
			  			$('#canvasSizeModal').modal('hide');
			  			ft.setAppStatus(status="", level='ok', popover=respMsg, show=true);
			  			$('#project_name').val(projectSaveName);
			  			$('#project_name').prop('readonly', true);
			  		} else {
			  			ft.setAppStatus(status="", level='error', popover=respMsg);
			  		}
				});*/
			} else {
				dialog.showMessageBox(
		    		options={buttons: ['OK'], type: 'error', title: 'Input error', message: 'Invalid album size.', detail: 'Please provide valid album size.'},
		    		function(buttonIndex){
		    			$('#custom_canvas_size_h').focus();
	    		});
			}	
		}	
	});

	$('#canvasSizeModal').on('show.bs.modal', function(e){
		$('#ft_project_name').val('');
		$('#no_of_pages').val('30');
		$('#custom_canvas_size_h').val('10');
		$('#custom_canvas_size_w').val('24');
		$('.project-setup-btn').prop('disabled', false);

		$('#create_project_album_sizes div').html('');
		$.each(ft.albumSizes, function(k, sizeText){
			var albumSizeTemp = "";
			var sizeDataValue = sizeText.replace("x", ",");
			var sizeGroup = sizeText.split("x")[0];
			if($.inArray(sizeText, availableAlbumSizes) >= 0) {				
				albumSizeTemp += '<div class="album-size badge badge-green" data-value="' + sizeDataValue + '">' + sizeText + '</div>';
			} else {
				albumSizeTemp += '<div class="album-size badge badge-disabled">' + sizeText + '</div>';
			}
			
			$('#create_project_album_sizes').find('.s-' + sizeGroup).append(albumSizeTemp);
		});

		if(getConfig('projects').length == 0) {
			$('#cancel_setup_project_btn').hide();
		} else {
			$('#cancel_setup_project_btn').show();
		}
	});

	// thumb sizer slider
	$("#thumb_sizer").slider({
		min: 10,
		max: 50,
		value: 25
	});

    // Filter controls
    $('#grayscale').slider({
		min: 0,
		max: 2,
		value: 0
	});

	$("#brightness").slider({
		min: -1,
		max: 1,
		value: 0
	});

	$("#contrast").slider({
		min: -1,
		max: 1,
		value: 0
	});

	$('#saturation').slider({
		min: -1,
		max: 1,
		value: 0
	});

	$('#blur').slider({
		min: 0,
		max: 1,
		value: 0
	});

	$('#hue').slider({
		min: -1,
		max: 1,
		value: 0
	});

	$('#thumb_sizer').on('change', function(e){
    	$('#image_panel').find('.grid-item').css('width', e.value.newValue + '%');	
    	arrangeImages();
    });

	$('#enable_filters').on('change', function(e) {	
		e.preventDefault();
		e.stopPropagation();

		if($(this).prop("checked")) {
			if(!$(this).parent('.ft-accordion').hasClass('active')) {
				$(this).parent('.ft-accordion').trigger('click');
			}

			initFilters();
				
			$('#enable_grayscale').prop('disabled', false);
			$('#enable_blur').prop('disabled', false);
			$("#brightness").slider('enable');
			$('#contrast').slider('enable');
			$('#saturation').slider('enable');
			$('#hue').slider('enable');
		} else {
			if($(this).parent('.ft-accordion').hasClass('active')) {
				$(this).parent('.ft-accordion').trigger('click');
			}

			$.each(adjustableFilters, function(k, v){
				filters[k] = null;
			});

			applyFilters(filters);

			ft.disableFilters();
		}
		
	});

	$('#enable_grayscale').on('change', function(e){
		if($(this).prop('checked')) {
			$('#grayscale').slider('enable');
		} else {
			$('#grayscale').slider('refresh');
			$('#grayscale').slider('disable');
		}

		applyFilter(3, $(this).prop('checked') && new fabric.Image.filters.Grayscale());
	});

	$('#enable_blur').on('change', function(e){
		if($(this).prop('checked')) {
			$('#blur').slider('enable');
		} else {
			$('#blur').slider('refresh');
			$('#blur').slider('disable');
			$('#blur_value').val(0);
		}

		applyFilter(4, $(this).prop('checked') && new fabric.Image.filters.Blur());
	});

    /////////////// Brightness ///////////////
    $('#brightness').on('change', function(e){
    	$('#brightness_value').val(parseInt(e.value.newValue*100));	
    });

    $('#brightness').on('slideStop', function(e){
    	$('#brightness_value').val(parseInt(e.value*100));
		applyFilterValue(0, 'brightness', parseFloat(e.value)); 	
    });
    // rest on doubleclick
    $('#flt_brightness').find('.slider-handle').dblclick(function(){
    	$('#brightness_value').val(0);
		applyFilterValue(0, 'brightness', 0);
		$('#brightness').slider('setValue', 0);
	});
	// change value on textbox value
	$('#brightness_value').on('change', function(e){
		applyFilterValue(0, 'brightness', (parseFloat($(this).val())/100).toFixed(6));
		$('#brightness').slider('setValue', (parseFloat($(this).val())/100).toFixed(6));
	});

	//////////////// Contrast ////////////////////
	$('#contrast').on('change', function(e){
		$('#contrast_value').val(parseInt(e.value.newValue*100));
	});

	$('#contrast').on('slideStop', function(e){
		$('#contrast_value').val(parseInt(e.value*100));
		applyFilterValue(1, 'contrast', parseFloat(e.value));
	});
	//reset on dluble click
	$('#flt_contrast').find('.slider-handle').dblclick(function(){
		$('#contrast_value').val(0);
		applyFilterValue(1, 'contrast', 0);
		$('#contrast').slider('setValue', 0);
	});
	// change value on textbox value
	$('#contrast_value').on('change', function(e){
		applyFilterValue(1, 'contrast', (parseFloat($(this).val())/100).toFixed(6));
		$('#contrast').slider('setValue', (parseFloat($(this).val())/100).toFixed(6));
	});

	//////////////// Saturation ////////////////////
	$('#saturation').on('change', function(e){
		$('#saturation_value').val(parseInt(e.value.newValue*100));
	});

	$('#saturation').on('slideStop', function(e){
		$('#saturation_value').val(parseInt(e.value*100));
		applyFilterValue(2, 'saturation', parseFloat(e.value));
	});
	//reset on dluble click
	$('#flt_saturation').find('.slider-handle').dblclick(function(){
		$('#saturation_value').val(0);
		applyFilterValue(2, 'saturation', 0);
		$('#saturation').slider('setValue', 0);
	});
	// change value on textbox value
	$('#saturation_value').on('change', function(e){
		applyFilterValue(2, 'saturation', (parseFloat($(this).val())/100).toFixed(6));
		$('#saturation').slider('setValue', (parseFloat($(this).val())/100).toFixed(6));
	});

	//////////////// Grayscale ////////////////
	$('#grayscale').on('change', function(e){
		applyFilterValue(3, 'mode', grayscaleMode[parseInt(e.value.newValue)]);
	});
	//reset on dluble click
	$('#flt_grayscale').find('.slider-handle').dblclick(function(){
		$('#grayscale').slider('setValue', 0, false, true);
	});

	////////////// Blur /////////////////
	$('#blur').on('change', function(e){
		$('#blur_value').val(parseInt(e.value.newValue*100));
	});

	$('#blur').on('slideStop', function(e){
		applyFilterValue(4, 'blur', parseFloat(e.value));
	});

	//reset on dluble click
	$('#flt_blur').find('.slider-handle').dblclick(function(){
		$('#blur_value').val(0);
		applyFilterValue(4, 'blur', 0);
		$('#blur').slider('setValue', 0);
	});
	// change value on textbox value
	$('#blur_value').on('change', function(e){
		applyFilterValue(4, 'blur', (parseFloat($(this).val()/100)));
		$('#blur').slider('setValue', (parseFloat($(this).val()/100)));
	});

	/////////////// HUE /////////////////
	$('#hue').on('change', function(e){
		$('#hue_value').val(parseInt(e.value.newValue*100));
	});

	$('#hue').on('slideStop', function(e){
		applyFilterValue(5, 'rotation', parseFloat(e.value));
	});

	//reset on dluble click
	$('#flt_hue').find('.slider-handle').dblclick(function(){
		$('#hue_value').val(0);
		applyFilterValue(5, 'rotation', 0);
		$('#hue').slider('setValue', 0);
	});
	// change value on textbox value
	$('#hue_value').on('change', function(e){
		applyFilterValue(5, 'rotation', (parseFloat($(this).val()/100)));
		$('#hue').slider('setValue', (parseFloat($(this).val()/100)));
	});

	// update canvas status for undo/redo
	/*$('#brightness, #contrast, #saturation, #grayscal').on('slideStop', function(e){
		ftCanvas.updateCanvasState();
	});*/

	$('.ft-color-picker-pane').mouseup(function(e){
		ftCanvas.updateCanvasState();
	});

	$('input[type="range"]').on('slideStop', function(e){
		ftCanvas.updateCanvasState();
	});

  	function initFilters() {
  		var brightnessFilter = new fabric.Image.filters.Brightness({
			brightness: parseFloat($('#brightness').val())
		});
		var contrastFilter = new fabric.Image.filters.Contrast({
			contrast: parseFloat($('#contrast').val())
		});
		var saturationFilter = new fabric.Image.filters.Saturation({
			saturation: parseFloat($('#saturation').val())
		});
		var hueRotationFilter = new fabric.Image.filters.HueRotation({
			rotation: parseFloat($('#hue').val())
		});

		filters = [brightnessFilter, contrastFilter, saturationFilter];
		filters[5] = hueRotationFilter;
  		
  		applyFilters(filters);
  	}

  	function getFilter(index) {
	    var obj = canvas.getActiveObject();
	    return obj.filters[index];
  	}

  	function applyFilter(index, filter) {
	    var obj = canvas.getActiveObject();
	    if(obj) {
	    	obj.filters[index] = filter;    
		    obj.applyFilters();	   
		    canvas.renderAll();
	    }	    
  	}
   
	function applyFilters(filters) {
	    var obj = canvas.getActiveObject();
	    if(obj) {
	    	var existingFilters = obj.filters;
	    	if(existingFilters[6] != undefined) {
	    		obj.filters = filters.concat(existingFilters[6]);
	    	} else {
	    		obj.filters = filters;
	    	}	    	
		    obj.applyFilters();
		    ftCanvas.updateCanvasState();
		    canvas.renderAll();
	    }	    
  	}

	function applyFilterValue(index, prop, value) {
	    var obj = canvas.getActiveObject();

	    if (obj.filters[index]) {
	      	obj.filters[index][prop] = value;
	      	obj.applyFilters();
	      	canvas.renderAll();
	    }
  	}

  	// color spectrum functions
  	$('.fill-rgb-val').on('change', function(e){
  		var rgbRed 		= $('#fill_cp_red').val();
  		var rgbGreen 	= $('#fill_cp_green').val();
  		var rgbBlue		= $('#fill_cp_blue').val();
  		var alpha		= $('#fill_cp_alpha').val();

  		var rgbaValue = 'rgba(' + rgbRed + ',' + rgbGreen + ',' + rgbBlue + ',' + alpha + ')';
  		console.log(rgbaValue);
  		$('#fill_colorpicker_val').colorpicker('setValue', rgbaValue);
  		//$('#fill_colorpicker_val').trigger('change');
  	});

  	$('.fill-rgb-val').keydown(function(e){
		if(e.keyCode === 13) {
			$(this).trigger('change');
		}
	});

  	$('#fill_cp_hex').on('blur', function(e) {
  		var rgbaString = ft.hexToRGBA('#' + $(this).val(), $('#fill_cp_alpha').val());

  		if(!rgbaString) {
  			dialog.showMessageBox(
		    		options={buttons: ['OK'], type: 'error', title: 'Input error', message: 'Invalid Hex value.', detail: 'Please enter 6 digit hex value without leading # sign.'},
		    		function(buttonIndex){
		    			$('#fill_cp_hex').focus();
    		});
  		} else {
  			$('#fill_colorpicker_val').colorpicker('setValue', rgbaString);
  			//$('#fill_colorpicker_val').trigger('change');
  		}
  	});

  	$('#fill_cp_hex').keydown(function(e){
		if(e.keyCode === 13) {
			$(this).trigger('blur');
		}
	});

	$('.stroke-rgb-val').on('change', function(e){
  		var rgbRed 		= $('#stroke_cp_red').val();
  		var rgbGreen 	= $('#stroke_cp_green').val();
  		var rgbBlue		= $('#stroke_cp_blue').val();
  		var alpha		= $('#stroke_cp_alpha').val();

  		var rgbaValue = 'rgba(' + rgbRed + ',' + rgbGreen + ',' + rgbBlue + ',' + alpha + ')';
  		console.log(rgbaValue);
  		$('#stroke_colorpicker_val').colorpicker('setValue', rgbaValue);
  		//$('#stroke_colorpicker_val').trigger('change');
  	});

  	$('#stroke_cp_hex').on('blur', function(e) {
  		var rgbaString = ft.hexToRGBA('#' + $(this).val(), $('#stroke_cp_alpha').val());

  		if(!rgbaString) {
  			dialog.showMessageBox(
		    		options={buttons: ['OK'], type: 'error', title: 'Input error', message: 'Invalid Hex value.', detail: 'Please enter 6 digit hex value without leading # sign.'},
		    		function(buttonIndex){
		    			$('#stroke_cp_hex').focus();
    		});
  		} else {
  			$('#stroke_colorpicker_val').colorpicker('setValue', rgbaString);
  			//$('#stroke_colorpicker_val').trigger('change');
  		}
  	});

  	$('#stroke_cp_hex').keydown(function(e) {
		if(e.keyCode === 13) {
			$(this).trigger('blur');
		}
	});

	$('.shadow-rgb-val').on('change', function(e){
  		var rgbRed 		= $('#shadow_cp_red').val();
  		var rgbGreen 	= $('#shadow_cp_green').val();
  		var rgbBlue		= $('#shadow_cp_blue').val();
  		var alpha		= $('#shadow_cp_alpha').val();

  		var rgbaValue = 'rgba(' + rgbRed + ',' + rgbGreen + ',' + rgbBlue + ',' + alpha + ')';

  		$('#shadow_colorpicker_val').colorpicker('setValue', rgbaValue);
  		//$('#stroke_colorpicker_val').trigger('change');
  	});

  	$('#shadow_cp_hex').on('blur', function(e) {
  		var rgbaString = ft.hexToRGBA('#' + $(this).val(), $('#shadow_cp_alpha').val());

  		if(!rgbaString) {
  			dialog.showMessageBox(
		    		options={buttons: ['OK'], type: 'error', title: 'Input error', message: 'Invalid Hex value.', detail: 'Please enter 6 digit hex value without leading # sign.'},
		    		function(buttonIndex){
		    			$('#shadow_cp_hex').focus();
    		});
  		} else {
  			$('#shadow_colorpicker_val').colorpicker('setValue', rgbaString);
  			//$('#stroke_colorpicker_val').trigger('change');
  		}
  	});

  	$('#shadow_cp_hex').keydown(function(e) {
		if(e.keyCode === 13) {
			$(this).trigger('blur');
		}
	});

	/////// Apply Textbox styles //////////
	$('.ft-text-bold').on('click', function(e) {
		var activeObject = canvas.getActiveObject();
		if(activeObject.fontWeight == undefined || activeObject.fontWeight == 'normal') {
			activeObject.fontWeight = 'bold';
			$(this).removeClass('ft-active').addClass('ft-active');
		} else if(activeObject.fontWeight == 'bold') {
			activeObject.fontWeight = 'normal';
			$(this).removeClass('ft-active');
		}
		canvas.renderAll();
	});

	// font style
	$('.ft-text-italic').on('click', function(e) {
		var activeObject = canvas.getActiveObject();
		
		if(activeObject.fontStyle == undefined || activeObject.fontStyle == 'normal') {
			activeObject.fontStyle = 'italic';
			$(this).removeClass('ft-active').addClass('ft-active');
		} else if(activeObject.fontStyle == 'italic') {
			activeObject.fontStyle = 'normal';
			$(this).removeClass('ft-active');
		}

		canvas.renderAll();
		
	});

	// text align
	$('.ft-txtal').on('click', function(e) {
		var activeObject = canvas.getActiveObject();
		var alignTo = $(this).data('ftalign');
		activeObject.textAlign = alignTo;
		$('.ft-txtal').removeClass('ft-active');
		$(this).addClass('ft-active');
		canvas.renderAll();
	});

	// font size
	$('#ft_font_size_slider').on('change', function(e){
		$('#ft_font_size').val(e.value.newValue);
		var activeObject = canvas.getActiveObject();
		activeObject.fontSize = parseFloat(e.value.newValue);
		canvas.renderAll();
	});

	$('#ft_font_size').keydown(function(e) {
		if(e.keyCode === 13) {
			$('#ft_font_size_slider').slider('setValue', $(this).val(), false, true);
		}
	});

	$('#textbox_fontsize').find('.slider-handle').dblclick(function(){
		$('#ft_font_size_slider').slider('setValue', 200, false, true);
	});

	// line height
	$('#ft_line_height').on('change', function(e){
		$('#line_height_value').html(e.value.newValue.toFixed(2));
		var activeObject = canvas.getActiveObject();
		activeObject.lineHeight = parseFloat(e.value.newValue);
		canvas.renderAll();
	});
	//reset on dluble click
	$('#textbox_lineheight').find('.slider-handle').dblclick(function(){
		$('#ft_line_height').slider('setValue', 1, false, true);
	});

	//////////// Shadow //////////////
	$('#enable_shadow').on('change', function(e){
		if($(this).prop('checked')) {
			ft.enableShadow();
		} else {
			ft.disableShadow();
		}

		ftCanvas.updateCanvasState();
	});

	$('#ft_shadow_spread_slider').on('change', function(e){
		var activeObject = canvas.getActiveObject();
		clearTimeout($.data(this, 'timer'));
	  	$.data(this, 'timer', setTimeout(function() {
	     	activeObject.shadow.blur = e.value.newValue;
			canvas.renderAll();
	  	}, 100));
	});

	$('#textbox_shadow_spread').find('.slider-handle').dblclick(function(){
		$('#ft_shadow_spread_slider').slider('setValue', 50, false, true);
	});

	$('#ft_shadow_offset_x_slider').on('change', function(e){
		var activeObject = canvas.getActiveObject();
		$.data(this, 'timer', setTimeout(function() {
	     	activeObject.shadow.offsetX = e.value.newValue;
			canvas.renderAll();
	  	}, 100));		
	});

	$('#textbox_shadow_offset_x').find('.slider-handle').dblclick(function(){
		$('#ft_shadow_offset_x_slider').slider('setValue', 0, false, true);
	});

	$('#ft_shadow_offset_y_slider').on('change', function(e){
		var activeObject = canvas.getActiveObject();
		$.data(this, 'timer', setTimeout(function() {
	     	activeObject.shadow.offsetY = e.value.newValue;
			canvas.renderAll();
	  	}, 100));		
	});

	$('#textbox_shadow_offset_y').find('.slider-handle').dblclick(function(){
		$('#ft_shadow_offset_y_slider').slider('setValue', 0, false, true);
	});

	$('#ft_stroke_slider').on('change', function(e){
		var activeObject = canvas.getActiveObject();
		if(activeObject.stroke == undefined) {
			activeObject.stroke = '#000000';
			$('#stroke_color').find('.diable-pannel').hide();
		}
		$.data(this, 'timer', setTimeout(function() {
	     	activeObject.strokeWidth = e.value.newValue;
			$('#stroke_width_value').val(e.value.newValue.toFixed(2));
			canvas.renderAll();
	  	}, 100));	
		
	});

	$('#stroke_width').find('.slider-handle').dblclick(function(){
		$('#ft_stroke_slider').slider('setValue', 0, false, true);
	});

	$('#stroke_width_value').on('change', function(e){
		$('#ft_stroke_slider').slider('setValue', $(this).val(), false, true);
	});

	$('#ft_opacity_slider').on('change', function(e){
		var activeObject = canvas.getActiveObject();
		
		$('#opacity_value').val(e.value.newValue.toFixed(2));
		$.data(this, 'timer', setTimeout(function() {
	     	activeObject.opacity = e.value.newValue;
			canvas.requestRenderAll();
	  	}, 100));	
	});

	/////////// Edit textbox text ////////////////
	$('#ft-txt-content').keyup(function(e){
		var activeObject = canvas.getActiveObject();
		activeObject.text = $(this).val();
		canvas.renderAll();
	});

  	function arrangeImages(delay=300, append=false, elements="") {
        setTimeout(function() {
        	if(!append){
        		$masonryGrid.masonry("layout");
        	} else {
    		 	$masonryGrid.masonry('appended', elements);
        	}

            ft.setAppStatus(status="", level='ok', popover="");
	        ft.hideSwal();
        }, delay);
    }

    //////////////// Fonts list /////////////////////
    var enFonts = ft.fonts.en;
    var sinhalaFonts = ft.fonts.sinhala;
    var enFontQuote = enFonts.fontQuote;

    const enOrdered = {};
	Object.keys(enFonts).sort().forEach(function(key) {
	  enOrdered[key] = enFonts[key];
	});
    
    var fontTemplate = '';
    $.each(enOrdered, function(k, fontObj) {
    	if(fontObj.fontFamily != undefined) {
    		var fontName = fontObj.fontFamily;
	    	var styles = fontObj.styles;
	    	var bold = italic = 'false';

	    	var styleBBadge = '<span class="badge inactive"><i class="fa fa-bold" aria-hidden="true"></i></span>';
	    	var styleIBadge = '<span class="badge inactive"><i class="fa fa-italic" aria-hidden="true"></i></span>';

	    	if(styles.indexOf('bold') >= 0) {
	    		styleBBadge = '<span class="badge active"><i class="fa fa-bold" aria-hidden="true"></i></span>';
	    		bold = 'true';
	    	}

	    	if(styles.indexOf('italic') >= 0) {
	    		styleIBadge = '<span class="badge active"><i class="fa fa-italic" aria-hidden="true"></i></span>';
	    		italic = 'true';
	    	}

	    	fontTemplate += '<div class="font-item" data-font-family="' + fontName + '" data-style-bold="' + bold + '" data-style-italic="' + italic + '">';
	    	fontTemplate += '<div class="font-name">' + fontName + '</div>';
	    	fontTemplate += '<div class="font-display" style="font-family:' + fontName + '">' + enFontQuote + '</div>';
	    	//fontTemplate += '<div class="available-styles" title="available styles">' + styleBBadge + ' ' + styleIBadge + '</div>';
	    	fontTemplate += '</div>';
	    	fontTemplate += '<hr>';
    	}
    });
    
    $('.en-list').html(fontTemplate);	

    const slOrdered = {};
	Object.keys(sinhalaFonts).sort().forEach(function(key) {
	  slOrdered[key] = sinhalaFonts[key];
	});

	var fontTemplate = '';
    $.each(slOrdered, function(k, fontObj) {
    	if(fontObj.fontFamily != undefined) {
    		var fontName = fontObj.fontFamily;
	    	var styles = fontObj.styles;
	    	var bold = italic = 'false';

	    	var styleBBadge = '<span class="badge inactive"><i class="fa fa-bold" aria-hidden="true"></i></span>';
	    	var styleIBadge = '<span class="badge inactive"><i class="fa fa-italic" aria-hidden="true"></i></span>';

	    	if(styles.indexOf('bold') >= 0) {
	    		styleBBadge = '<span class="badge active"><i class="fa fa-bold" aria-hidden="true"></i></span>';
	    		bold = 'true';
	    	}

	    	if(styles.indexOf('italic') >= 0) {
	    		styleIBadge = '<span class="badge active"><i class="fa fa-italic" aria-hidden="true"></i></span>';
	    		italic = 'true';
	    	}

	    	fontTemplate += '<div class="font-item" data-font-family="' + fontName + '" data-style-bold="' + bold + '" data-style-italic="' + italic + '">';
	    	fontTemplate += '<div class="font-name">' + fontName + '</div>';
	    	fontTemplate += '<div class="font-display" style="font-family:' + fontName + '">W;aiyjka;hd ch.kS\'\'\'</div>';
	    	//fontTemplate += '<div class="available-styles" title="available styles">' + styleBBadge + ' ' + styleIBadge + '</div>';
	    	fontTemplate += '</div>';
	    	fontTemplate += '<hr>';
    	}
    });

	$('.sl-list').html(fontTemplate);

	$('#fonts_tabs a').click(function (e) {
	  e.preventDefault();
	  $(this).tab('show');
	});

	$('#ft_font_face, .font-face-btn').on('click', function(e){
		e.preventDefault();
		$('#fonts_list').fadeIn(300);
	})

	$('.close-btn').on('click', function(e){
		e.preventDefault();
		var targetElemet = $(this).data('target');
		if(targetElemet != undefined) {
			$('#' + targetElemet).fadeOut(300);
		}
	});

	$('#fonts_list').on('click', '.font-item', function(e){
		var activeObject = canvas.getActiveObject();
		var fontFamily = $(this).data('font-family');
		//var isBold = $(this).data('style-bold') != 'true';
		//var isItalic = $(this).data('style-italic') != 'true';
		//activeObject.fontFamily = fontFamily;
		//$('.ft-text-bold').prop('disabled', isBold);
		//$('.fa-italic').prop('disabled', isItalic);
		ft.loadFontAndUse(fontFamily);
		//canvas.renderAll();
		$('#fonts_list .font-item').removeClass('selected');
		$(this).addClass('selected');
	});

    function arrangeUnusedImages(delay=300, append=false, elements="", callBack){
    	setTimeout(function(){
    		if(!append){
        		$masonryGridUnused.masonry("layout");
        	} else {
    		 	$masonryGridUnused.masonry('appended', elements);
        	}

        	ft.setAppStatus(status="", level='ok', popover="");
        	ft.hideSwal();

            if(callBack != undefined && typeof callBack == 'function') {
        		callBack();
        	}
    	}, delay);
    }

    function arrangeUsedImages(delay=300, append=false, elements="", callBack){
    	setTimeout(function(){
    		if(!append){
        		$masonryGridUsed.masonry("layout");
        	} else {
    		 	$masonryGridUsed.masonry('appended', elements);
        	}

        	ft.setAppStatus(status="", level='ok', popover="");
        	ft.hideSwal(); 

        	if(callBack != undefined && typeof callBack == 'function') {
        		callBack();
        	}           
    	}, delay);
    }

    var initScreen = false;
    $('li[aria-controls="develop"]').on('shown.bs.tab', function(e) {
    	if(!initScreen) {
    		$('.canvas-zoom[data-zoom="reset"]').trigger('click');
    		initScreen = true;
    	}
    });

    /*setTimeout(function(){	
		ipc.send('close-splash');
	}, 3000);*/


    $('#right_toolbar_content').on('mousewheel', '.slider', function(e){
    	//console.log(e);
    	var sliderId = $(this).attr('id');
    	$rangeId = $('input[data-slider-id="' + sliderId + '"]');

    	var isEnabled = $rangeId.slider('getAttribute', 'enabled');

    	if(isEnabled) {
    		var curVal = $rangeId.slider('getValue');
	    	var step = $rangeId.slider('getAttribute', 'step');

	    	if(e.deltaY === 1) {
	    		$rangeId.slider('setValue', (curVal+step), true, true);
	    	} else if(e.deltaY === -1) {
	    		$rangeId.slider('setValue', (curVal-step), true, true);
	    	}

	    	clearTimeout($.data(this, 'timer'));
			  $.data(this, 'timer', setTimeout(function() {
			     ftCanvas.updateCanvasState();
		  	}, 250));
    	}    	
    });

    var acc = $(".ft-accordion");
	var i;

	for (i = 0; i < acc.length; i++) {
	  	acc[i].onclick = function() {
		    this.classList.toggle("active");
		    var panel = this.nextElementSibling;
		    if (panel.style.maxHeight){
		      panel.style.maxHeight = null;
		    } else {
		      panel.style.maxHeight = panel.scrollHeight + "px";
		    } 
	  	}
	}

	$('.change-destination-btn').on('click', function(e){
		dialog.showOpenDialog({
		    properties: ['openDirectory', 'createDirectory', 'promptToCreate']
	  	}, function (dir_path) {
		    if(dir_path == undefined) {
		    	return false;
		    } else {
		    	projectJSON.exportPath = dir_path[0];
		    	$('#exp_destination_folder').val(dir_path[0]);
		    }
	  	});
	});

	$('#exportModal').on('show.bs.modal', function(e){
		var button = $(e.relatedTarget);
		var exportMode = button.data('export');

		$('#exp_destination_folder').val(projectJSON.exportPath);
		$('#exp_filename').val($('#selected_page').val() + '.jpg');		
	});

	$('#exportModal').on('hidden.bs.modal', function(e){
		$('#exp_destination_folder').val('');
		$('#exp_filename').val('');
	});

	$('.export-file-btn').on('click', function(e) {
		$('#exportModal').modal('show');		
	});

	$('.export-jpg-btn').on('click', function(e){
		e.preventDefault();
		ft.saveProject(canvas, isNew=false, false);

		// hide guide lines
		var canvasObjects = canvas.getObjects();
		$.each(canvasObjects, function(k, obj){
			if(obj.ftExportable != undefined && obj.ftExportable === false) {
				obj.visible = false;
			}
		});

		canvas.discardActiveObject();
		canvas.renderAll();

		var destinationFolder = $('#exp_destination_folder').val();
		var pageNumber = $('#selected_page').val();
		var expFileName = $('#exp_filename').val();
		var jpgQuality = $('#exp_quality').slider('getValue');
		var isOverwrtire = $('#exp_overwrite').prop('checked');

		if(path.extname(expFileName).toLowerCase() != '.jpg') {
			expFileName = expFileName + '.jpg';
		}

		if(!isOverwrtire) {
			if(fs.existsSync(path.join(destinationFolder, expFileName))){
				var baseFileName = path.basename(expFileName, '.jpg');

				var incrementalPart = 1;
				expFileName = baseFileName + '_' + incrementalPart + '.jpg';
				while(fs.existsSync(path.join(destinationFolder, expFileName))) {
					incrementalPart++;
					expFileName = baseFileName + '_' + incrementalPart + '.jpg';
				}
				
			}
		}

		$('#exportModal').modal('hide');

		ft.loadingDialog('Exporting ' + expFileName + '...');
		ipc.on('file-download', function(event, downState, args=null){
			var downPercentage;
			if(args) {
				downPercentage = JSON.parse(`${args}`).percentage;
			}
			
			if(downState == 'downloading') {
				ft.progress(downPercentage);
			} else if(downState == 'completed') {
				// write exif data
				var savedFilePath = path.join(destinationFolder, expFileName);
				var jpeg = fs.readFileSync(savedFilePath);
				var imdata = jpeg.toString("binary");

				var zeroth = {};
				var exif = {};
				var gps = {};

				var saveDate = new Date();
				var monthVal = (saveDate.getMonth() + 1) < 10 ? '0' + (saveDate.getMonth() + 1) : (saveDate.getMonth() + 1);
				var dateVal = saveDate.getDate() < 10 ? '0' + saveDate.getDate() : saveDate.getDate();

				var exifDateStr = saveDate.getFullYear() + ":" + monthVal + ":" + dateVal + " " + saveDate.getHours() + ":" + saveDate.getMinutes() + ":" + saveDate.getSeconds();
				//zeroth[piexif.ImageIFD.Make] = "Make";
				//zeroth[piexif.ImageIFD.XResolution] = [777, 1];
				//zeroth[piexif.ImageIFD.YResolution] = [777, 1];
				zeroth[piexif.ImageIFD.Software] = "PhotoBook " + getConfig('version') + " (" + getConfig('platform') + ")";
				exif[piexif.ExifIFD.DateTimeOriginal] = exifDateStr;
				//exif[piexif.ExifIFD.LensMake] = "LensMake";
				//exif[piexif.ExifIFD.ColorSpace] = 2;
				//exif[piexif.ExifIFD.LensSpecification] = [[1, 1], [1, 1], [1, 1], [1, 1]];
				//gps[piexif.GPSIFD.GPSVersionID] = [7, 7, 7, 7];
				//gps[piexif.GPSIFD.GPSDateStamp] = "1999:99:99 99:99:99";
				var exifObj = {"0th":zeroth, "Exif":exif, "GPS":gps};
				var exifbytes = piexif.dump(exifObj);

				var newData = piexif.insert(exifbytes, imdata);
				var newJpeg = new Buffer(newData, "binary");
				fs.writeFileSync(savedFilePath, newJpeg);
				//var exifData = piexif.load(imdata);
				//console.log(exifData); 
				//return;

				ft.resetProgress();
				ft.hideSwal();

				ipc.removeAllListeners('file-download');
			}
		});
		
		$('#ft_canvas').get(0).toBlob(function(blob){
			//var exifData = piexif.load(imgDataURL);

			saveAs(blob, expFileName);

			$.each(canvasObjects, function(k, obj){
				if(obj.ftExportable != undefined && obj.ftExportable === false) {
					obj.visible = true;
				}
			});

			projectJSON.pages['page' + pageNumber] = 2;
			ft.saveProject(canvas);
			$('#project_pages').find('.page-pannel').find('div[data-page="' + pageNumber + '"]').removeClass('status_1').addClass('status_2');
		}, 'image/jpeg', jpgQuality);
		
	})

	$('.page-pannel-btn').on('click', function(e){
		e.preventDefault();
		$('#project_pages').find('.page-pannel').fadeIn(100, function(){
			hideOnClickOutside($('#project_pages').find('.page-pannel'));
		});
	});

	// switch pages
	$('#project_pages').on('click', '.page-pannel-item', function(e){
		e.preventDefault();
		if($(this).hasClass('current')){
			return;
		}
		ft.saveProject(canvas, isNew=false, false);
		$this = $(this);

		console.log('page change triggered');
		$('#color_pallete').html('');

		var pageNumber = $(this).data('page');

		ft.loadingDialog('Loading page ' + pageNumber + '...');
		ft.setAppStatus(status="", level='busy', popover=""); 
		
		var tmplJSON = gotoTemplatePage(pageNumber);

		if(tmplJSON == 'page not found') {
			dialog.showMessageBox({
				buttons: ['OK'],
				type: 'info',
				title: 'Photobook',
				message: 'Page not available',
				detail: 'This template doesn\'t have the requested page.'
			}, function(btnIndes){
				return false;
			});
		} else if(tmplJSON != false) {
			canvas.clear();
			canvas.loadFromJSON(tmplJSON, function(){
				canvas.defaultCursor = 'default';
				canvas.hoverCursor = 'pointer';

				canvas.renderAll.bind(canvas);

				undoReady = true; // ready to start undo/redo

				ft.setLayersList(canvas);

				$('#fill_color').find('.diable-pannel').hide();
				$('#fill_colorpicker_val').colorpicker('setValue', canvas.backgroundColor);

				if(parseInt(projectJSON.pages['page' + pageNumber]) == 0) {
					projectJSON.pages['page' + pageNumber] = '1';
					$this.addClass('status_1');
				}
				$('#project_pages').find('.page-pannel-item').removeClass('current');
				$this.addClass('current');

				$('#selected_page').val(pageNumber);

				$.each(canvas.getObjects(), function(k, obj){
					if(!$.isEmptyObject(obj.ftColorPallete)) {
	  					var colorPallete = obj.ftColorPallete;
	  					var colorPalleteTemplate = "";
						$.each(colorPallete, function(colname, hex){
							colorPalleteTemplate += "<div class='color-pallete-color' data-color='" + hex + "' style='background:" + hex + "'></div>";
						});

						colorPalleteTemplate += "<div class='color-pallete-color pick-a-color' title='pick a color'><div class='glyph-icon flaticon-pipette'></div></div>";

						$('#color_pallete').html(colorPalleteTemplate);
	  				}
				});					

				ft.setAppStatus(status="", level='ok', popover="");
				ft.hideSwal();
				ft.saveProject(canvas, isNew=false, false);
				ftCanvas.resetHistoryStack();
			});
		}
		
	});

	function hideOnClickOutside(selector) {
	  	const outsideClickListener = (event) => {
		    if (!$(event.target).closest(selector).length) {
		      if ($(selector).is(':visible')) {
		        $(selector).fadeOut(100)
		        removeClickListener()
		      }
		    }
	  	}

	  	const removeClickListener = () => {
	    	document.removeEventListener('click', outsideClickListener)
	  	}

	  	document.addEventListener('click', outsideClickListener)
	}

	$('#use_tmpl_btn_on_modal').on('click', function(e){
		e.preventDefault();

		var tmplItem = $(this).data('tmplitem');
		tmplItem.trigger('click');
	})
	
	$('#template_list').on('click', '.template-item', function(e){
		e.preventDefault();
		e.stopPropagation();

		$this = $(this);
		var templateId = $(this).data('tmpl-id');

		if(templateId == projectJSON.template || $this.hasClass('size-not-available')) {
			return false;
		}

		dialog.showMessageBox({buttons: ["Yes", "No"], type: 'question', title: 'Apply template', message: 'Apply template', detail: 'Are you sure, do you want to use this template for your project?'},
			function(btnIndex){
				if(btnIndex == 0) {
					$('#template_preview_modal').modal('hide');
					ft.loadingDialog('Template is loading...');
					ft.setAppStatus(status="", level='busy', popover="");

					//var templateIndex = $(this).data('tmpl-index');
					var albumSize = ft.currentCanvasDim();
					var templSize = albumSize.canvasHeight + "x" + albumSize.canvasWidth;

					projectJSON.album_size = templSize;
					projectJSON.template = templateId;

					var newTemplateName = getTemplateName(templateId);

					var tmplJSON = switchTemplates(projectJSON);
					if(tmplJSON === false) {
						ft.hideSwal();
						ft.setAppStatus(status="", level='error', popover='Template loading failed');
					} else {
						canvas.clear();
						canvas.loadFromJSON(tmplJSON, function(){
							canvas.defaultCursor = 'default';
							canvas.hoverCursor = 'pointer';

							canvas.renderAll.bind(canvas);

							undoReady = true;

							ft.setLayersList(canvas);
							$('#template_list').find('.template-item').removeClass('ft-selected-card');
							$this.addClass('ft-selected-card');							
							$this.find('btn').prop('disabled', true);
							$('#ft_maintabs a[href="#develop"]').trigger('click');
							ft.saveProject(canvas, isNew=false, false);
							ft.setAppStatus(status="", level='ok', popover="");
							$('#project_list').find('.ft-selected-card').find('.ft-tmpl-name').html(newTemplateName);
							ft.hideSwal();
						});
					}
				}
			}
		);
	});

	// create new project
	$('.new-project-btn').on('click', function(e){
		if(projectName != "") {
			dialog.showMessageBox(
	    		options={buttons: ['Yes, go ahead', 'Cancel'], type: 'question', title: 'Create PhotoBook project', message: 'Create new project?', detail: 'Do you want to close current project and create new one?'},
    			function(buttonIndex){
	    			if(buttonIndex == 0) {
	    				$('#canvasSizeModal').modal('show');
	    			}
    			}
			);
		}
	});

	// switch projects
	$('#project_list').on('click', '.ft-project-card', function(e){
		e.preventDefault();
		e.stopPropagation();

		$this = $(this);
		var switchProjectName = $this.data('project-name');
		if(switchProjectName == projectName) {
			return false;
		}
		if(switchProjectName != "") {
			ft.saveProject(canvas);
		}
		ft.switchProjects(switchProjectName);
		
	});

	//delete all projects
	$('.delete-all-projects-btn').on('click', function(e){
		e.preventDefault();
		dialog.showMessageBox({
			buttons: ['Yes, delete all', 'No, go back'],
			type: 'question',
			title: 'Delete projects',
			message: 'Delete all projects',
			detail: 'Do you wand to delete all your projects?\nRemember, you cannot undo this action.'
		}, function(btnIndex){
			if(btnIndex == 1) {
				return;
			} else {
				var projectsArray = getConfig('projects');
				$.each(projectsArray, function(k, projectName){
					var projectPath = path.join(getConfig('workspace'), projectName);
					try {
						fse.removeSync(projectPath);
					} catch(err) {
						ft.logError(err.message);
						dialog.showErrorBox('Delete all projects', 'Project deleting failed!');
					}					
				});

				if(setConfig({'projects': [], 'current_project': '', 'current_page': 1})) {
					$('#project_list').html();
					$('#project_list').addClass('no-data');
					dialog.showMessageBox({
						buttons: ['OK'],
						title: 'Delete all projects',
						type: 'info',
						message: 'All projects deleted successfully.',
						detail: 'All projects deleted successfully. PhotoBook app will reload its content now.'
					}, function(btnIndex){
						location.reload();
					});
				}
			}
		});
	});

	// delete project
	$('#project_list').on('click', '.delete-project-btn', function(e){
		e.preventDefault();
		e.stopPropagation();
		$this = $(this);
		var delProjectName = $this.closest('.ft-project-card').data('project-name');
		//console.log(delProjectName); return;
		dialog.showMessageBox(options={buttons: ['Delete', 'No, go back'], type: 'question', title: 'Delete project', message: 'Do you really want to delete this project?'},
			function(btnIndex){
				if(btnIndex == 0) {
					//ft.loadingDialog('Project is deleting...');
					ft.setAppStatus(status="", level='busy', popover="");
					ft.saveProject(canvas);

					var projectsArray = getConfig('projects');
					var projectPath = path.join(getConfig('workspace'), delProjectName);
					fse.remove(projectPath, (err) => {
						if(err) {
							ft.logError(err)
							ft.setAppStatus(status="", level='error', popover="");
							dialog.showMessageBox({buttons: ["OK"], type: "error", title: "Delete project", message: "Project deleted process failed."});
						} else {
							var delIndex = projectsArray.indexOf(delProjectName)
							if(delIndex > -1) {
								projectsArray.splice(delIndex, 1)
							}
							
							if(setConfig({'projects': projectsArray})) {
								ft.setAppStatus(status="", level='ok', popover="");
								dialog.showMessageBox({buttons: ["OK"], type: "info", title: "Delete project", message: "Project deleted."},
									function(btnIndex){
										if(btnIndex == 0) {
											location.reload();
										}
									}
								);
							} else {
								ft.setAppStatus(status="", level='error', popover="");
								dialog.showMessageBox({buttons: ["OK"], type: "error", title: "Delete project", message: "Project deleted process failed."});
							}
						}
					});
				}
			}
		);
	});

	$('.activation-sign').on('click', function(e){
		ipc.send('show-about-page');
	});

	// Market place
	isOnline().then(online => {
		//console.log(online);
		$('#shop_items').find('.tab-content').find('.items').removeClass('loader');
		if(online === false) {
			$('#shop_items').find('.tab-content').addClass('no-connection');
		} else {
			$('#shop_items').find('.tab-content').removeClass('no-connection');
			ipc.send('load-market-place');
			ipc.on('load-market-place-resp', (event, args) => {
				if(args) {
					var resp = JSON.parse(`${args}`);
					var templateItems = JSON.parse(resp.templates);
					var appsUpdates = resp.apps_updates;

					var updateItemCount = appsUpdates.length + templateItems.length;
					
					if(updateItemCount > 0) {
						$('#update_item_count').html(updateItemCount);
						$('#update_item_count').show();
					} else {
						$('#update_item_count').hide();
					}
					

					var templatesShopTemp = "<table width='100%'>";
					var newTemplates = [];
					if(templateItems.length > 0) {
						templatesShopTemp += "<tr><td colspan='2'><button class='btn btn-default install-template-btn'><i class='fa fa-download' aria-hidden='true'></i> Install all</button></td></tr>";
					} else {
						templatesShopTemp += "<tr><td colspan='2'><button class='btn btn-default checkforupdates-btn'><i class='fa fa-refresh' aria-hidden='true'></i> Check for updates</button></td></tr>";
					}
					$.each(templateItems, function(k, templateItem){
						if(templateItem.payment_status == 2 || templateItem.payment_status == 0) {
							var actionBtn = "<span class='label label-danger'>NOT PAID</span>";
							if(templateItem.payment_status == 2) {
								//actionBtn = "<a href='#' class='btn btn-sm download-temp-btn' data-item-id='" + templateItem.id + "'>Install</a>";
								actionBtn = "<span class='label label-success'><i class='fa fa-check' aria-hidden='true'></i> Ready to install</span>";
								newTemplates.push(templateItem.id);
							}
							
							templatesShopTemp += "<tr class='shop-item'><td>" + templateItem.name + "</td><td class='text-right'>" + actionBtn + "</td></tr>";
						}						
					});
					templatesShopTemp += "</table>";
					templatesShopTemp += "<input type='hidden' name='new_template_ids' id='new_template_ids' value='" + newTemplates + "' />";

					$('#templates_shop').find('.items').html(templatesShopTemp);
					$('#app_updates').find('.items').html(appsUpdates);

					$('#shop_items').find('.tab-content').removeClass('empty-shop');
				}
				

				ipc.removeAllListeners('load-market-place-resp');
			});
		}
	});

	$('a[href="#ft_shop"]').on('show.bs.tab', function(event){
		if(!$('#shop_items').find('.tab-content').hasClass('empty-shop')) {
			return;
		}
		$('#shop_items').find('.tab-content').find('.items').addClass('loader');
		checkForUpdates();
	});

	function checkForUpdates(){
		isOnline().then(online => {
			//console.log(online);
			$('#shop_items').find('.tab-content').find('.items').removeClass('loader');
			if(online === false) {
				$('#shop_items').find('.tab-content').addClass('no-connection');
			} else {
				$('#shop_items').find('.tab-content').removeClass('no-connection');
				ipc.send('load-market-place');
				ipc.on('load-market-place-resp', (event, args) => {
					if(args) {
						var resp = JSON.parse(`${args}`);
						var templateItems = JSON.parse(resp.templates);
						var appsUpdates = resp.apps_updates;

						var updateItemCount = appsUpdates.length + templateItems.length;

						if(updateItemCount > 0) {
							$('#update_item_count').html(updateItemCount);
							$('#update_item_count').show();
						} else {
							$('#update_item_count').hide();
						}
						

						var templatesShopTemp = "<table width='100%'>";
						var newTemplates = [];
						if(templateItems.length > 0) {
							templatesShopTemp += "<tr><td colspan='2'><button class='btn btn-default install-template-btn'><i class='fa fa-download' aria-hidden='true'></i> Install all</button></td></tr>";
						} else {
							templatesShopTemp += "<tr><td colspan='2'><button class='btn btn-default checkforupdates-btn'><i class='fa fa-refresh' aria-hidden='true'></i> Check for updates</button></td></tr>";
						}
						$.each(templateItems, function(k, templateItem){
							if(templateItem.payment_status == 2 || templateItem.payment_status == 0) {
								var actionBtn = "<span class='label label-danger'>NOT PAID</span>";
								if(templateItem.payment_status == 2) {
									//actionBtn = "<a href='#' class='btn btn-sm download-temp-btn' data-item-id='" + templateItem.id + "'>Install</a>";
									actionBtn = "<span class='label label-success'><i class='fa fa-check' aria-hidden='true'></i> Ready to install</span>";
									newTemplates.push(templateItem.id);
								}
								
								templatesShopTemp += "<tr class='shop-item'><td>" + templateItem.name + "</td><td class='text-right'>" + actionBtn + "</td></tr>";
							}						
						});
						templatesShopTemp += "</table>";
						templatesShopTemp += "<input type='hidden' name='new_template_ids' id='new_template_ids' value='" + newTemplates + "' />";

						$('#templates_shop').find('.items').html(templatesShopTemp);
						$('#app_updates').find('.items').html(appsUpdates);

						$('#shop_items').find('.tab-content').removeClass('empty-shop');
					}
					

					ipc.removeAllListeners('load-market-place-resp');
				});
			}
		});
	}

	// set app name
	$('#app_name_label').on('click', '.set-app-name-btn', function(e){
		isOnline().then(online => {
			if(online === true) {
				swal({
				  	title: 'Set your app name',
				  	html: 'Remember! once you set the app name, you cannot change it.',
				  	input: 'text',
				  	inputPlaceholder: 'App name (max 15 characters)',
				  	showCancelButton: true,
				  	inputValidator: (value) => {
				  		if(value.length > 15) {
				  			return 'Use shorter name, less than 15 characters.'
				  		}
					    return !value && 'Please enter the app name!'
				  	}
				}).then((app_name) => {
					if(app_name.dismiss == 'cancel') {
						return;
					} else {
						ipc.send('set-app-name', app_name.value);
						ipc.on('set-app-name-resp', (event, args) => {
							var response = JSON.parse(`${args}`);
							if(response.resp === true) {
								$('#app_name_label').html(app_name.value);
								dialog.showMessageBox({buttons: ['OK'], type: 'info', title: 'Set app name', message: 'App name is set', detail: 'The app name is set successfully!'})
							} else if(response.resp === 'exists') {
								dialog.showMessageBox({buttons: ['OK'], type: 'info', title: 'Set app name', message: 'App name is exists', detail: 'The app name is already used.\nPlease try another name.'},
									function(btnIndex){
										$('#app_name_label').find('.set-app-name-btn').trigger('click');
									});
							}

							ipc.removeAllListeners('set-app-name-resp');						
						})
					}
				})						
			} else {
				dialog.showMessageBox({buttons: ['OK'], type: 'info', title: 'Set app name', message: 'No internet connection', detail: 'You must have an internet connection for setting up your app name.'})
			}
		});
		
	});

	$('#templates_shop').on('click', '.install-template-btn', function(e){
		var type = 'template';
		var tempIds = $('#new_template_ids').val();

		var body = {updateType: type, app_serial: getConfig('serial'), itemIds: tempIds}
		fetch(photobookWeb + "/api/appupdates/auth", {method: 'POST', body: JSON.stringify(body), headers:{'Content-Type': 'application/json'}, timeout: 5000})
        .then(res => res.json())
        .then(respJson => {
        	if(!respJson.authResp) {
        		dialog.showMessageBox({
        			buttons: ['OK'],
        			type: 'info',
        			title: 'Install Templates',
        			message: 'Nothing to install',
        			detail: 'seems like you do not have any paid items to be install.'
        		}, function(btnIndex){
        			return;
        		});
        	} else {
        		tempIds = respJson.authResp;
        		ipc.send('installer', JSON.stringify({installType: type, itemIds: tempIds}));
        	}
        });
	});

	$('#templates_shop').on('click', '.checkforupdates-btn', function(e){
		checkForUpdates();
	});

	$('#unused').on('click', '.remove-unused', function(e){
		e.preventDefault()

		$this = $(this);
		var randId = $this.data('file-id');

		swal({
			html: 'Do you want to remove this image from the list?',
			showCancelButton: true,
			showConfirmButton: true,
		}).then((clicked) => {
			if(clicked.value === true) {
				unusedFiles.splice($.inArray(randId, unusedFiles), 1);
				$this.parent('.img-container-outer').remove();
				$('.unused-count').html(unusedFiles.length);

				projectJSON.files[randId]['is_added'] = 0;

				ft.saveProject(canvas, isNew=false, false);
			} else if(clicked.dismiss == 'cancel'){
				return;
			}
		});
		
	});

	// flip images
	$('.flip-btn').on('click', function(e){
		e.preventDefault();

		var flipDirection = $(this).data('flip');
		var activeObject = canvas.getActiveObject();
		if(activeObject) {
			if(flipDirection == 'x') {
				var thisFlipX = !activeObject.flipX;
				activeObject.set('flipX', thisFlipX);
			} else if(flipDirection == 'y') {
				var thisFlipY = !activeObject.flipY;
				activeObject.set('flipY', thisFlipY);
			}

			canvas.requestRenderAll();
		}
	});

   /* $('input').on('input', function(e){
    	if(this.type == 'number' && this.maxLength != undefined) {
    		var str = String(this.value).slice(0,this.maxLength);
    		this.value = str;
    	}
    });*/

    // write config values
	function setConfig(configObj) {
		try {
			var confDataObj = JSON.parse(fs.readFileSync(ftConfigFilePath, 'utf8'))
			var configKeys = Object.keys(configObj)
			configKeys.forEach(function(confKey) {
				var confVal = configObj[confKey]
				confDataObj[confKey] = confVal
				fs.writeFileSync(ftConfigFilePath, JSON.stringify(confDataObj), 'utf8')
			})		
		} catch(err) {
			ft.logError(err.message)
			return err.message
		}

		return true
	}

	// get config values
	function getConfig(confKey) {
		try {
			var confDataObj = JSON.parse(fs.readFileSync(ftConfigFilePath, 'utf8'))
			return confDataObj[confKey];
		} catch(err) {
			ft.logError(err.message)
			return err.message;
		}
	}

	// get template name from ID
	function getTemplateName(tmplId) {
		var tempJSONPath = path.join(templatesPath, tmplId.toString(), 'template.json');
		if(fs.existsSync(tempJSONPath)) {
			var tmplObj = JSON.parse(fs.readFileSync(tempJSONPath, 'utf8'));

			return tmplObj.name;
		} else {
			return '-';
		}
		
	}

	// get project JSON values
	function getProjectJSONValue(confKey) {
		var workspace = getConfig('workspace')
		var projectName = getConfig('current_project')
		var projectJSONFilePath = path.join(workspace, projectName, 'project.json')

		try {
			var confDataObj = JSON.parse(fs.readFileSync(projectJSONFilePath, 'utf8'))
			return confDataObj[confKey]
		} catch(err) {
			ft.logError(err.message);
			return false;
		}
	}

	// update project JSON file
	function updateProjectJSON(newJSONString) {
		var workspace = getConfig('workspace')
		var projectName = getConfig('current_project')
		var projectJSONFilePath = path.join(workspace, projectName, 'project.json')

		try {
			if(typeof(newJSONString) == 'object') {
				fs.writeFileSync(projectJSONFilePath, JSON.stringify(newJSONString))
			} else if(typeof(newJSONString) == 'string') {
				fs.writeFileSync(projectJSONFilePath, newJSONString)
			}

			return true
		} catch(err) {
			ft.logError(err.message)
			return err.message
		}
	}

	// switch templates
	function switchTemplates(pjDataObj) {
		undoReady = false;
		var tmplId = pjDataObj.template
		var tmplSize = pjDataObj.album_size

		var tmplJSONFile = path.join(templatesPath, tmplId.toString(), tmplSize, '1.json')

		try {
			setConfig({'current_page': '1'})
			updateProjectJSON(JSON.stringify(pjDataObj))
			var templateJSON = fs.readFileSync(tmplJSONFile, 'utf8')
			return templateJSON;
		} catch(err) {
			ft.logError(err.message)
			return false;
		}
	}

	// change template page
	function gotoTemplatePage(tmplPageNumber) {
		undoReady = false;
		var tmplId = getProjectJSONValue('template')
		var tmplSize = getProjectJSONValue('album_size')
		var prPages = getProjectJSONValue('pages')
		var currentPageStatus = parseInt(prPages['page' + tmplPageNumber])
		var tmplPath = ""

		if(currentPageStatus > 0) {
			// page already used and saved
			var workspace = getConfig('workspace')
			var projectName = getConfig('current_project')
			tmplPath = path.join(workspace, projectName, tmplPageNumber + '.json')

		} else {
			// fresh page
			tmplPath = path.join(templatesPath, tmplId.toString(), tmplSize, tmplPageNumber + '.json')
		}
		//console.log(tmplPath)
		if(!fs.existsSync(tmplPath)) {
			return 'page not found';
		} else {
			setConfig({'current_page': tmplPageNumber})
			var newPageJSON = fs.readFileSync(tmplPath, 'utf8')
			return newPageJSON;
		}
	}

	// switch projects
	function switchProject(switchProjectId) {
		var workspaceDir = getConfig('workspace');
		return setConfig({'current_project': switchProjectId, 'current_page': '1'}) === true;
	}

	function tempSecret(tempStr) {
		const salt = "asduerqe32%dasd@f*gjut1s3"
		const hash = crypto.createHash('sha256')
		return hash.update(tempStr + salt).digest('hex')
	}

	function getTemplatesString() {
		var templatesString = "";
		try {
			var templates = fs.readdirSync(templatesPath);
			var temlateIds = [];
			templates.forEach(function(tempDirName){
				if(tempDirName != 'img') {
					temlateIds.push(tempDirName)				
				}			
			});
		} catch(err) {
			ft.logError(err.message)
			return false
		}

		temlateIds.sort()

		return temlateIds.toString()
	}

	// apply fixed filters
	$('.fixed-filter').on('click', function(e){
		e.preventDefault();

		if($(this).hasClass('active-filter')){
			applyFilter(6, null);	
			$('.fixed-filter').removeClass('active-filter');		
		} else {
			if(filters.length == 0) {
				initFilters();
			}
			var filterVal = $(this).data('filter');
			applyFilter(6, null);
			switch(filterVal){
				case "Sepia":
					applyFilter(6, new fabric.Image.filters.Sepia());
					break;
				case "Brownie":
					applyFilter(6, new fabric.Image.filters.Brownie());
					break;
				case "Vintage":
					applyFilter(6, new fabric.Image.filters.Vintage());
					break;
				case "Kodachrome":
					applyFilter(6, new fabric.Image.filters.Kodachrome());
					break;
				case "Technicolor":
					applyFilter(6, new fabric.Image.filters.Technicolor());
					break;
				case "Polaroid":
					applyFilter(6, new fabric.Image.filters.Polaroid());
					break;
				default:
					applyFilter(6, null);
			}

			$('.fixed-filter').removeClass('active-filter');
			$(this).addClass('active-filter');
		}
	});

	var clipByName = function (pathCode) {
		var ctx = canvas.getContext('2d');
	    ctx.save();
	    ctx.beginPath();
	  
	  	var path = new fabric.Path(pathCode);
	  	path._renderPathCommands(ctx);
	    ctx.restore();
	}

    $('.scrollbar-macosx').scrollbar();
    $('.scrollbar-inner').scrollbar();
	$('[data-toggle="popover"]').popover();
	$('.type-bg').show();
});

