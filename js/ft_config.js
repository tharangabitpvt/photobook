$(document).ready(function(){
	var ft = {
		'editImgPath': 'images/edit/',
		'projectJSON': {
			name: '',
			files: {}, // {filename1: {path: '', use_count: 0}, filename2: {path: '', use_count: 0}, ...}
			resolution: '',
			dpi: 72,
			template: 1
		},

		'addImage': function(fileName) {
			var imagePane = $('#image_pane');
			imagePane.attr('src', ft.editImgPath + fileName);
		},

		'setDPI': function (canvas, dpi) {
		    // Set up CSS size.
		    canvas.style.width = canvas.style.width || canvas.width + 'px';
		    canvas.style.height = canvas.style.height || canvas.height + 'px';

		    // Get size information.
		    var scaleFactor = dpi / 96;
		    var width = parseFloat(canvas.style.width);
		    var height = parseFloat(canvas.style.height);

		    // Backup the canvas contents.
		    var oldScale = canvas.width / width;
		    var backupScale = scaleFactor / oldScale;
		    var backup = canvas.cloneNode(false);
		    backup.getContext('2d').drawImage(canvas, 0, 0);

		    // Resize the canvas.
		    var ctx = canvas.getContext('2d');
		    canvas.width = Math.ceil(width * scaleFactor);
		    canvas.height = Math.ceil(height * scaleFactor);

		    // Redraw the canvas image and scale future draws.
		    ctx.setTransform(backupScale, 0, 0, backupScale, 0, 0);
		    ctx.drawImage(backup, 0, 0);
		    ctx.setTransform(scaleFactor, 0, 0, scaleFactor, 0, 0);
		},

		// set canvas size according to the sie of the album page
		'setCanvasSize': function(fabCanvas, canHeight=10, canWidth=24, dpi=300) {
			var widthInPx = canWidth * dpi;
			var heightInPx = canHeight * dpi;

			var editContainerWidth = parseFloat($('#ft_image_editor').css('width'));
			var editContainerHeight = parseFloat($('#ft_image_editor').css('height'));

			var workAreaHeight = canvasFitHeight = window.innerHeight - 100;
			var workAreaWidth = canvasFitWidth = (workAreaHeight * widthInPx)/heightInPx;

			fabCanvas.setDimensions({width: widthInPx, height: heightInPx}, {backstoreOnly: true});

			//console.log('edit container W:' + workAreaWidth + ' ' + editContainerWidth + ' height:'+editContainerHeight);

			ft.fitToScreen(fabCanvas);	
			$('#resolution_val').html(heightInPx + ' x ' + widthInPx + ' (' + dpi + 'dpi)');		
		},

		// set zoom level
		'zoomCanvas': function (canvas, factor) {
			if(factor == 1) {
				//console.log('fit to screen triggered!');
				ft.fitToScreen(canvas);
			} else {
				//var canvasCssWidth = parseFloat($('#ft_canvas').css('width'));

				var newWidth= (100 * factor) + "%";
				var newHeight  = "auto";
			
				canvas.setDimensions({width: newWidth, height: newHeight}, {cssOnly: true});				
			}
			canvas.calcOffset();

			ft.vCenterCanvas();

		},

		'vCenterCanvas': function() {
			var canvasObj = $('#ft_canvas');
			var canvasHeight = parseFloat(canvasObj.css('height'));
			var canvasWidth = parseFloat(canvasObj.css('width'));
			var editContainerHeight = parseFloat($('#ft_image_editor').css('height')) - 10;
			var editConteinerWidth = parseFloat($('#ft_image_editor').css('width')) - 10;
			
			if(canvasHeight < editContainerHeight) {
				$('.canvas-container').css({'top': (editContainerHeight/2) + 'px', 'margin-top': '-' + (canvasHeight/2) + 'px'});
			}
		},

		'fitToScreen': function(fabCanvas) {
			var canvasCurrentCSSHeight = parseFloat($('#ft_canvas').css('height'));
			var workAreaHeight = window.innerHeight - 100;

			//fabCanvas.setDimensions({width: 'auto', height: workAreaHeight + 'px'}, {cssOnly: true});
			$('.canvas-container').css({width: 'auto', height: workAreaHeight + 'px'});
			$('#ft_canvas').css({width: 'auto', height: workAreaHeight + 'px'});

			var editContainerWidth = parseFloat($('#ft_image_editor').css('width')) - 10;
			var editContainerHeight = parseFloat($('#ft_image_editor').css('height')) - 10;

			var canvasCssWidth = parseFloat($('#ft_canvas').css('width'));
			var canvasCssHeight = parseFloat($('#ft_canvas').css('height'));

			if(canvasCssWidth > editContainerWidth) {
				$('.canvas-container').css({width: editContainerWidth + 'px', height: 'auto'});
				fabCanvas.setDimensions({width: editContainerWidth + 'px', height: 'auto'}, {cssOnly: true});
				ft.vCenterCanvas();
			}

			fabCanvas.renderAll();
			fabCanvas.calcOffset();

		},

		// get canvas dimentions in inches
		'currentCanvasDim': function() {
			var canvasSizeArray = $('#canvas_size').data('size').split(",");
			return {canvasHeight: canvasSizeArray[0], canvasWidth: canvasSizeArray[1]}
		},

		'calcZoomValue': function() {
			var canvasCssWidth = parseFloat($('#ft_canvas').css('width'));
			var actualZoomVal = (canvasCssWidth/canvas.width) * 100;
			$('#zoom_value').html(Math.round(actualZoomVal) + "%");
		},

		'resizeControls': function(canvas) {
			var objects = canvas.getObjects();
			var newDip = $('#dpi').val();
			for(i=0; i < objects.length; i++) {
				objects[i].cornerSize = Math.round((13 * newDip)/72);
				objects[i].rotatingPointOffset = Math.round((40 * newDip)/72);
				objects[i].transparentCorners = false;
			}
		},

		'setAppStatus': function(status='', level='ok', popover="", show=false){ // level = ['ok', 'warning', 'error', critical]
			var levels = {
							ok		: 'fa-check-circle',
							info	: 'fa-info-circle',
							busy	: 'fa-circle-o-notch fa-spin fa-fw',
							warning	: 'fa-exclamation-circle',
							error	: 'fa-exclamation-circle text-danger blink',
							critical: 'fa-exclamation-triangle text-danger blink'
						}
			if(popover != "") {
				var statusStr = '<a id="app_status_popover" tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-placement="top" data-animation="true" data-content="' + popover + '"><i class="fa ' + levels[level] + '" aria-hidden="true"></i> ' + status + '</a>';
			} else {
				var statusStr = '<div><i class="fa ' + levels[level] + '" aria-hidden="true"></i> ' + status + '</div>';
			}

			$('#app_status').html(statusStr);
			$('#app_status_popover').popover();

			if(popover != "" && show) {
				$('#app_status_popover').popover('show');
				setTimeout(function(){
					$('#app_status_popover').popover('hide');
				}, 3000);
			}
		},

		'loadingDialog': function(msg="Processing...") {
			swal({
	  				html:'<i class="fa fa-circle-o-notch fa-spin fa-fw"></i> ' + msg + ' <span class="ft-progress"></span>',
	  				showCancelButton: false,
	  				showConfirmButton: false,
	  				allowEscapeKey: false,
	  				allowOutsideClick: false,
	  				padding: 30,
	  			});
		},

		'hideSwal': function() {
			 if(swal.isVisible()){
	            swal.closeModal();
	        }
		},

		'logError': function (log_entry) {
			var date = new Date()
			var log_string = "========================= Renderer process ==========================\n";
			log_string += date.toISOString() + ": \n"; 
			log_string += log_entry + "\n\n";

			fs.appendFile(ftErrorLogFile, log_string, (err) => {

			})
		},

		'availableControls': {
			image: ['src', 'scale', 'angle', 'centeredRotation', 'cropX', 'cropY', 'flipX', 'flipY', 'opacity', 'selectable'],
			text: ['fontFamily'],
		},

		'enableFilters': function() {
			$('#brightness').slider('enable');
			$('#contrast').slider('enable');
			$('#saturation').slider('enable');

			$('#enable_filters').prop('checked', true);
		},

		'disableFilters': function() {
			$('#brightness').slider('refresh');
			$('#brightness').slider('disable');
			$('#contrast').slider('refresh');
			$('#contrast').slider('disable');
			$('#saturation').slider('refresh');
			$('#saturation').slider('disable');

			$('#enable_filters').prop('checked', false);
		},

		'lockControls': function(obj) {
			obj.lockMovementX 	= true;
		    obj.lockMovementY 	= true;
		    obj.lockScalingX 	= true;
		    obj.lockScalingY 	= true;
		    obj.lockRotation 	= true;
		}
	}
});