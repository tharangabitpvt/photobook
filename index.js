const grayBackground = '#263238'
const buttonBackgroundColor = '#455A64'
const inactiveTextColot = '#607D8B'
const textColor = '#CFD8DC'
const lightBgColor = '#eef1f3'

const {app, BrowserWindow, webContents, session, ipcMain, dialog, Tray, shell, Menu} = require('electron')
//const protocols = require('electron-protocols')
const url = require('url')
const path = require('path')
const fs = require('fs')
const fse = require('fs-extra')
const crypto = require('crypto')
const fetch = require('electron-fetch')
const isOnline = require('is-online')
const defaultWorkspace = path.join(app.getPath('documents'), 'photobook')
const AdmZip = require('adm-zip')
//const DownloadManager = require("electron-download-manager");

const ftConfigFilePath = path.join(__dirname, 'ft', 'ft.json')
const ftErrorLogFile = path.join(__dirname, 'ft', 'error_log.txt')
const templatesPath = path.join(__dirname, 'templates')
const defaultExportPath = app.getPath('downloads')
const tempDirPath = path.join(__dirname, 'tmp')

const photobookWeb = 'http://127.0.0.1:8000'

const trialPeriod = 86400000*20; // 20 days

var isFirstLoad = true

//DownloadManager.register(__dirname);

// register photb:// protocol
//protocols.register('photb', protocols.basepath(app.getAppPath()));
//app.commandLine.appendSwitch('--enable-transparent-visuals --disable-gpu')
app.on('ready', () => {
	// check for illegally added templates
	var templString = getTemplatesString()
	var currentTmplKey = getConfig('tmplKey')
	//console.log(tempSecret(templString), tempSecret("1"))
	if(tempSecret(templString) !== currentTmplKey) {
		dialog.showMessageBox({
			buttons: ['Fix me', 'Quit'],
			type: 'warning',
			title: 'Problem detected',
			message: 'There is a problem with your PhotoBook app',
			detail: 'Don\'t worry! We can fix it for you.\nJust click on "Fix me" button and wait.\nInternet connection is required.'
		}, function(btnIndex){
			if(btnIndex == 0) {
				FixMe()
			} else {
				app.exit(0)
			}						
		})

		return
	}

	let {width, height} = require('electron').screen.getPrimaryDisplay().size
	let mainWindow = new BrowserWindow({title: "PhotoBook", fullscreen: false, backgroundColor: grayBackground, width: width, height: height, toolbar:true, show: false})
	mainWindow.maximize()
	let splashWindow = new BrowserWindow({parent: mainWindow, model: true, width: 590, height: 330, resizable: false, frame: false, show: false}) //

	mainWindow.on('closed', () => {mainWindow=null})
	splashWindow.on('closed', () => {splashWindow=null})

	mainWindow.on('close', (event) => {
		const colseAppDialog = dialog.showMessageBox(
			{buttons:['Yes', 'No'], 
			title: 'Quit PhotoBook', 
			type: 'question', 
			message: 'Exit from PhotoBook?', 
			detail: 'Do you want to quit from PhotoBook now?', defaultId: 0, cancelId: 1},
		)

		if(colseAppDialog == 1) {
			event.preventDefault()
		}
	})

	let mainUrl = url.format({
	  protocol: 'file',
	  slashes: true,
	  pathname: path.join(__dirname, 'index.html')
	})

	mainWindow.loadURL(mainUrl)

	mainWindow.on('unresponsive', function () {
        const options = {
          type: 'info',
          title: 'Something went wrong!',
          message: 'You can reload the window or close PhotoBook.',
          detail: 'If you have imported large number of files, please remove some unnecessary files and try. If you see this message very offten, please contact PhotoBook for help.',
          buttons: ['Reload', 'Close']
        }
        dialog.showMessageBox(options, function (index) {
          if (index === 0) mainWindow.reload()
          else mainWindow.close()
        })
    })

    mainWindow.webContents.on('crashed', function () {
        const options = {
            type: 'info',
            title: 'Something went wrong!',
            message: 'You can reload the window or close PhotoBook.',
            detail: 'If you have imported large number of files, please remove some unnecessary files and try. If you see this message very offten, please contact PhotoBook for help.',
            buttons: ['Reload', 'Close']
        }
        dialog.showMessageBox(options, function (index) {
            if (index === 0) mainWindow.reload()
            else mainWindow.close()
        })
    })

    mainWindow.on('closed', () => {
        mainWindow=null
    })


    ipcMain.on('check-for-activation', (event) => {
    	var isAppActive = checkForActivation()
    	var app_serial = getConfig('serial')
    	var app_version = getConfig('version')
    	var app_label = getConfig('app_label')
    	event.sender.send('check-for-activation-resp', JSON.stringify({
    			activeState: isAppActive,
    			appSerial: app_serial, 
    			appVersion: app_version,
    			appLabel: app_label}))
    })
    // set installtion date
    var isAppActive = checkForActivation()

    if(isAppActive !== false) {
    	if(getConfig('workspace') == "") {
			dialog.showMessageBox({buttons: ['Yes, use default location', 'Choose different location'], cancelId: 2, type: 'info', title: 'Setup your workspace', message: 'Your workspace', detail: 'All your PhotoBook projects will be saved in this location.\nDefault location: ' + defaultWorkspace},
				function(btnIndex){
					//console.log(btnIndex)
					if(btnIndex == 0) {
						// set default workspace loaction
						if(saveWorkspace(defaultWorkspace)) {
							app.relaunch()
							app.exit(0)
						} else {
							app.exit(0)
						}
				  	} else if(btnIndex == 1) {
					  	(function setwp(){
				  			dialog.showOpenDialog({
							    properties: ['openDirectory', 'createDirectory', 'promptToCreate']
						  	}, function (dir_path) {
							    if(dir_path == undefined) {
							    	dialog.showMessageBox({buttons: ['Ok, Open file dialog', 'Quit PhotoBook'], cancelId: 2, type: 'info', title: 'Setup your workspace', message: 'You must select your workspace.', detail: 'Please select a location for the workspace.'},
							    		function(btnIndex){
							    			if(btnIndex == 0) {
							    				setwp()
							    			} else {
							    				app.exit(0)
							    			}
							    		}
							    	);									    	
							    } else {
							    	if(saveWorkspace(dir_path[0])) {
										app.relaunch()
										app.exit(0)
									} else {
										app.exit(0)
									}
							    }
						  	});
				  		})();
				  	} else {
				  		app.exit(0)
				  	}
				}
			);
		} else if(getConfig('workspace') != "" && !fs.existsSync(getConfig('workspace'))) {
			dialog.showMessageBox({buttons: ['Select workspace', 'Quit'], type: 'warning', message: 'Workspace not found!' , detail:'It seems following workspace no longer exists.\n' + getConfig('workspace') + '\nPlease re-locate your workspace or select new workspace.'},
				function(btnIndex){
					if(btnIndex == 0) {
						(function setwp(){
				  			dialog.showOpenDialog({
							    properties: ['openDirectory', 'createDirectory', 'promptToCreate']
						  	}, function (dir_path) {
							    if(dir_path == undefined) {
							    	dialog.showMessageBox({buttons: ['Ok, Open file dialog', 'Quit PhotoBook'], cancelId: 2, type: 'info', title: 'Setup your workspace', message: 'You must select your workspace.', detail: 'Please select a location for the workspace.'},
							    		function(btnIndex){
							    			if(btnIndex == 0) {
							    				setwp()
							    			} else {
							    				app.exit(0)
							    			}
							    		}
							    	);									    	
							    } else {
							    	if(saveWorkspace(dir_path[0])) {
										app.relaunch()
										app.exit(0)
									} else {
										app.exit(0)
									}
							    }
						  	});
				  		})();
					} else {
						app.exit(0)
					}
				});
		} else {
			let splashUrl = url.format({
			  	protocol: 'file',
			  	slashes: true,
			  	pathname: path.join(__dirname, 'splash.html')
			})

			splashWindow.loadURL(splashUrl)
			splashWindow.once('ready-to-show', () => {
				mainWindow.show()
				splashWindow.show()				
			})
		    //let saveProjectWindow = new BrowserWindow({parent: mainWindow, modal: true, width: 600, height: 400, resizable: false, backgroundColor:lightBgColor});
		    //saveProjectWindow.loadURL(path.join('file://', __dirname, '/project_save.html'))

		    mainWindow.webContents.session.on('will-download', (event, item, webContents) => {
			  // Set the save path, making Electron not to prompt a save dialog.
			  	var exportPath = getProjectJSONValue('exportPath');
			  	if(exportPath == "") {
		  			item.setSavePath(path.join(defaultExportPath, item.getFilename()))
			  	} else {
			  		item.setSavePath(path.join(exportPath, item.getFilename()))
			  	}
			  
			  	item.on('updated', (event, state) => {
				    if (state === 'interrupted') {
				      console.log('Download is interrupted but can be resumed')
				    } else if (state === 'progressing') {
				      	if (item.isPaused()) {
				        	console.log('Download is paused')
				      	} else {
					        var totalBytes = parseInt(item.getTotalBytes());
		                    var receivedBytes = parseInt(item.getReceivedBytes());
		                    var percentage = parseInt((receivedBytes/totalBytes)*100);
		                    //console.log("Bytes downloaded: " + percentage + "%")
		                    webContents.send('file-download', 'downloading', JSON.stringify({percentage:percentage}));
			      		}
				    }
			  	})
			  	item.once('done', (event, state) => {
				    if (state === 'completed') {
				      webContents.send('file-download', 'completed');
				    } else {
				      console.log(`Download failed: ${state}`)
				    }
			  	})
			})

		    ipcMain.once('close-splash', function(e) {
		    	if(splashWindow != null) {
		    		splashWindow.close()
		    	}
		    })

			ipcMain.on('get-ft-config', function(event) {	

				fs.readFile(ftConfigFilePath, 'utf8', function (err, data) {
				  	if (err) {
				  		logError(err.message)
				  		dialog.showMessageBox({buttons: ['Quit'], title: 'Error.', message: 'Fatal error!', detail: 'Fatal error happened.\nPlease contact PhotoBook.'},
				  			function(btnIndex){
				  				app.exit(0)
				  			})
				  	}				  	 
				  	
				  	var ftConfigObj = JSON.parse(data)
				  	var projectJSONPath = path.join(ftConfigObj.workspace, ftConfigObj.current_project, 'project.json')
					var projectDataJSONPath = path.join(ftConfigObj.workspace, ftConfigObj.current_project, ftConfigObj.current_page + '.json')
					var userProjects = getProjectsList();
					var templatesList = getTemplatesList();
					var activationCodeForCheck =  getConfig("activationCode")

					if(!fs.existsSync(projectJSONPath)) {
						setConfig({'current_project':"", 'current_page': ""})
					}					

				  	if(getConfig('current_project') == "") {
				  		event.sender.send('config-data', JSON.stringify({data:data, projectJSON:{}, defaultWorkspace:defaultWorkspace, userProjects:userProjects, templatesList:templatesList, activationCode: activationCodeForCheck}))
				  	} else {
				  		if(isFirstLoad) {
				  			isFirstLoad = false
				  			var currentProject = getConfig('current_project');

					  		dialog.showMessageBox({
						  		buttons: ['New project', 'Existing project'],
						  		typeof: 'question',
						  		title: 'Open PhotoBook',
						  		message: 'Welcome to PhotoBook',
						  		detail: 'Would you like to start new project or open your previous project "' + currentProject + '"?'
						  	}, function(btnIndex){
						  		if(btnIndex === 0) {
						  			setConfig({'current_project':"", 'current_page': ""})
						  			event.sender.send('config-data', JSON.stringify({data:data, projectJSON:{}, defaultWorkspace:defaultWorkspace, userProjects:userProjects, templatesList:templatesList, activationCode: activationCodeForCheck}))
						  		} else {
						  			if(!fs.existsSync(projectDataJSONPath)) {
										setConfig({'current_page': 1})
										projectDataJSONPath = path.join(ftConfigObj.workspace, ftConfigObj.current_project, '1.json')
									}

							  		fs.readFile(projectJSONPath, 'utf8', function(err, projectJSONObj){
							  			if(err){
							  				logError(err.message)
							  				dialog.showMessageBox({buttons: ['Quit'], title: 'Error.', message: 'Fatal error!', detail: 'Fatal error happened.\nPlease contact PhotoBook.'},
								  			function(btnIndex){
								  				app.exit(0)
								  			})
							  			} else {			  				
							  				fs.readFile(projectDataJSONPath, 'utf8', function(err, projectDataObj){				  				
							  					if(err) {
							  						logError(err.message)
							  						dialog.showMessageBox({buttons: ['Quit'], title: 'Error.', message: 'Fatal error!', detail: 'Fatal error happened.\nPlease contact PhotoBook.'},
										  			function(btnIndex){
										  				app.exit(0)
										  			})
							  					} else {
													event.sender.send('config-data', JSON.stringify({data:data, projectJSON:projectJSONObj, projectDataJSON: projectDataObj, defaultWorkspace:defaultWorkspace, userProjects:userProjects, templatesList:templatesList, activationCode: activationCodeForCheck}))
							  					}
							  				})
							  			}
							  		})
						  		}
						  	})
				  		} else {
				  			if(!fs.existsSync(projectDataJSONPath)) {
								setConfig({'current_page': 1})
								projectDataJSONPath = path.join(ftConfigObj.workspace, ftConfigObj.current_project, '1.json')
							}

					  		fs.readFile(projectJSONPath, 'utf8', function(err, projectJSONObj){
					  			if(err){
					  				logError(err.message)
					  				dialog.showMessageBox({buttons: ['Quit'], title: 'Error.', message: 'Fatal error!', detail: 'Fatal error happened.\nPlease contact PhotoBook.'},
						  			function(btnIndex){
						  				app.exit(0)
						  			})
					  			} else {			  				
					  				fs.readFile(projectDataJSONPath, 'utf8', function(err, projectDataObj){				  				
					  					if(err) {
					  						logError(err.message)
					  						dialog.showMessageBox({buttons: ['Quit'], title: 'Error.', message: 'Fatal error!', detail: 'Fatal error happened.\nPlease contact PhotoBook.'},
								  			function(btnIndex){
								  				app.exit(0)
								  			})
					  					} else {
											event.sender.send('config-data', JSON.stringify({data:data, projectJSON:projectJSONObj, projectDataJSON: projectDataObj, defaultWorkspace:defaultWorkspace, userProjects:userProjects, templatesList:templatesList, activationCode: activationCodeForCheck}))
					  					}
					  				})
					  			}
					  		})
				  		}			  				  		
				  	}
				  	
				})
			})

			// save project
			ipcMain.on('save-project', function(event, projectDataJSON){
				var projectDataObj = JSON.parse(`${projectDataJSON}`)
				var canvasJSONObj = projectDataObj.cavasJSON

				fs.readFile(ftConfigFilePath, 'utf8', (err, data) => {
				  	if (err) {
				  		logError(err.message)
				  		dialog.showMessageBox({buttons: ['Quit'], title: 'Error.', message: 'Fatal error!', detail: 'Fatal error happened.\nPlease contact PhotoBook.'},
				  			function(btnIndex){
				  				app.exit(0)
				  			})
				  	} else {
				  		var ftConfigObj = JSON.parse(data)

				  		var projectJSONData = projectDataObj.projectJSON //JSON.parse(`${projectJSON}`);
						var projectDir = path.join(ftConfigObj.workspace, projectJSONData.name)
						var currentPage = projectDataObj.currentPage
						if(projectDataObj.isNew) {
							currentPage = '1';
						}
						
						var projectJsonPath = path.join(projectDir, 'project.json')
						var projectDataJSONPath = path.join(projectDir, currentPage + '.json')

						if(projectJSONData.exportPath == "") {
							projectJSONData.exportPath = defaultExportPath;
						}

						if(projectJSONData.createDate == "") {
							var date = new Date();
							dateStr = formatDate(date);
							projectJSONData.createDate = dateStr;
						}

						var writeCurrentPage = setConfig({'current_page': currentPage})
						if(writeCurrentPage !== true) {
							event.sender.send('save-project-resp', JSON.stringify({status:'faild', errcode: writeCurrentPage, msg:'Writing config data failed.'}))
							return false
						}

						if(!fs.existsSync(projectDir)) {
							fs.mkdir(projectDir, (err) => {
								if(err && err.code != 'EEXIST'){
									logError(err.message)
									event.sender.send('save-project-resp', JSON.stringify({status:'faild', errcode: err.code, msg:'Make project directory failed.'}))
								} else {
									console.log('project name: ' + projectJSONData.name)
									var projectsObj = ftConfigObj.projects;
									projectsObj.push(projectJSONData.name);

									if(setConfig({'projects': projectsObj, 'current_project': projectJSONData.name}) !== true) {
										event.sender.send('save-project-resp', JSON.stringify({status:'faild', errcode: writeCurrentPage, msg:'Writing config data failed.'}))
										return false
									}
												
									// write project json
									fs.writeFile(projectJsonPath, JSON.stringify(projectJSONData, null, 4), 'utf-8', function(err){
										if(err) {
											logError(err.message)
											event.sender.send('save-project-resp', JSON.stringify({status:'faild', errcode: err.code, msg:'Writing project config file failed.'}))
										} else {
											// save canvas JSON
											fs.writeFile(projectDataJSONPath, JSON.stringify(canvasJSONObj, null, 4), 'utf-8', function(err){
												if(err) {
													logError(err.message)
													event.sender.send('save-project-resp', JSON.stringify({status:'faild', errcode: err.code, msg:'Writing project data failed.'}))
												} else {
													event.sender.send('save-project-resp', JSON.stringify({status:'ok', msg:'Project saved.'}))
												}
											})									
										}
									})
													
								}
							})	
						} else {
							if(projectDataObj.isNew) {
								event.sender.send('save-project-resp', JSON.stringify({status:'faild', errcode: 'EEXIST', msg:'Project already exists.'}))
							} else {
								fs.writeFile(projectJsonPath, JSON.stringify(projectJSONData, null, 4), 'utf-8', function(err){
									if(err) {
										logError(err.message)
										event.sender.send('save-project-resp', JSON.stringify({status:'faild', errcode: err.code, msg:'Writing project config file failed.'}))
									} else {
										// save canvas JSON
										fs.writeFile(projectDataJSONPath, JSON.stringify(canvasJSONObj, null, 4), 'utf-8', function(err){
											if(err) {
												logError(err.message)
												event.sender.send('save-project-resp', JSON.stringify({status:'faild', errcode: err.code, msg:'Writing project data failed.'}))
											} else {
												event.sender.send('save-project-resp', JSON.stringify({status:'ok', msg:'Project saved.'}))
											}
										})							
									}
								})
							}					
						}
				  	}	  	
				})
				
			})

			// show about page
			ipcMain.on('show-about-page', (event) => {
				let aboutWindow = new BrowserWindow({title: "About PhotoBook", width: 600, height: 331, resizable: false, modal: true, frame: false, toolbar: false, show: false})
				let aboutUrl = url.format({
				  	protocol: 'file',
				  	slashes: true,
				  	pathname: path.join(__dirname, 'about.html')
				})
				aboutWindow.loadURL(aboutUrl)

				aboutWindow.once('ready-to-show', () => {
					aboutWindow.show()
				})
			});

			// load market place
			ipcMain.on('load-market-place', (event) => {
				isOnline().then(online => {
					if(online){
						const data = {app_version: getConfig('version'), app_serial: getConfig('serial')}

						fetch(photobookWeb + '/api/app_shop', { 
						    method: 'POST',
						    body:    JSON.stringify(data),
						    headers: { 'Content-Type': 'application/json' },
						})
						.then(res => res.json())
						.then(json => {
							var respJson = {templates: json.newTemplates, apps_updates: json.updates} 
							//console.log(respJson)
							event.sender.send('load-market-place-resp', JSON.stringify(respJson))
						})
						.catch(function (err) {
					        logError(err)
					        event.sender.send('load-market-place-resp', null)
					        //console.log(err)
					        //dialog.showMessageBox({buttons: ['OK'], type: 'error', message: 'Something went wrong!', detail: 'Some error occurred while processing the market place. Please contact PhotoBook for help.'})
				      	})
					}
				})				
			})

			// install templates
			ipcMain.on('installer', (event, args) => {
				// check for illegally added templates
				var templString = getTemplatesString()
				var currentTmplKey = getConfig('tmplKey')

				var installationFile = "";

				if(tempSecret(templString) !== currentTmplKey) {
					dialog.showMessageBox({
						buttons: ['Fix me', 'Quit'],
						type: 'warning',
						title: 'Problem detected',
						message: 'There is a problem with your PhotoBook app',
						detail: 'Don\'t worry! We can fix it for you.\nJust click on "Fix me" button and wait.\nInternet connection is required.'
					}, function(btnIndex){
						if(btnIndex == 0) {
							FixMe()
						} else {
							app.exit(0)
						}						
					})
				} else {
					var args = JSON.parse(`${args}`)
					var installType = args.installType
					var itemIds = args.itemIds
					
					//console.log(installType, itemId)
					//const insWinId = installerWindow.id

					// installer window
					let installerWindow = new BrowserWindow({title: "PhotoBook Installer", fullscreen: false, width: 500, height: 200, parent: mainWindow, resizable: false, show: false})
					let installerUrl = url.format({
					  	protocol: 'file',
					  	slashes: true,
					  	pathname: path.join(__dirname, 'installer.html')
					})

					installerWindow.on('closed', () => {
						installerWindow = null;
						mainWindow.reload();
					})

					installerWindow.on('close', (event) => {
						const installerWindowColseAppDialog = dialog.showMessageBox(
							{buttons:['Yes', 'No'], 
							title: 'PhotoBook Installer', 
							type: 'question', 
							message: 'Cancel Installer', 
							detail: 'Do you want to cancel installer now?', defaultId: 0, cancelId: 1},
						)

						if(installerWindowColseAppDialog == 1) {
							event.preventDefault()
						}
					})

					installerWindow.loadURL(installerUrl)
			    	installerWindow.once('ready-to-show', () => {
						installerWindow.show()

						installerWindow.webContents.send('installer-init', installType, itemIds, getConfig('serial'))
					})

					installerWindow.webContents.session.on('will-download', (event, item, webContents) => {
				        // Set the save path, making Electron not to prompt a save dialog.
				        item.setSavePath(path.join(__dirname, 'tmp', item.getFilename()))			        

				        item.on('updated', (event, state) => {
				            if (state === 'interrupted') {
				                console.log('Download is interrupted but can be resumed')
				                item.resume()
				                //webContents.send('file-download', 'download_interrupt_resumable');
				            } else if (state === 'progressing') {
				                if (item.isPaused()) {
				                    console.log('Download is paused')
				                } else {
				                    var totalBytes = parseInt(item.getTotalBytes());
				                    var receivedBytes = parseInt(item.getReceivedBytes());
				                    var percentage = parseInt((receivedBytes/totalBytes)*100);
				                    console.log(totalBytes, receivedBytes)
				                    webContents.send('file-download', 'downloading', JSON.stringify({percentage:percentage, totalBytes:totalBytes, receivedBytes:receivedBytes}));
				                }
				            }
				        })
				        item.once('done', (event, state) => {
				            if (state === 'completed') {
				                console.log('download completed')
				                webContents.send('file-download', 'completed');

				                downFileName = path.join(tempDirPath, item.getFilename())
				                console.log(downFileName, installType)
				                if(installType == 'template') {
			                	 	webContents.send('file-download', 'installing');
				                	try {
				                		var zip = new AdmZip(downFileName)
				                		//var zipEntries = zip.getEntries();
				                		//zipEntries.forEach(function(zipEntry))
				                		zip.extractAllTo(path.join(templatesPath, '/'), true)
				                	} catch(err) {
				                		logError('Zip error: \n' + err.message)
				                		console.log('Zip error', err.message)
				                		dialog.showMessageBox({
				                			buttons: ["OK"],
				                			type: "error",
				                			title: "PhotoBook Template installer",
				                			message: "Template installation failed.",
				                			detail: "Error occurred while extracting files. Please try again."
				                		}, function(btnIndex){
				                			return
				                		});
				                	} finally {
				                		if(fs.existsSync(downFileName)) {
				                			fs.unlinkSync(downFileName)
				                		}

				                		installationFile = item.getFilename();

				                		webContents.send('file-download', 'download-part-done');

				                		installationFile = "";
				                	}			                	
				                }
				            } else {
				                console.log(`Download failed: ${state}`)
				            }
				        })
				    })

					ipcMain.on('download-part-send-complete', (event, installerType, templateId) => {
            			//console.log('download-parts', item.getFilename())
            			var body = {updateType: installerType, app_serial: getConfig('serial'), itemIds: templateId, installDir: path.basename(installationFile, '.zip')}
                		fetch(photobookWeb + "/api/appupdates/completed", {method: 'POST', body: JSON.stringify(body), headers:{'Content-Type': 'application/json'}, timeout: 5000})
	                    .then(res => res.json())
	                    .then(respJson => {
	                    	//console.log(respJson);
	                        if(respJson.resp == 'done') {
	                        	var newTmplStr = getTemplatesString()
	                        	setConfig({'tmplKey': tempSecret(newTmplStr)})

	                        	//installerWindow.hide();
                				event.sender.send('download-part-send-complete-resp', 'done');
                				installationFile = "";
                				//ipcMain.removeAllListeners('download-parts');
	                        } else {
	                        	logError('Server error on update: \n' + respJson.errorMessage);
	                        	dialog.showMessageBox({
	                        		buttons: ['OK'],
	                        		type: 'error',
	                        		title: 'Installer',
	                        		message: 'Something went wrong!',
	                        		detail: 'Some error happened when completing update process.',
	                        	})
	                        }
	                    })
	                    .catch(err => {
	                        event.returnValue = 404
	                        logError("Fetch Error" + err.message)
	                        console.log("Fetch Error" + err.message)
	                        dialog.showMessageBox({
                        		buttons: ['OK'],
                        		type: 'error',
                        		title: 'Installer',
                        		message: 'Something went wrong!',
                        		detail: 'Some error happened when completing update process.',
                        	})
	                    })
            			
            		})
				}
			})

			// set app name
			if(getConfig('app_label') == "" || getConfig('app_label') == null) {
				ipcMain.on('set-app-name', (event, app_name) => {
					if(app_name === null || app_name == "") {
						console.log('app name is empty')
						return
					}
					const appSerialNumber = getConfig('serial');
					const data = {app_name: app_name, app_serial: appSerialNumber}
					fetch(photobookWeb + '/api/set_app_name', { 
					    method: 'POST',
					    body:    JSON.stringify(data),
					    headers: { 'Content-Type': 'application/json' },
					})
					.then(res => res.json())
					.then(json => {
						if(json.set_app_name_resp === true) {
							setConfig({'app_label': app_name})								
						}
						event.sender.send('set-app-name-resp', JSON.stringify({resp: json.set_app_name_resp}))
					})
					.catch(function (err) {
				        logError(err)
				        console.log(err)
				        dialog.showMessageBox({buttons: ['OK'], type: 'error', message: 'Something went wrong!', detail: 'Some error occurred while saving the app name. Please contact PhotoBook for help.'})
			      	})
				})
			}			
		}
    } else {
    	// load trial expired window
		let activationWindow = new BrowserWindow({title: "PhotoBook", width: 600, height: 335, parent: mainWindow, modal: true, frame: false,  resizable: false, show: false})
		let aboutUrl = url.format({
		  	protocol: 'file',
		  	slashes: true,
		  	pathname: path.join(__dirname, 'about.html')
		})
		activationWindow.loadURL(aboutUrl)
    	activationWindow.once('ready-to-show', () => {
			activationWindow.show()
		})  
    }

    ipcMain.on('try-activate', (event, activationKey) => {
    	const dialogTitle = 'PhotoBook activation'
    	isOnline().then(online => {
    		if(online) {
    			const data = {appkey: activationKey}

				fetch(photobookWeb + '/api/pb_activate', { 
				    method: 'POST',
				    body:    JSON.stringify(data),
				    headers: { 'Content-Type': 'application/json' },
				})
				.then(res => res.json())
				.then(json => {
					var activationRespose = json.activation_response
					
					if(!activationRespose) {
						event.sender.send('activation-resp', 'failed')
						dialog.showMessageBox({buttons: ['OK'], type: 'error', title: dialogTitle, message: 'Invalid activation key!', detail: 'Please check your activation key and try again.\nIf you need any help on activation please contact PhotoBook.'})
					} else if(activationRespose === 'used') {
						event.sender.send('activation-resp', 'failed')
						dialog.showMessageBox({buttons: ['OK'], type: 'error', title: dialogTitle, message: 'Key already used.', detail: 'This key is already used. Please contact PhotoBook for help.'})
					} else if(activationRespose === true) {
						setConfig({'serial': json.serial_no, 'activationCode': activationKey})
						event.sender.send('activation-resp', 'success')
						dialog.showMessageBox({buttons: ['OK'], type: 'info', title: dialogTitle, message: 'Activation successful!', detail: 'Your PhotoBook is activated successfully.\nApplication will be restarted now.\nEnjoy PhotoBook!'},
							function(btnIndex){
								app.relaunch()
								app.exit(0)
							})
					} else {
						dialog.showMessageBox({buttons: ['OK'], type: 'error', title: dialogTitle, message: 'Something went wrong!', detail: 'Some error occurred while processing the activation. Please contact PhotoBook for help. Your app will be closed now.'},
			        	function(){
			        		app.exit(0)
			        	})
					}
				})
				.catch(function (err) {
			        logError(err)
			        //console.log(err)
			        dialog.showMessageBox({buttons: ['OK'], type: 'error', title: dialogTitle, message: 'Something went wrong!', detail: 'Some error occurred while processing the activation. Please contact PhotoBook for help. Your app will be closed now.'},
			        	function(){
			        		app.exit(0)
			        	})
		      	})
    		} else {
    			dialog.showMessageBox({buttons: ['OK'], type: 'info', title: dialogTitle, message: 'Connection failed!', detail: 'Please check your internet connection and try again.'})
    		}
    	})
    })
	
});

// fix PhotoBook
function FixMe() {
	isOnline().then(online => {
		if(online) {
			var body = {app_serial: getConfig('serial')}
			fetch(photobookWeb + "/api/fixme", {method: 'POST', body: JSON.stringify(body), headers:{'Content-Type': 'application/json'}, timeout: 5000})
		    .then(res => res.json())
		    .then(respJson => {
		    	//console.log(respJson);
		    	if(respJson.template_ids == "") {
			        dialog.showMessageBox({
			    		buttons: ['OK'],
			    		type: 'error',
			    		title: 'Fix me',
			    		message: 'Re-install the app',
			    		detail: 'You don\'t have any templates installed yet.\nSo, please re-install the app.',
			    	}, function(btnIndex){
			    		app.exit(0)
			    	})

			    	return
		    	} else {
		    		var installedTemplates = respJson.template_ids.split(",");
		    		var templates = fs.readdirSync(templatesPath);

					templates.forEach(function(tempDirName){
						if(tempDirName != 'img' && tempDirName != "1") {
							if(installedTemplates.indexOf(tempDirName) == -1) {
								fse.removeSync(path.join(templatesPath, tempDirName))
							}
						}			
					});

					app.relaunch()
					app.exit(0)
		    	}
		    })
		    .catch(err => {
		        //event.returnValue = 404
		        logError("Fetch Error: " + err.message)
		        console.log("Fetch Error: " + err.message)
		        dialog.showMessageBox({
		    		buttons: ['OK'],
		    		type: 'error',
		    		title: 'Fix me',
		    		message: 'Something went wrong!',
		    		detail: 'Some error happened when fixing process.',
		    	}, function(btnIndex){
		    		app.exit(0)
		    	})
		    })
		} else {
			dialog.showMessageBox({
				buttons: ['OK'], 
				type: 'info', 
				title: 'Fix me', 
				message: 'Connection failed!', 
				detail: 'No Internet connection'
				}, function(btnIndex){
					app.exit(0)
				})
		}
	})
}

// write config values
function setConfig(configObj) {
	try {
		var confDataObj = JSON.parse(fs.readFileSync(ftConfigFilePath, 'utf8'))
		var configKeys = Object.keys(configObj)
		configKeys.forEach(function(confKey) {
			var confVal = configObj[confKey]
			confDataObj[confKey] = confVal
			fs.writeFileSync(ftConfigFilePath, JSON.stringify(confDataObj), 'utf8')
		})		
	} catch(err) {
		logError(err.message)
		return err.message
	}

	return true
}

// get config values
function getConfig(confKey) {
	try {
		var confDataObj = JSON.parse(fs.readFileSync(ftConfigFilePath, 'utf8'))
		return confDataObj[confKey]
	} catch(err) {
		logError(err.message)
		return err.message
	}
}

// update project JSON file
function updateProjectJSON(newJSONString) {
	var workspace = getConfig('workspace')
	var projectName = getConfig('current_project')
	var projectJSONFilePath = path.join(workspace, projectName, 'project.json')

	try {
		if(typeof(newJSONString) == 'object') {
			fs.writeFileSync(projectJSONFilePath, JSON.stringify(newJSONString))
		} else if(typeof(newJSONString) == 'string') {
			fs.writeFileSync(projectJSONFilePath, newJSONString)
		}

		return true
	} catch(err) {
		logError(err.message)
		return err.message
	}
}

// get project JSON values
function getProjectJSONValue(confKey) {
	var workspace = getConfig('workspace')
	var projectName = getConfig('current_project')
	var projectJSONFilePath = path.join(workspace, projectName, 'project.json')

	try {
		var confDataObj = JSON.parse(fs.readFileSync(projectJSONFilePath, 'utf8'))
		return confDataObj[confKey]
	} catch(err) {
		logError(err.message)
		return err.message
	}
}

function tempSecret(tempStr) {
	const salt = "asduerqe32%dasd@f*gjut1s3"
	const hash = crypto.createHash('sha256')
	return hash.update(tempStr + salt).digest('hex')
}

function getTemplatesList() {
	var templateInfo = {}

	try {
		var templates = fs.readdirSync(templatesPath);
		
		templates.forEach(function(tempDirName){
			if(tempDirName != 'img') {
				var tempDetails = {}
				var tempJSONPath = path.join(templatesPath, tempDirName, 'template.json')
				var tempJSON = JSON.parse(fs.readFileSync(tempJSONPath, 'utf8'))
				//console.log(JSON.parse(tempJSON))
				tempDetails.id = tempJSON.id
				tempDetails.name = tempJSON.name
				tempDetails.sizes = tempJSON.sizes
				tempDetails.pages = tempJSON.pages
				tempDetails.noof_placeholders = tempJSON.noof_placeholders
				templateInfo[tempDirName] = tempDetails
			}			
		});
	} catch(err) {
		logError(err.message)
		return err.message
	}

	return templateInfo;
	
}

function getTemplatesString() {
	var templatesString = "";
	try {
		var templates = fs.readdirSync(templatesPath);
		var temlateIds = [];
		templates.forEach(function(tempDirName){
			if(tempDirName != 'img') {
				temlateIds.push(tempDirName)				
			}			
		});
	} catch(err) {
		logError(err.message)
		return false
	}

	temlateIds.sort()

	return temlateIds.toString()
}

function getProjectsList() {
	var projects = getConfig('projects')
	var workspaceDir = getConfig('workspace')
	var projectsInfo = {}

	for(var a=projects.length-1; a>=0; a--) {
		var proName = projects[a];
		var proFile = path.join(workspaceDir, proName, 'project.json');
		if(fs.existsSync(proFile)) {
			var proJSON = JSON.parse(fs.readFileSync(proFile, 'utf8'));
			var pagesObj = proJSON.pages;
			var noofNotTouched = 0;
			var noofEdit = 0;
			var noofExported = 0;
			var pagesArraySorted = Object.keys(pagesObj);
			pagesArraySorted.forEach(function(pg){
				if(pagesObj[pg] == 0) {
					noofNotTouched++;
				} else if(pagesObj[pg] == 1) {
					noofEdit++;
				} else if(pagesObj[pg] == 2) {
					noofExported++;
				}
			});
			projectsInfo[proName] = {album_size: proJSON.album_size, template: proJSON.template, pages: {total:Object.keys(pagesObj).length, noTouched: noofNotTouched, edit: noofEdit, exported: noofExported}, createDate: proJSON.createDate};
		}			
	}
	
	return projectsInfo;
}

function formatDate(date) {
	var hours = date.getHours();
	var minutes = date.getMinutes();
	var ampm = hours >= 12 ? 'pm' : 'am';
	hours = hours % 12;
	hours = hours ? hours : 12; // the hour '0' should be '12'
	minutes = minutes < 10 ? '0'+minutes : minutes;
	var strTime = hours + ':' + minutes + ' ' + ampm;
	var dayVal = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
	var monthVal = date.getMonth()+1 < 10 ? '0' + (date.getMonth()+1) : date.getMonth()+1;
	return date.getFullYear() + "-" + monthVal + "-" + dayVal + " " + strTime;
}

function saveWorkspace(workspaceDir) {	
	try {
		fs.mkdirSync(workspaceDir);
		var setwsp = setConfig({'workspace': workspaceDir})
		if(setwsp) {
			return true
		} else {
			return false
		}
	} catch(err) {
		if(err.code == 'EEXIST') {
			var setwsp = setConfig({'workspace': workspaceDir})
			if(setwsp) {
				return true
			} else {
				return false
			}
		} else {
			logError(err.message)
			return false;
		}		
	}
}

// check activation status
function checkForActivation() {
	var appActive = true
    var installDate = getConfig('installDate')
    if(installDate == "") {
    	var timestamp = new Date().getTime()
    	setConfig({'installDate': timestamp})
    } else if(installDate > 0) {
    	// check for trail period
    	var installedDate = getConfig("installDate")
    	var activationCode = getConfig("activationCode")
    	var nowTimestamp = new Date().getTime()
    	if(activationCode == "" && (nowTimestamp - installedDate) > trialPeriod) {
    		appActive = false
    	} else if(activationCode == "") {
    		var daysLeft = parseInt(((installedDate+trialPeriod) - nowTimestamp)/86400000)
    		appActive = {status: 'in-trial', daysLeft: daysLeft}
    	}
    }

    return appActive
}

// format error log string
function logError(log_entry) {
	var date = new Date()
	var log_string = "========================= Main process ==========================\n";
	log_string += date.toISOString() + ": \n"; 
	log_string += log_entry + "\n\n";

	fs.appendFile(ftErrorLogFile, log_string, (err) => {

	})
}